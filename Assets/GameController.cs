﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	GameStateVO gsVo = GameStateVO.Instance;
	public ScoreboardController sc;	
	public DicesController dc;
	public DicesDisplayController ddc;
	public RollButtonController rbc;
	public BonusRollController brc;
	public PlayerTurnController ptc;
	public SubmitButtonController sbc;
	public lockIconController lic;
	public ToolTipsController ttc;
	public SoundManager sm;
	public PopupManager ppm;
	public TopHudController thc;
	public TickerController tc;
	public TripleReplayController trc;
	public CurrencyCounterController ccc;
	public Animator sceneAnimation;

	// Use this for initialization
	void Start () {
		GameStates ();
	}
	
	public void resetGame ()
	{
		gsVo.currentTurnState = GameStateVO.TurnState.gameInit;
			GameStates ();
	}

	void GameStates ()
	{
		switch (gsVo.currentTurnState) 
		{
		case GameStateVO.TurnState.gameInit:
			gsVo.Initialize();
			sc.UpdateVisual();
			dc.MakeDicesEmpty ();
			sc.UpdateYahtzeeBonus ();
			dc.UpdateVisuals();
			rbc.UpdateVisuals();
			brc.UpdateVisuals();
			ptc.UpdateVisuals();
			trc.UpdateVisuals();
			ccc.UpdateVisuals();
			sbc.SetActiveSubmitButton (false);
			lic.SetLockIconState (lockIconController.LockIconState.nothing);
			ttc.TurnOffAllToolTips();
			tc.UpdateVisualState();
			sm.gc = this;
			sm.playStartSound();
			ppm.closePopups();
			break;
		case GameStateVO.TurnState.switchPlayer:
			
			break;
		case GameStateVO.TurnState.endGame:
			gsVo.InitializeDices ();
//			dc.MakeDicesEmpty ();
			dc.UpdateVisuals ();
			gsVo.currentRoll = GameStateVO.Rolls.noRoll;
			rbc.UpdateVisuals();
			brc.UpdateVisuals ();
			sbc.SetActiveSubmitButton (false);
//			ppm.SetPop(PopupManager.PopUpTypes.WinScreen);
			break;
		}
	}

	public void RestartRolls ()
	{
		if (ccc.PayStuff (3)) {
			dc.MakeDicesEmpty ();
			gsVo.TripleReplayPlayed();
			gsVo.currentRoll = GameStateVO.Rolls.noRoll;
			sbc.SetActiveSubmitButton(false);
			GoToNextRoll ();
		}
	}

	public void BonusRoll()
	{
		if (ccc.PayStuff (1)) {
			GoToNextRoll ();
		}
	}

	public void GoToNextRoll ()
	{
		if (!GameStateVO.Instance.AreAllDiceLocked ()) {
			gsVo.currentRoll++;
	
		switch (gsVo.currentRoll) {
		case GameStateVO.Rolls.noRoll:
			break;
		case GameStateVO.Rolls.roll0:
			sbc.SetActiveSubmitButton (false);
			lic.SetLockIconState (lockIconController.LockIconState.nothing);
			break;
		case GameStateVO.Rolls.roll1:
		case GameStateVO.Rolls.roll2:
		case GameStateVO.Rolls.bonusRoll:
//		case GameStateVO.Rolls.tripleReplay:
		case GameStateVO.Rolls.RollEnd:
			sbc.SetActiveSubmitButton (false);
			lic.SetLockIconState (lockIconController.LockIconState.disable);
			dc.RollDices();
			break;
		}
		}
		rbc.UpdateVisuals();
		brc.UpdateVisuals();
		trc.UpdateVisuals();
		tc.UpdateVisualState();
		dc.UpdateVisuals();
		sc.UpdateVisual();
	}

	public void EndTurnPlayer ()
	{
		sc.ScoreCurrentlyVisualizedScore (GameStateVO.Instance.CurrentPlayer());
		sc.UpdateScorePoints (GameStateVO.Instance.CurrentPlayer());
		sc.UpdateYahtzeeBonus ();
		sc.UpdateVisual ();

		if (sceneAnimation != null) 
		{
			if (GameStateVO.Instance.currentPlayerTurn == GameStateVO.PlayerTurn.player1) 
			{
				sceneAnimation.SetTrigger ("p1Animation");
			} 
			else 
			{
				ddc.UpdateVisuals ();
				ddc.AnimateDisaply ();
				sceneAnimation.SetTrigger ("p2Animation");
			}
		}
		else 
		{
			prepareNextTurn();
		}
	}

	public void prepareNextTurn ()
	{
		sbc.SetActiveSubmitButton (false);

		gsVo.currentTurnState = GameStateVO.TurnState.switchPlayer;
		gsVo.SwitchPlayer ();
		gsVo.ResetRolls ();
		gsVo.InitializeDices ();
		dc.MakeDicesEmpty ();
		dc.UpdateVisuals ();

		sc.VisuallyUpdateSubTotal (GameStateVO.Instance.player1);
		sc.VisuallyUpdateSubTotal2 (GameStateVO.Instance.player2);
		thc.UpdatePlayersScores (GameStateVO.Instance.player1);
		sc.UpdateVisual ();
//		ptc.UpdateVisuals ();
		rbc.UpdateVisuals ();
		brc.UpdateVisuals ();
		tc.UpdateVisualState ();
		sc.VisuallyUpdateBonusScore (GameStateVO.Instance.player1);
		sc.VisuallyUpdateBonusScore2 (GameStateVO.Instance.player2);

		if (gsVo.AreAllSlotsScored()) {
			gsVo.currentTurnState = GameStateVO.TurnState.endGame;
			GameStates ();
		}
	}
}