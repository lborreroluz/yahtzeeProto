﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DiceDisplayController : MonoBehaviour {

	public DiceFacing DiceNumber;
	public GameObject DiceContainer;
	public int value;
	public int id;

	public enum VisualStates
	{
		empty = 0,
		rolled
	}
	public VisualStates currentDiceState = VisualStates.empty;
	
	public void UpdateVisuel ()
	{		
		switch (currentDiceState)
		{
		case VisualStates.empty:
			DiceContainer.gameObject.SetActive(false);
			break;
		case VisualStates.rolled:
			DiceNumber.setFace(value);
			DiceContainer.gameObject.SetActive(true);
			break;
		}
	}
}