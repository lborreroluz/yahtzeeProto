﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System;

public class ScoreboardController : MonoBehaviour {

	public List<ScoreboardSlotController> scoreCard = new List<ScoreboardSlotController>();
	public GameController gc;
	public GameStateVO gs;
	public BonusScoreController bonusScore;
	public BonusScoreController bonusScore2;
	public Text SubTotal;
	public Text SubTotal2;
	public Text UpperScore;
	public Text LowerScore;

	public int SelectedSlotId = -1;

	void Start ()
	{
		gs = GameStateVO.Instance;
		for (int i = 0; i < scoreCard.Count; i++) {
			scoreCard[i].id = i;
			scoreCard[i].gc = gc;
		}
		UpdateVisual();
	}

	public void ToggleToolTips ()
	{
		for(int i = 0; i<scoreCard.Count; i++)
		{
			scoreCard[i].isToolTip = !scoreCard[i].isToolTip;
		}
	}

	public void VisuallyUpdateSubTotal (PlayerVO player)//GameStateVO.Instance.CurrentPlayer ()
	{	
		int upScore = 0;
		
		for (int i = 0; i < 6; i++) {
			if (player.ScoreBoard [i].currentState == ScoreboardSlotVO.SlotState.scored) {
				upScore += player.ScoreBoard [i].value;
			}
		}
		SubTotal.text = "" + upScore + "/63";
	}

	public void VisuallyUpdateSubTotal2 (PlayerVO player)//GameStateVO.Instance.CurrentPlayer ()
	{	
		int upScore = 0;
		
		for (int i = 0; i < 6; i++) {
			if (player.ScoreBoard [i].currentState == ScoreboardSlotVO.SlotState.scored) {
				upScore += player.ScoreBoard [i].value;
			}
		}
		SubTotal2.text = "" + upScore + "/63";
	}

	public void UpdateScorePoints (PlayerVO player)//GameStateVO.Instance.CurrentPlayer ()
	{	
		int upScore = 0;
		int lowrScore = 0;
		int totalScore = 0;
		
		for (int i = 0; i < 6; i++) {
			if (player.ScoreBoard [i].currentState == ScoreboardSlotVO.SlotState.scored) {
				upScore += player.ScoreBoard [i].value;
			}
		}
		if (upScore >= 63) {
			upScore += 35;
//			gc.sm.playSound(gc.sm.ellen_slot_special_UpperSectionBonus);
		}

		for (int i = 6; i < player.ScoreBoard.Count; i++) 
		{
			if (player.ScoreBoard [i].currentState == ScoreboardSlotVO.SlotState.scored) 
			{
				lowrScore += player.ScoreBoard [i].value;
			}
		}
		if (player.YahtzeeBonuses > 1) {
//			upScore += ((ybsc2.valueToDisplay)*50);
//			lowrScore += ((ybsc.valueToDisplay)*50);
		}
		totalScore = upScore + lowrScore;

		UpperScore.text = "" + upScore;
		LowerScore.text = "" + lowrScore;
//		TotalScore.text = "" + totalScore;
		player.TotalScore = totalScore;
	}

	public void VisuallyUpdateBonusScore (PlayerVO player)//GameStateVO.Instance.CurrentPlayer ()
	{
		int upScore = 0;
		for (int i = 0; i < 6; i++) {
			if (player.ScoreBoard [i].currentState == ScoreboardSlotVO.SlotState.scored) {
				upScore += player.ScoreBoard [i].value;
			}
		}
		bonusScore.valueToDisplay = upScore;
		Debug.Log("a: " + upScore);
		if (upScore >= 63) {
			SubTotal.gameObject.SetActive(false);
			bonusScore.currentVisualState = ScoreboardSlotController.VisualState.scored;
			bonusScore.UpdateVisual ();
		} else {
			bonusScore.currentVisualState = ScoreboardSlotController.VisualState.normalNoVisualizedScore;
			bonusScore.UpdateVisual ();
		}
	}

	public void VisuallyUpdateBonusScore2 (PlayerVO player)//GameStateVO.Instance.CurrentPlayer ()
	{
		int upScore = 0;
		for (int i = 0; i < 6; i++) {
			if (player.ScoreBoard [i].currentState == ScoreboardSlotVO.SlotState.scored) {
				upScore += player.ScoreBoard [i].value;
			}
		}
		bonusScore2.valueToDisplay = upScore;
		if (upScore >= 63) {
			SubTotal2.gameObject.SetActive(false);
			bonusScore2.currentVisualState = ScoreboardSlotController.VisualState.scored;
			bonusScore2.UpdateVisual ();
		} else {
			bonusScore2.currentVisualState = ScoreboardSlotController.VisualState.normalNoVisualizedScore;
			bonusScore2.UpdateVisual ();
		}
	}

	public void UpdateYahtzeeBonus ()
	{
		if (GameStateVO.Instance.CurrentPlayer ().ScoreBoard [(int)PlayerVO.PlayerScoreBoard.yahtzee].currentState == ScoreboardSlotVO.SlotState.scored &&
		    CheckForXOfAKind (GameStateVO.Instance.Dices, 5) &&
			CalculatePoints(SelectedSlotId) > 0
			) {
			GameStateVO.Instance.CurrentPlayer ().YahtzeeBonuses++;
			if (GameStateVO.Instance.CurrentPlayer ().YahtzeeBonuses > 1) {
//				if (SelectedSlotId < 6) {
//					ybsc2.valueToDisplay++;
//					ybsc2.currentVisualState = ScoreboardSlotController.VisualState.scored;
//				} else {
//					ybsc.valueToDisplay++;
//					ybsc.currentVisualState = ScoreboardSlotController.VisualState.scored;
//				}
				scoreCard[SelectedSlotId].EnableBonusBanner(true);
			}
		}
//		ybsc.UpdateVisual();
//		ybsc2.UpdateVisual();
	}

	public void UpdateVisual()
	{
		for(int i = 0; i<scoreCard.Count; i++)
		{
			scoreCard[i].UpdateVisual();
		}
		bonusScore.UpdateVisual();
		bonusScore2.UpdateVisual();
	}

//	public void ChangeVisualToSpecificState(ScoreboardSlotController.VisualState slotState)
//	{
//		for (int i = 0; i < scoreCard.Count; i++) {
//			if (GameStateVO.Instance.CurrentPlayer().ScoreBoard[i].currentState == ScoreboardSlotVO.SlotState.scored) {
//				continue;
//			}
//			else 
//			{
//				scoreCard[i].currentVisualState = slotState;
//			}
//			scoreCard[i].UpdateVisual();
//		}
//	}

//	public void UnselectVisualizedPoints()
//	{
//		for (int i = 0; i < scoreCard.Count; i++) {
//			if (GameStateVO.Instance.CurrentPlayer().ScoreBoard[i].currentState == ScoreboardSlotVO.SlotState.scored) {
//				continue;
//			}
//			else 
//			{
//				scoreCard[i].currentVisualState = ScoreboardSlotController.VisualState.empty;
//			}
//			scoreCard[i].UpdateVisual();
//		}
//	}

	public void ScoreCurrentlyVisualizedScore(PlayerVO player)//GameStateVO.Instance.CurrentPlayer ()
	{
		player.ScoreBoard [SelectedSlotId].currentState = ScoreboardSlotVO.SlotState.scored;
		player.ScoreBoard [SelectedSlotId].value = CalculatePoints(SelectedSlotId);
		player.lastScoredSlotId = SelectedSlotId;
	}

//	public void VisualizeOneScoreOnPlayerCard (int id)
//	{
//		for (int i = 0; i < scoreCard.Count; i++) {
//			if (GameStateVO.Instance.CurrentPlayer().ScoreBoard[i].currentState == ScoreboardSlotVO.SlotState.scored) {
//				continue;
//			}
//			if (i == id) {
//				scoreCard[i].valueToDisplay = CalculatePoints(i);
//				scoreCard[i].currentVisualState = ScoreboardSlotController.VisualState.visualizingPoint;
//				SelectedSlotId = id;
//			}
//			else 
//			{
//				scoreCard[i].currentVisualState = ScoreboardSlotController.VisualState.empty;
//			}
//			scoreCard[i].UpdateVisual();
//		}
//	}

//	public void VisualizePlayerCard(PlayerVO player)//GameStateVO.Instance.CurrentPlayer ()
//	{
//		for (int i = 0; i < scoreCard.Count; i++) {
//			scoreCard [i].TitleText.text = GameStateVO.Instance.CurrentPlayer().typeTitleStrings[i];
//			scoreCard [i].DescriptiveText.text = GameStateVO.Instance.CurrentPlayer().descriptionStrings[i];
//			scoreCard [i].valueToDisplay = player.ScoreBoard [i].value;
//			scoreCard [i].valueToDisplayForThem = GameStateVO.Instance.OtherPlayer ().ScoreBoard [i].value;
//			
//			switch (player.ScoreBoard [i].currentState) 
//			{
//			case ScoreboardSlotVO.SlotState.empty:
//				scoreCard [i].currentVisualState = ScoreboardSlotController.VisualState.empty;
//				break;
//			case ScoreboardSlotVO.SlotState.scored:
//				scoreCard[i].currentVisualState = ScoreboardSlotController.VisualState.scored;
//				break;
//			}
//			
//			scoreCard[i].UpdateVisual();
//		}
//	}

//	public void VisualizePossiblePoints ()
//	{
//		for (int i = 0; i < scoreCard.Count; i++) {
//			if (GameStateVO.Instance.CurrentPlayer().ScoreBoard[i].currentState == ScoreboardSlotVO.SlotState.scored) {
//				continue;
//			}
//			scoreCard[i].valueToDisplay = CalculatePoints(i);
//			if (scoreCard[i].valueToDisplay > 0) {
//				scoreCard[i].currentVisualState = ScoreboardSlotController.VisualState.visualizingPoint;
//			} else {
//				scoreCard[i].currentVisualState = ScoreboardSlotController.VisualState.empty;
//			}
//			scoreCard[i].UpdateVisual ();
//		}
//	}

	public int CalculatePoints (int i)
	{
		int returnvalue = 0;
		switch ((PlayerVO.PlayerScoreBoard)i) {
			case PlayerVO.PlayerScoreBoard.ones:
				returnvalue = addPoints (GameStateVO.Instance.Dices, 1);
				break;
			case PlayerVO.PlayerScoreBoard.twos:
				returnvalue = addPoints (GameStateVO.Instance.Dices, 2);
				break;
			case PlayerVO.PlayerScoreBoard.threes:
				returnvalue = addPoints (GameStateVO.Instance.Dices, 3);
				break;
			case PlayerVO.PlayerScoreBoard.frous:
				returnvalue = addPoints (GameStateVO.Instance.Dices, 4);
				break;
			case PlayerVO.PlayerScoreBoard.fives:
				returnvalue = addPoints (GameStateVO.Instance.Dices, 5);
				break;
			case PlayerVO.PlayerScoreBoard.sixes:
				returnvalue = addPoints (GameStateVO.Instance.Dices, 6);
				break;
			case PlayerVO.PlayerScoreBoard.threeOfAKind:
				if (CheckForXOfAKind (GameStateVO.Instance.Dices, 3)) {
					returnvalue = addPoints (GameStateVO.Instance.Dices);
				}
				break;
			case PlayerVO.PlayerScoreBoard.fourOfAKind:
				if (CheckForXOfAKind (GameStateVO.Instance.Dices, 4)) {
					returnvalue = addPoints (GameStateVO.Instance.Dices);
				}
				break;
			case PlayerVO.PlayerScoreBoard.fullHouse:
				if (CheckForFullHouse (GameStateVO.Instance.Dices)) {
					returnvalue = 25;
				}
				break;
			case PlayerVO.PlayerScoreBoard.smallStraight:
				if (CheckForShortSequence(GameStateVO.Instance.Dices)) {
					returnvalue = 30;
				}
				break;
			case PlayerVO.PlayerScoreBoard.largeStraight:
				if (CheckForLongSequence(GameStateVO.Instance.Dices)) {
					returnvalue = 40;
				}
				break;
			case PlayerVO.PlayerScoreBoard.yahtzee:
				if (CheckForXOfAKind (GameStateVO.Instance.Dices, 5)) {
					returnvalue = 50;
				}
				break;
			case PlayerVO.PlayerScoreBoard.chance:
				returnvalue = addPoints (GameStateVO.Instance.Dices);
				break;
			}
			return returnvalue;
	}

	public bool CheckForShortSequence (List<DiceVO> diceValues)
	{
		var foos = new List<DiceVO> (diceValues);
		for (int i = foos.Count - 1; i >= 0; i--) {
			for (int j = foos.Count - 1; j >= 0; j--) {
				if (foos [i].value == foos [j].value && i != j) {
					foos.RemoveAt (j);
					break;
				}
			}
		}

		if (foos.Count < 4) {
			return false;
		}

		int[] numbers = new int[foos.Count];
		for (int i = 0; i < foos.Count; i++) {
			numbers [i] = foos [i].value;
		}
		Array.Sort(numbers);

		string s = "";
		for (int i = 0; i < numbers.Length; i++) {
			s += numbers[i];
		}
		return (bool)(s.Contains("1234") || s.Contains("2345") || s.Contains("3456"));
	}

	public bool CheckForLongSequence (List<DiceVO> diceValues)
	{
		var foos = new List<DiceVO> (diceValues);
		for (int i = foos.Count - 1; i >= 0; i--) {
			for (int j = foos.Count - 1; j >= 0; j--) {
				if (foos [i].value == foos [j].value && i != j) {
					foos.RemoveAt (j);
					break;
				}
			}
		}

		if (foos.Count < 5) {
			return false;
		}

		int[] numbers = new int[foos.Count];
		for (int i = 0; i < foos.Count; i++) {
			numbers [i] = foos [i].value;
		}
		Array.Sort(numbers);

		string s = "";
		for (int i = 0; i < numbers.Length; i++) {
			s += numbers[i];
		}
		return (bool)(s.Contains("12345") || s.Contains("23456"));
	}

	public bool CheckForFullHouse (List<DiceVO> diceValues)
	{	
		bool returnValue1 = false;
		bool returnValue2 = false;
		int[] numbers = new int[5] { 0, 1, 2, 3, 4 };
		for (int i = 0; i < diceValues.Count; i++) {
			numbers [i] = diceValues [i].value;
		}

		int excluded = 0;
		for (int i = 1; i < 7; i++) {
			List<int> numQuery =
				(from num in numbers
				 where num == i
				 select num).ToList ();
			if (numQuery.Count >= 3) {
				returnValue1 = true;
				excluded = numQuery [0];
			}
		}
		
		int leftOver = 0;
		for (int i = 1; i < 7; i++) {
			if (excluded == i) {continue; }
			List<int> numQuery =
				(from num in numbers
				         where num == i
				         select num).ToList ();
			if(numQuery.Count >= 2)
			{
				leftOver = numQuery [0];
				returnValue2 = true;
			}
		}
		return (returnValue1 && returnValue2);
	}

	public bool CheckForXOfAKind (List<DiceVO> diceValues, int x)
	{
		bool returnValue = false;
		int[] numbers = new int[5] { 0, 1, 2, 3, 4 };
		for (int i = 0; i < diceValues.Count; i++) {
			numbers [i] = diceValues [i].value;
		}

		for (int i = 1; i < 7; i++) {
			List<int> numQuery =
				(from num in numbers
				         where num == i
				         select num).ToList ();
			if(numQuery.Count >= x)
			{
				returnValue = true;
			}
		}
		return returnValue;
	}

	public int addPoints (List<DiceVO> diceValues, int number)
	{
		int sum = 0;
		for (int i = 0; i < diceValues.Count; i++) {
			if (diceValues [i].value == number) 
			{
				sum += 	diceValues[i].value;
			}
		}
		return sum;
	}

	public int addPoints (List<DiceVO> diceValues)
	{
		int sum = 0;
		for (int i = 0; i < diceValues.Count; i++) {
				sum += 	diceValues[i].value;
		}
		return sum;
	}

	public void EnterScore (int scoreIndex)
	{
		Debug.Log(scoreIndex);
	}
}
