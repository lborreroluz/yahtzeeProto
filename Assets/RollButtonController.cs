﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RollButtonController : MonoBehaviour {

	public Button rollButton;
	public Text buttonText;
	public Image roll1;
	public Image roll2;
	public Image roll3;
	public Color enabledRollColor;
	public Color usedRollColor;

	public void UpdateVisuals ()
	{
		if (GameStateVO.Instance.currentPlayerTurn == GameStateVO.PlayerTurn.player1) {
//			rollButton.gameObject.SetActive(false);
			switch (GameStateVO.Instance.currentRoll) {
			case GameStateVO.Rolls.noRoll:	
				rollButton.gameObject.SetActive (false);
				break;
			case GameStateVO.Rolls.roll0:	
				rollButton.gameObject.SetActive (true);
				buttonText.text = "Roll";
				roll1.color = enabledRollColor;
				roll2.color = enabledRollColor;
				roll3.color = enabledRollColor;
				break;
			case GameStateVO.Rolls.roll1:	
				rollButton.gameObject.SetActive (true);
				buttonText.text = "Roll";
				roll1.color = usedRollColor;
				roll2.color = enabledRollColor;
				roll3.color = enabledRollColor;
				break;
			case GameStateVO.Rolls.roll2:	
				rollButton.gameObject.SetActive (true);
				buttonText.text = "Roll";
				roll1.color = usedRollColor;
				roll2.color = usedRollColor;
				roll3.color = enabledRollColor;
				break;
			case GameStateVO.Rolls.bonusRoll:	
//			case GameStateVO.Rolls.tripleReplay:	
				rollButton.gameObject.SetActive (false);
				buttonText.text = "Bonus Roll 3[BR]";
				break;
			case GameStateVO.Rolls.RollEnd:	
				rollButton.gameObject.SetActive (false);
				buttonText.text = "";
				break;
			}
		} else {
			rollButton.gameObject.SetActive(false);		
		}
	}
}