﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class IconSelector : MonoBehaviour {

	public List<GameObject> iconList;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	public void UpdateVisuals (int num)
	{
		for (int i = 0; i < iconList.Count; i++) {
			iconList[i].SetActive(i == num);
		}
	}
}
