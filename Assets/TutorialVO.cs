﻿using UnityEngine;
using System.Collections;

public sealed class TutorialVO{
    static readonly TutorialVO _instance = new TutorialVO();
	int tutorialIncrement;
	int rollsMade;
	int informationButtonClicked;
	int DiceLocked;
	int cellSubmitted;
	
    public static TutorialVO Instance
    {
		get
		{
		    return _instance;
		}
    }

    TutorialVO()
    {
		tutorialIncrement = 0;
		rollsMade = 0;
		informationButtonClicked = 0;
		DiceLocked = 0;
		cellSubmitted = 0;
    }

	public void TutorialIncrement()
	{
		tutorialIncrement++;
	}
}
