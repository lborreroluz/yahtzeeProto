﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SubmitButtonController : MonoBehaviour {

	public GameController gc;
	public GameObject SubmitButton;
	public Button ActualSubmitButton;
	public Text Description;
	public IconSelector icon;
	public Button InformationIcon;
	public GameObject messageContainerNoneRolled;

	public enum SubmitButtonState
	{
		invisible = 0,
		noDiceRolled,
		entrySubmitable,
		alreadySubmitted
	}

	public SubmitButtonState currentSubmitButtonState = SubmitButtonState.invisible;

	public void SetActiveSubmitButton(bool value)
	{
		SubmitButton.SetActive (value);
		switch(GameStateVO.Instance.currentRoll)
		{
			case GameStateVO.Rolls.noRoll:
			case GameStateVO.Rolls.roll0:
			case GameStateVO.Rolls.RollEnd:
			currentSubmitButtonState = SubmitButtonState.invisible;
			break;
			case GameStateVO.Rolls.roll1:
			case GameStateVO.Rolls.roll2:
			case GameStateVO.Rolls.bonusRoll:
//			case GameStateVO.Rolls.tripleReplay:
			currentSubmitButtonState = SubmitButtonState.entrySubmitable;
			break;
		}
		UpdateVisual();
	}

	public void UpdateVisual()
	{
		icon.UpdateVisuals(gc.sc.SelectedSlotId);
		switch(currentSubmitButtonState)
		{
			case SubmitButtonState.invisible:
			ActualSubmitButton.interactable = false;
			ActualSubmitButton.gameObject.SetActive(false);
			Description.text = "" + TextDescriptions.getTitleForSlotID(gc.sc.SelectedSlotId) + " for " + gc.sc.CalculatePoints(gc.sc.SelectedSlotId) + " points.";
			messageContainerNoneRolled.SetActive(true);
			break;
			case SubmitButtonState.noDiceRolled:
			break;
			case SubmitButtonState.entrySubmitable:
			ActualSubmitButton.interactable = true;
			ActualSubmitButton.gameObject.SetActive(true);
			Description.text = "Play " + TextDescriptions.getTitleForSlotID(gc.sc.SelectedSlotId) + " for " + gc.sc.CalculatePoints(gc.sc.SelectedSlotId) + " points.";
			messageContainerNoneRolled.SetActive(false);
			break;
			case SubmitButtonState.alreadySubmitted:
			break;
		}
	}

	public void ShowTip ()
	{
		gc.ttc.TurnOnAToolTip(gc.sc.SelectedSlotId);
		gc.ttc.TurnOnAToolTip(44);
		gc.ttc.TurnOnAToolTip(45);
	}
}