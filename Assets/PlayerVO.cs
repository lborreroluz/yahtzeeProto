﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerVO {

	public int YahtzeeBonuses;
	public int TotalScore;
	public int lastScoredSlotId = -1;

	public enum PlayerScoreBoard
	{
		ones = 0,
		twos,
		threes,
		frous,
		fives,
		sixes,
		threeOfAKind,
		fourOfAKind,
		fullHouse,
		smallStraight,
		largeStraight,
		yahtzee,
		chance
	}

	public List<ScoreboardSlotVO> ScoreBoard = new List<ScoreboardSlotVO>(13);

	public PlayerVO ()
	{
		for (int i = 0; i < 13; i++) 
		{
			ScoreBoard.Add(new ScoreboardSlotVO());
		}
	}

	public int GiveMeFirstEmptyScoreboardSlotId()
	{
		int returnValue = -1;
		for (int i = 0; i < 13; i++) 
		{
			if (ScoreBoard [i].currentState == ScoreboardSlotVO.SlotState.empty) 
			{
				returnValue = i;
				break;
			}
		}
		return returnValue;
	}

	public void Initialize ()
	{
		
		for (int i = 0; i < 13; i++) 
		{
			ScoreBoard[i].value = 0;
			ScoreBoard[i].currentState = ScoreboardSlotVO.SlotState.empty;
		}
	}
}
