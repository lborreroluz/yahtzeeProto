﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BonusScoreController : ScoreboardSlotController {

	public GameObject button;
	public GameObject infoIcon;
	public Image ring;
	public int valueToDisplay;
	
	public void UpdateVisual ()
	{
		switch (currentVisualState) 
		{
		case VisualState.disabled:
		case VisualState.normalNoVisualizedScore:
		case VisualState.visualizingPoint:
		case VisualState.highlight:
			ScoreText.text = "+0";
			ScoreText.color = normalColor;
			ScoreText.gameObject.SetActive(false);
			button.SetActive(true);
			infoIcon.SetActive(true);
			ring.fillAmount = ((float)valueToDisplay/63);
			break;
		case VisualState.scored:
			ScoreText.gameObject.SetActive(true);
			ScoreText.text = "+35";
			DescriptiveText.text = "" + valueToDisplay + "/63";
			ScoreText.color = scoredColor;
			button.SetActive(false);
			infoIcon.SetActive(false);
			break;
		}
	}
	
	public void VisualizePoints ()
	{
	}
	
	public void ScorePoints ()
	{
	}

	public void ShowTip()
	{
		gc.ttc.TurnOnAToolTip(id);
		gc.ttc.TurnOnAToolTip(44);
	}
}
