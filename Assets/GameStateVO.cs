﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class GameStateVO
{
	
	public PlayerVO player1;
	public PlayerVO player2;

	public List<ScoreboardSlotVO> PossibleScore = new List<ScoreboardSlotVO>(13);
	public List<DiceVO> Dices = new List<DiceVO>(5);

	public bool HasUsedTripleReplay;
 
	public enum TurnState
	{
		gameInit = 0,
		switchPlayer,
		endGame
	}
	public TurnState currentTurnState;

	public enum Rolls
	{
		noRoll = 0,
		roll0,
		roll1,
		roll2,
		bonusRoll,
//		tripleReplay,
		RollEnd
	}
	public Rolls currentRoll = Rolls.roll0;
	
	public enum PlayerTurn
	{
		player1 = 1,
		player2
	}
	public PlayerTurn currentPlayerTurn;

	public void SwitchPlayer ()
	{
		currentPlayerTurn = (currentPlayerTurn == PlayerTurn.player1)?PlayerTurn.player2:PlayerTurn.player1;
		HasUsedTripleReplay = false;
		ResetRolls ();
	}

	public void ResetRolls ()
	{
		InitializeDices();
		currentRoll = Rolls.roll0;
	}

    static readonly GameStateVO _instance = new GameStateVO();
    public static GameStateVO Instance
    {
		get
		{
		    return _instance;
		}
    }

    GameStateVO()
    {
		HasUsedTripleReplay = false;
		player1 = new PlayerVO();
		player2 = new PlayerVO();
		currentTurnState = TurnState.gameInit;
		currentPlayerTurn = PlayerTurn.player1;
		for (int i = 0; i < 5; i++) 
		{
			Dices.Add(new DiceVO());
		}
		for (int i = 0; i < 13; i++) 
		{
			PossibleScore.Add(new ScoreboardSlotVO());
		}
    }

	public void Initialize ()
	{
		player1.Initialize();
		player2.Initialize();
		currentTurnState = TurnState.gameInit;
		currentPlayerTurn = PlayerTurn.player1;
		InitializeDices();
	}

	public void InitializeDices()
	{
		for (int i = 0; i < 5; i++) 
		{
			Dices[i].value = 0;
			Dices[i].currentDiceState = DiceVO.DiceState.empty;
		}
	}
	
	public PlayerVO CurrentPlayer ()
	{
		return (currentPlayerTurn == PlayerTurn.player1)?player1:player2;
	}

	public PlayerVO OtherPlayer ()
	{
		return (currentPlayerTurn != PlayerTurn.player1)?player1:player2;
	}

	public int GetDiceSum()
	{
		int sum = 0;
		for (int i = 0; i < Dices.Count; i++) 
		{
			sum += Dices[i].value;
		}
		return sum;
	}

	public bool AreAnyDicesLocked()
	{
		bool returnValue = false;
		for (int i = 0; i < Dices.Count; i++) 
		{
			if (Dices [i].currentDiceState == DiceVO.DiceState.locked) 
			{
				returnValue = true;
			}
		}
		return returnValue;
	}

	public bool AreAllDiceLocked()
	{
		bool returnValue = true;
		for (int i = 0; i < Dices.Count; i++) 
		{
			if (Dices [i].currentDiceState == DiceVO.DiceState.rolled || Dices [i].currentDiceState == DiceVO.DiceState.empty) 
			{
				returnValue = false;
			}
		}
		return returnValue;
	}

	public void TripleReplayPlayed()
	{
		HasUsedTripleReplay = true;
	}

	public bool HasPlayerPlayedTripleReplay ()
	{
		return HasUsedTripleReplay;
	}
	
	public bool AreAllSlotsScored ()
	{
		bool returnValue = true;
		for (int i = 0; i < PossibleScore.Count; i++) {
			if (CurrentPlayer().ScoreBoard [i].currentState != ScoreboardSlotVO.SlotState.scored) {
				returnValue = false;
			}
		}
		return returnValue;
	}
}