﻿using UnityEngine;
using System.Collections;

public class DiceVO {

	public DiceVO()
	{
		value = 0;
		currentDiceState = DiceState.empty;
	}
	
	public int value;
	public enum DiceState
	{
		empty = 0,
		rolled,
		locked
	}
	public DiceState currentDiceState = DiceState.empty;
}
