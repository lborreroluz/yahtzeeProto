﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TopHudController : MonoBehaviour {

	public Text playerScore1;
	public Text playerScore2;

	public string p1Prefix;
	public string p2Prefix;

	string scores1;
	string scores2;

	public void UpdatePlayersScores (PlayerVO player)
	{
		scores1 = "" + GameStateVO.Instance.player1.TotalScore;
		scores2 = "" + GameStateVO.Instance.player2.TotalScore;
		StartCoroutine ("ScoreAnimation");
	}

	IEnumerator ScoreAnimation()
	{
		if (GameStateVO.Instance.currentPlayerTurn == GameStateVO.PlayerTurn.player1) {
//			yield return new WaitForSeconds (2.6f);
			playerScore2.text = p1Prefix + scores2;
			playerScore1.text = p2Prefix + scores1;
		} else 
		{
			playerScore2.text = p1Prefix + scores2;
			playerScore1.text = p2Prefix + scores1;
		}
		yield return null;
	}
}