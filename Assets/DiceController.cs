﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DiceController : MonoBehaviour {

	public DiceFacing DiceNumber;
	public DiceFacing LockedDiceNumber;
	public Button DiceButton;
	public Button LockedDiceButton;
	public int value;
	public int id;
	public GameController gc;

	public enum VisualStates
	{
		empty = 0,
		rolled,
		locked	
	}
	public VisualStates currentDiceState = VisualStates.empty;
	
	public void UpdateVisuel ()
	{		
		switch (currentDiceState)
		{
		case VisualStates.empty:
			DiceButton.gameObject.SetActive(false);
			LockedDiceButton.gameObject.SetActive(false);
			break;
		case VisualStates.locked:
			DiceButton.gameObject.SetActive(false);
			LockedDiceButton.gameObject.SetActive(true);
			break;
		case VisualStates.rolled:
			DiceNumber.setFace(value);
			LockedDiceNumber.setFace(value);
			DiceButton.gameObject.SetActive(true);
			LockedDiceButton.gameObject.SetActive(false);
			break;
		}
	}

	public void MakeEmpty ()
	{		
		currentDiceState = VisualStates.empty;
	}

	public void LockDice ()
	{
		GameStateVO.Instance.Dices[id].currentDiceState = DiceVO.DiceState.locked;
		currentDiceState = VisualStates.locked;
		gc.lic.SetLockIconState (lockIconController.LockIconState.enabled);
		UpdateVisuel ();
	}

	public void UnlockDice ()
	{
		GameStateVO.Instance.Dices[id].currentDiceState = DiceVO.DiceState.rolled;
		currentDiceState = VisualStates.rolled;
		gc.lic.SetLockIconState (lockIconController.LockIconState.disable);
		gc.dc.UpdateVisuals();
		UpdateVisuel ();
	}

	public void RollDice ()
	{
		if (currentDiceState != VisualStates.locked) {
			value = RandomGenerator.GetRandomNumber(1, 7);
			currentDiceState = VisualStates.rolled;
			UpdateVisuel ();
		}
	}
}
