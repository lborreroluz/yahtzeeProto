﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerTurnController : MonoBehaviour {

	public Text playerName;
	public GameObject Player1Highlight;
	public GameObject Player2Highlight;
	bool isP1;

//	public void ChangePlayerTurn ()
//	{
//		if (GameStateVO.Instance.currentPlayerTurn != GameStateVO.PlayerTurn.player1) 
//		{
//			GameStateVO.Instance.currentPlayerTurn = GameStateVO.PlayerTurn.player1;
//		}
//		else
//		{
//			GameStateVO.Instance.currentPlayerTurn = GameStateVO.PlayerTurn.player2;
//		}
//	}

	public void UpdateVisuals () {
		isP1 = (GameStateVO.Instance.currentPlayerTurn == GameStateVO.PlayerTurn.player1);
//		playerName.text = (isP1)?"Player1":"Player2";
//		StartCoroutine ("SwitchPlayerAnimation");
	}

	IEnumerator SwitchPlayerAnimation()
	{
		if (isP1) {
			yield return new WaitForSeconds (2.6f);
		}
		Player1Highlight.SetActive (isP1);
		Player2Highlight.SetActive (!isP1);
		yield return null;
	}
}