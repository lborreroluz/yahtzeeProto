﻿using UnityEngine;
using System.Collections;

public class UserDataVO {

	static readonly UserDataVO _instance = new UserDataVO();
    public static UserDataVO Instance
    {
		get
		{
		    return _instance;
		}
    }

	public int userSoftCurrencyCount;

    UserDataVO()
    {
		userSoftCurrencyCount = 5;
    }

	public bool RemoveCash (int cost)
	{
		bool returnValue = false;
		if (userSoftCurrencyCount >= cost) {
			userSoftCurrencyCount -= cost;
			returnValue = true;
		}
		return returnValue;
	}
}