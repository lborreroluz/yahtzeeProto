﻿using UnityEngine;
using System.Collections;

public class HamburgerIconToggle : MonoBehaviour {

	public GameObject MenuContainer;
	public ToolTipsController ttc;

	public void toggleMenu()
	{
		ttc.TurnOnAToolTip(14);
		ttc.TurnOnAToolTip (44);
	}
}
