﻿using UnityEngine;
using System.Collections;

public class ScoreboardSlotVO {

	public ScoreboardSlotVO ()
	{
		value = 0;
		currentState = SlotState.empty;
	}

	public enum SlotState
	{
		empty = 0,
		scored
	}

	public int value = 0;
	public SlotState currentState = SlotState.empty;
}
