﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DicesDisplayController : MonoBehaviour {
	public List<DiceDisplayController> DicesDisplayControllerList;
	public GameObject DisplayContainer;

	void Start ()
	{
		DisplayContainer.SetActive (false);
		for (int i = 0; i < DicesDisplayControllerList.Count; i++) {
			DicesDisplayControllerList [i].id = i;
		}
	}

	public void UpdateVisuals ()
	{
		for (int i = 0; i < DicesDisplayControllerList.Count; i++) 
		{
			DicesDisplayControllerList[i].value = GameStateVO.Instance.Dices[i].value;
			DicesDisplayControllerList[i].UpdateVisuel ();
		}		
	}

	public void AnimateDisaply()
	{
		DisplayContainer.SetActive (true);
		StartCoroutine(ShowDice());
	}

	public void HideDisplay ()
	{
		
	}

	IEnumerator ShowDice() {
		for (int i = 0; i < DicesDisplayControllerList.Count; i++) 
		{
			DicesDisplayControllerList[i].value = GameStateVO.Instance.Dices[i].value;
			DicesDisplayControllerList [i].currentDiceState = DiceDisplayController.VisualStates.rolled;
			DicesDisplayControllerList[i].UpdateVisuel ();
		}
		yield return new WaitForSeconds(2.5f);
		DisplayContainer.SetActive (false);
		yield return null;
	}
}
