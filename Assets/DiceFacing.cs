﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DiceFacing : MonoBehaviour {
		
	public Image face;
	public List<Sprite> faces = new List<Sprite>();

	public void setFace (int i)
	{
		face.sprite = faces[i-1];
	}
}
