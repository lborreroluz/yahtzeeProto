﻿using UnityEngine;
using System.Collections;

public class PlayerAi : MonoBehaviour {

	public GameController gc;
	GameStateVO gsVo = GameStateVO.Instance;
	bool hasPlayed = false;

	void Update()
	{
		PlayTurn();
	}

	public void PlayTurn ()
	{
		if (gsVo.currentPlayerTurn == GameStateVO.PlayerTurn.player2) {
			if (!hasPlayed) {
				gc.GoToNextRoll ();
				if (gsVo.currentRoll != GameStateVO.Rolls.noRoll) {
					hasPlayed = true;
					int slotId = CheckBestScoreSlotId ();
					gc.sc.SelectedSlotId = slotId;
					gc.EndTurnPlayer ();
				}
			}
		} 
else
		{
			hasPlayed = false;
		}
	}

	int CheckBestScoreSlotId ()
	{
		int bestScore = -1;
		int bestScoreId = -1;
		for (int i = 0; i < gsVo.player2.ScoreBoard.Count; i++) 
		{
			if (gsVo.player2.ScoreBoard [i].currentState == ScoreboardSlotVO.SlotState.empty) 
			{
				if (bestScore < gc.sc.CalculatePoints (i)) 
				{
					bestScore = gc.sc.CalculatePoints (i);
					bestScoreId = i;
				}
			}
		}
		return bestScoreId;
	} 
}
