﻿using UnityEditor;
using System.Linq;

public static class Unity5Compatibility
{
#if UNITY_5
	public static int FindParameter(this UnityEditor.Animations.AnimatorController animatorController, string name)
	{
		return animatorController.parameters.Count(x=>x.name == name);
	}
	public static int GetLayerCount(this UnityEditor.Animations.AnimatorController animatorController)
	{
		return animatorController.layers.Length;
	}
	public static int GetStateMachineCount(this UnityEditor.Animations.AnimatorStateMachine animatorStateMachine)
	{
		return animatorStateMachine.stateMachines.Length;
	}
	public static UnityEditor.Animations.AnimatorControllerLayer GetLayer(this UnityEditor.Animations.AnimatorController animatorController, int index)
	{
		return animatorController.layers[index];
	}
	public static UnityEditor.Animations.AnimatorState GetState(this UnityEditor.Animations.AnimatorStateMachine animatorStateMachine, int index)
	{
		return animatorStateMachine.states[index].state;
	}
	public static void RemoveState(this UnityEditor.Animations.AnimatorStateMachine animatorStateMachine, UnityEditor.Animations.ChildAnimatorState childAnimatorState)
	{
		animatorStateMachine.RemoveState(childAnimatorState.state);
	}
	public static void RemoveTransition(this UnityEditor.Animations.AnimatorStateMachine animatorStateMachine, UnityEditor.Animations.AnimatorStateTransition transition)
	{
		foreach(var state in animatorStateMachine.states)
		{
			state.state.RemoveTransition(transition);
		}
	}
	public static UnityEditor.Animations.AnimatorStateTransition AddTransition(this UnityEditor.Animations.AnimatorStateMachine animatorStateMachine, UnityEditor.Animations.AnimatorState from, UnityEditor.Animations.AnimatorState to)
	{
		return from.AddTransition(to);
	}
	public static UnityEditor.Animations.AnimatorStateTransition[] GetTransitionsFromState(this UnityEditor.Animations.AnimatorStateMachine animatorStateMachine, UnityEditor.Animations.AnimatorState fromState)
	{
		return fromState.transitions;
	}
	public static UnityEditor.Animations.AnimatorState GetDestinationState(this UnityEditor.Animations.AnimatorStateTransition state)
	{
		return state.destinationState;
	}
	public static int GetStateCount(this UnityEditor.Animations.AnimatorStateMachine animatorStateMachine)
	{
		return animatorStateMachine.states.Length;
	}
	public static int GetNameHash(this UnityEditor.Animations.ChildAnimatorState childAnimatorState)
	{
		return childAnimatorState.state.nameHash;
	}
	public static UnityEngine.Motion GetMotion(this UnityEditor.Animations.AnimatorState animatorState)
	{
		return animatorState.motion;
	}
	public static int GetNameHash(this UnityEditor.Animations.AnimatorState animatorState)
	{
		return animatorState.nameHash;
	}
	public static string GetName(this UnityEditor.Animations.ChildAnimatorState childAnimatorState)
	{
		return childAnimatorState.state.name;
	}
	public static string GetName(this UnityEditor.Animations.AnimatorState animatorState)
	{
		return animatorState.name;
	}
	public static void SetAnimationClip(this UnityEditor.Animations.AnimatorState animatorState, UnityEngine.AnimationClip animationClip )
	{
		animatorState.motion = animationClip;
	}
	public static UnityEditor.Animations.AnimatorCondition GetCondition(this UnityEditor.Animations.AnimatorStateTransition animatorStateTransition, int index )
	{
		return animatorStateTransition.conditions[index];
	}
#else
	public static int GetStateCount(this UnityEditorInternal.StateMachine stateMachine)
	{
		return stateMachine.stateCount;
	}
	public static int GetNameHash(this UnityEditorInternal.State state)
	{
		return state.uniqueNameHash;
	}
	public static string GetName(this UnityEditorInternal.State state)
	{
		return state.uniqueName;
	}
	public static UnityEditorInternal.State GetDestinationState(this UnityEditorInternal.Transition transition)
	{
		return transition.dstState;
	}
	public static int GetLayerCount(this UnityEditorInternal.AnimatorController animatorController)
	{
		return animatorController.layerCount;
	}
	public static int GetStateMachineCount(this UnityEditorInternal.StateMachine animatorStateMachine)
	{
		return animatorStateMachine.stateMachineCount;
	}
#endif
}
