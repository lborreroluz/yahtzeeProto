﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// Utility class drawing common Unity GUI elements.
/// </summary>
public static class CoreGUILayout
{
	private static bool sInitStyles = true;
	private static Texture2D sLogo;
	private static GUILayoutOption[] sLogoLabelOptions;
	private static GUIStyle sLogoLabelStyle;

	public static void InspectorHeader(string moduleTitle)
	{
		InspectorHeader (moduleTitle, null);
	}

	public static void InspectorHeader(string moduleTitle, string helpUrl)
	{
		InspectorHeader (moduleTitle, helpUrl, Color.white);
	}

	public static void InspectorHeader(string moduleTitle, string helpUrl, Color textColor)
	{
		if (sInitStyles)
		{
			sInitStyles = false;
			sLogo = Resources.Load<Texture2D>("logo-HIBERNUM");
			sLogoLabelStyle = new GUIStyle();
			sLogoLabelStyle.fontSize = 16;
			sLogoLabelStyle.fontStyle = FontStyle.Bold;
			sLogoLabelStyle.normal.textColor = textColor;
			sLogoLabelStyle.imagePosition = ImagePosition.ImageLeft;
			sLogoLabelStyle.alignment = TextAnchor.MiddleCenter;
			sLogoLabelStyle.margin = new RectOffset(2,2,10,10);
			sLogoLabelStyle.wordWrap = false;

			sLogoLabelOptions = new GUILayoutOption[]
			{
				GUILayout.MaxHeight(32f)
			};
		}

		var hasHelpUrl = !string.IsNullOrEmpty(helpUrl);
		var tooltip = "© Hibernum Créations" + (hasHelpUrl ? ". Click for more info." : string.Empty);
		if (GUILayout.Button (new GUIContent(moduleTitle, sLogo, tooltip), sLogoLabelStyle, sLogoLabelOptions))
		{
			if (hasHelpUrl)
			{
				Application.OpenURL(helpUrl);
			}
		}
	}
}
