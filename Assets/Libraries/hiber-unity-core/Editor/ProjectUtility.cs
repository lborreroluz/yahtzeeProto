﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class ProjectUtility : AssetPostprocessor 
{
//	private static void OnPostprocessAllAssets(string[] importedAssets,string[] deletedAssets,string[] movedAssets,string[] movedFromAssetPaths)
//	{
//		
//	}

	#region Directory Clean
	[MenuItem("Tools/Delete Empty Directories")]
	private static void EmptyDirectories()
	{
		int count = 0;

		ProcessDirectory (Application.dataPath, ref count);
		Debug.Log (string.Format ("Removed {0} empty directories from the project.", count));

		AssetDatabase.Refresh ();
	}

	[MenuItem("Tools/Delete All Player Prefs")]
	private static void DeleteAllPlayerPrefs()
	{
		PlayerPrefs.DeleteAll();
		PlayerPrefs.Save();
	}

	[MenuItem("Tools/Delete All Editor Prefs")]
	private static void DeleteAllEditorPrefs()
	{
		EditorPrefs.DeleteAll();
	}

	private static void ProcessDirectory(string startLocation, ref int removeCount)
	{
		string[] dirs = Directory.GetDirectories(startLocation);
		for(int i = 0, max = dirs.Length; i < max; ++i)
		{
			ProcessDirectory(dirs[i], ref removeCount);
			if (FilesAreAllMetaOrNoFiles (Directory.GetFiles(dirs[i])) && Directory.GetDirectories(dirs[i]).Length == 0)
			{
				Debug.Log(string.Format ("Empty dir: {0}", dirs[i]));
				Directory.Delete(dirs[i]);
				File.Delete (dirs[i] + ".meta");
				++removeCount;
			}
		}
	}

	private static bool FilesAreAllMetaOrNoFiles(string[] files)
	{
		if(files == null || files.Length == 0){ return true; }

		for(int i = 0, max = files.Length; i < max; ++i)
		{
			if(Path.GetExtension (files[i]) != ".meta")
			{
				return false;
			}
		}

		return true;
	}
	#endregion Directory Clean
}