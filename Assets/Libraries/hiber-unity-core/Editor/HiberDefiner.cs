﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;
using System.Text.RegularExpressions;

using Hibernum.Core;

public class HiberDefinerPostProcessor : AssetPostprocessor 
{
	//	Unknown,
	//	Standalone,
	//	WebPlayer,
	//	iPhone = 4,
	//	PS3,
	//	XBOX360,
	//	Android,
	//	GLESEmu = 9,
	//	NaCl = 11,
	//	FlashPlayer,
	//	Metro = 14,
	//	WP8,
	//	BB10,
	//	BlackBerry = 16,
	//	Tizen,
	//	PSP2,
	//	PS4,
	//	PSM,
	//	XboxOne,
	//	SamsungTV

	public static void OnPostprocessAllAssets (string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
	{
		bool reloaded = false;

		for(int i = 0, max = importedAssets.Length; i < max; ++i)
		{
			string asset = importedAssets[i];
			if(CheckForReload (asset)){ break; }
		}

		if(reloaded){ return; }

		for(int i = 0, max = deletedAssets.Length; i < max; ++i)
		{
			string asset = deletedAssets[i];
			if(CheckForReload (asset)){ break; }
		}
	}

	private static bool CheckForReload(string asset)
	{
		if(Path.GetExtension (asset) == ".def" || Path.GetExtension (asset) == ".pdef")
		{
			DefinerPreferences.LoadDefines ();
			DefinerPreferences.SaveDefines ();
			return true;
		}

		return false;
	}
}

public static class DefinerPreferences
{
	private static string kProjectDefinePath
	{
		get
		{
			return Application.dataPath + "/ProjectDefines.pdef";
		}
	}

	private static bool _kProjectDefinesAreLoaded = false;
	private static bool _kFileDefinesLoaded = false;

	private static string[] _kPlatformValues;
	public static string[] PlatformValues
	{
		get
		{
			if(_kPlatformValues == null)
			{
				List<string> values = new List<string>();
				Array vals = System.Enum.GetValues (typeof(BuildTargetGroup));
				
				foreach(var v in vals)
				{
					var btg = (BuildTargetGroup)v;
					if (btg == BuildTargetGroup.Unknown)
					{
						// Getting/Setting the defines for the Unknown group results in an error
						continue;
					}
					if(!values.Contains (btg.ToString ()))
					{
						values.Add (btg.ToString ());
					}
				}

				_kPlatformValues = values.ToArray ();
			}

			return _kPlatformValues;
		}
	}

	// Define format: DEFINE|Android|iPhone;OTHER_DEFINE|Standalone
	private static List<Define> _kProjectDefinesInternal = new List<Define>();
	public static List<Define> ProjectDefines
	{
		get
		{
			if(!_kProjectDefinesAreLoaded)
			{
				LoadProjectDefines ();
			}

			return _kProjectDefinesInternal;
		}
	}

	private static List<Define> _kFileDefinesInternal = new List<Define>();
	public static List<Define> FileDefines
	{
		get
		{
			if(!_kFileDefinesLoaded)
			{
				LoadFileDefines ();
			}

			return _kFileDefinesInternal;
		}
	}

	public static List<Define> AllDefines
	{
		get
		{
			List<Define> allDefs = new List<Define>();
			allDefs.AddRange (ProjectDefines);
			allDefs.AddRange (FileDefines);

			return allDefs;
		}
	}

	private static Vector2 _scrollPos = new Vector2();
	private static int _logLevel = 4;

	[PreferenceItem("Hiber Defines")]
	public static void PreferenceGUI()
	{
		if(GUILayout.Button ("Scan Project For Defines"))
		{
			DefineFinderWizard.OpenWizard ();
		}

		GUILayout.BeginHorizontal ();
		if(GUILayout.Button ("Refresh Defines"))
		{
			_kProjectDefinesAreLoaded = false;
			_kFileDefinesLoaded = false;
		}

		if(!_kProjectDefinesAreLoaded)
		{
			LoadProjectDefines ();
		}

		if(!_kFileDefinesLoaded)
		{
			LoadFileDefines ();
		}
		
		List<Define> defs = ProjectDefines;
		
		if(GUILayout.Button ("Add New Define"))
		{
			_kProjectDefinesInternal.Add (new Define());
		}
		GUILayout.EndHorizontal ();

		int newLogLevel = EditorGUILayout.Popup("Log Level", _logLevel, new string[]{"None", "Info", "Warning", "Error", "Debug (Trace)"});
		if(newLogLevel != _logLevel)
		{
			_logLevel = newLogLevel;

            //TODO: Since opt out changes, the defnies has changed
			Define info = new Define("LOGGING_INFO|All");
			Define warning = new Define("LOGGING_WARNING|All");
			Define error = new Define("LOGGING_ERROR|All");
			Define debug = new Define("LOGGING_DEBUG|All");

			switch(newLogLevel)
			{
			case 0:
				_kProjectDefinesInternal.Remove (info);
				_kProjectDefinesInternal.Remove (warning);
				_kProjectDefinesInternal.Remove (error);
				_kProjectDefinesInternal.Remove (debug);
				break;

			case 1:
				if(_kProjectDefinesInternal.Find ((x)=>{ return x == info && (x.Enabled = true) && (x.Groups = -1) == -1;}) == null) { _kProjectDefinesInternal.Add (info); }
				_kProjectDefinesInternal.Remove (warning);
				_kProjectDefinesInternal.Remove (error);
				_kProjectDefinesInternal.Remove (debug);
				break;

			case 2:
				if(_kProjectDefinesInternal.Find ((x)=>{ return x == info && (x.Enabled = true) && (x.Groups = -1) == -1;}) == null) { _kProjectDefinesInternal.Add (info); }
				if(_kProjectDefinesInternal.Find ((x)=>{ return x == warning && (x.Enabled = true) && (x.Groups = -1) == -1;}) == null) { _kProjectDefinesInternal.Add (warning); }
				_kProjectDefinesInternal.Remove (error);
				_kProjectDefinesInternal.Remove (debug);
				break;

			case 3:
				if(_kProjectDefinesInternal.Find ((x)=>{ return x == info && (x.Enabled = true) && (x.Groups = -1) == -1;}) == null) { _kProjectDefinesInternal.Add (info); }
				if(_kProjectDefinesInternal.Find ((x)=>{ return x == warning && (x.Enabled = true) && (x.Groups = -1) == -1;}) == null) { _kProjectDefinesInternal.Add (warning); }
				if(_kProjectDefinesInternal.Find ((x)=>{ return x == error && (x.Enabled = true) && (x.Groups = -1) == -1;}) == null) { _kProjectDefinesInternal.Add (error); }
				_kProjectDefinesInternal.Remove (debug);
				break;

			case 4:
				if(_kProjectDefinesInternal.Find ((x)=>{ return x == info && (x.Enabled = true) && (x.Groups = -1) == -1;}) == null) { _kProjectDefinesInternal.Add (info); }
				if(_kProjectDefinesInternal.Find ((x)=>{ return x == warning && (x.Enabled = true) && (x.Groups = -1) == -1;}) == null) { _kProjectDefinesInternal.Add (warning); }
				if(_kProjectDefinesInternal.Find ((x)=>{ return x == error && (x.Enabled = true) && (x.Groups = -1) == -1;}) == null) { _kProjectDefinesInternal.Add (error); }
				if(_kProjectDefinesInternal.Find ((x)=>{ return x == debug && (x.Enabled = true) && (x.Groups = -1) == -1;}) == null) { _kProjectDefinesInternal.Add (debug); }
				break;
			}
		}

		_scrollPos = GUILayout.BeginScrollView (_scrollPos);
		if(defs.Count > 0)
		{
			GUILayout.Label ("Project Defines");
			for(int i = defs.Count - 1; i >= 0; --i)
			{
				if(DrawDefineField (defs[i], true))
				{
					_kProjectDefinesInternal.RemoveAt (i);
				}
			}

			GUILayout.Space (20);
		}

		List<Define> fileDefs = _kFileDefinesInternal;
		if(fileDefs.Count > 0)
		{
			GUILayout.Label ("'.Def' File Defines");
			foreach(Define d in fileDefs)
			{
				DrawDefineField (d, false);
			}

			GUILayout.Space (20);
		}
		GUILayout.EndScrollView ();

		GUILayout.BeginHorizontal ();
		if(GUILayout.Button ("Save Defines"))
		{
			SaveDefines ();
		}

		if(GUILayout.Button ("Open Player Settings"))
		{
			EditorApplication.ExecuteMenuItem("Edit/Project Settings/Player");
		}
		GUILayout.EndHorizontal ();
	}

	private static bool DrawDefineField(Define define, bool canBeModified)
	{
		GUILayout.BeginHorizontal ();

		if(canBeModified)
		{
			bool enabled = GUILayout.Toggle (define.Enabled, GUIContent.none, GUILayout.Width (15));
			if(enabled != define.Enabled){ define.Enabled = enabled;  }
			define.DefineString = GUILayout.TextField (define.DefineString);
			define.Groups = EditorGUILayout.MaskField (define.Groups, PlatformValues, GUILayout.Width (100));
			if(GUILayout.Button ("-", GUILayout.Width (20)))
			{
				return true;
			}
		}
		else
		{
			GUILayout.TextField (define.DefineString);
			EditorGUILayout.MaskField (define.Groups, PlatformValues, GUILayout.Width (100));
		}
		GUILayout.EndHorizontal ();

		return false;
	}

	public static void AddDefine(Define newDef)
	{
		if(!_kProjectDefinesInternal.Contains (newDef))
		{
			_kProjectDefinesInternal.Add (newDef);
		}
	}

	public static void SaveDefines()
	{
		SimpleLogger.LogTrace(string.Format("HiberDefiner: SaveDefines - IN"));
		CleanupDefines ();

		string oldContent = null;
		if (File.Exists(kProjectDefinePath))
		{
			oldContent = File.ReadAllText (kProjectDefinePath);
		}
		string fileContent = "";
		foreach(Define d in ProjectDefines)
		{
			fileContent += d.ToString () + "\n";
		}

        if(!fileContent.Equals(oldContent))
		{
			File.WriteAllText (kProjectDefinePath, fileContent);
		}

        // Clear the settings, this actully will force a recompile of CS files, since defines have been wiped, it produces false negatives
        /*
		foreach(string platformString in PlatformValues)
		{
            try
            {
                Build.LogDebug(string.Format("Clearing platform [{0}]", platformString));
                BuildTargetGroup targetGroup = Define.ParseTargetGroup (platformString);

                PlayerSettings.SetScriptingDefineSymbolsForGroup(targetGroup, "");
                //Build.LogDebug(string.Format("Cleared platform [{0}]", platformString));
            }
            catch (Exception ex)
            {
                // Should not happen, it displays an error internally
                Build.LogException(ex);
            }
		}
		*/
        BuildTarget activeBuildTarget = EditorUserBuildSettings.activeBuildTarget;

		Dictionary<BuildTargetGroup, string> playerSettingsDefines = new Dictionary<BuildTargetGroup, string>();
		List<Define> allDefs = AllDefines;
		foreach(Define d in allDefs)
		{
			if(!d.Enabled){ continue; }

			string[] targets = d.Platforms;
			foreach(string s in targets)
			{
				BuildTargetGroup group = Define.ParseTargetGroup (s);
				if(playerSettingsDefines.ContainsKey (group))
				{
					playerSettingsDefines[group] += ";" + d.DefineString;
				}
				else
				{
					playerSettingsDefines.Add (group, d.DefineString);
				}
			}
		}
		
		SimpleLogger.LogDebug(string.Format("Checking defines for current platform [{0}]", activeBuildTarget.ToString()));

		foreach(KeyValuePair<BuildTargetGroup, string> kvp in playerSettingsDefines)
		{
			SimpleLogger.LogDebug(string.Format("Checking defines for group [{0}]", kvp.Key));

			// TODO: Might need hack for iOS->[iOS|iPhone] / tvOS->[iOS] | / tvOS->[tvOS] <<< all apple stuff
			string groupString = kvp.Key.ToString();
			bool isInGroup = activeBuildTarget.ToString().Contains(groupString);

			if (activeBuildTarget.ToString().Equals("iOS") || activeBuildTarget.ToString().Equals("iPhone") || activeBuildTarget.ToString().Equals("tvOS"))
			{
                // Ignoring tvOS group for now, since it it not fully supported, yet - JM
				isInGroup = groupString.Equals("iOS") || groupString.Equals("iPhone");
			}

			// We now need to check for the setting since we will get an error from Unity if the compoenents for that platform is not installed.
			bool needsSetting = isInGroup;
            if (needsSetting) 
            {
                try
                {
					SimpleLogger.LogDebug(string.Format("Setting defines for group [{0}] to [{1}]", kvp.Key, kvp.Value));
                    PlayerSettings.SetScriptingDefineSymbolsForGroup (kvp.Key, kvp.Value);
                    //Build.LogDebug(string.Format("Set defines for [{0}] to [{1}]", kvp.Key, kvp.Value));
                }
                catch (Exception ex)
                {
					SimpleLogger.LogDebug(string.Format("Exception while processing defines for [{0}] to [{1}], this is not an issues for other platforms!", kvp.Key, kvp.Value));
                    // Should not happen, it displays an error internally
					SimpleLogger.LogException(ex);
                }
            }
            else
            {
				SimpleLogger.LogDebug(string.Format("Skipping defines for group [{0}]", kvp.Key));
            }
		}

		_kProjectDefinesAreLoaded = false;
		_kFileDefinesLoaded = false;
		SimpleLogger.LogTrace(string.Format("HiberDefiner: SaveDefines - OUT"));
	}

	private static void LoadProjectDefines()
	{
		SimpleLogger.LogTrace(string.Format("HiberDefiner: LoadProjectDefines - IN"));

		_kProjectDefinesInternal = new List<Define>();

		if(!File.Exists (kProjectDefinePath)){ _kProjectDefinesAreLoaded = true; return; }

		string[] lines = File.ReadAllLines (kProjectDefinePath);
		for(int i = 0; i < lines.Length; ++i)
		{
			if(string.IsNullOrEmpty (lines[i])){ continue; }

			_kProjectDefinesInternal.Add (new Define(lines[i]));
		}

		CleanupDefines ();

		_kProjectDefinesInternal.Sort ((Define left, Define right)=>{
			return -string.Compare (left.DefineString, right.DefineString, true);
		});

        //TODO: Since opt out changes, the defines have changed
		if(_kProjectDefinesInternal.Contains (new Define("LOGGING_ERROR"))){ _logLevel = 3; }
		else if(_kProjectDefinesInternal.Contains (new Define("LOGGING_WARNING"))){ _logLevel = 2; }
		else if(_kProjectDefinesInternal.Contains (new Define("LOGGING_INFO"))){ _logLevel = 1; }
		else { _logLevel = 0; }

		_kProjectDefinesAreLoaded = true;
		SimpleLogger.LogTrace(string.Format("HiberDefiner: LoadProjectDefines - OUT"));
	}

	private static void CleanupDefines()
	{
		List<string> defines = new List<string>();

		for(int i = _kProjectDefinesInternal.Count - 1, min = 0; i >= min; --i)
		{
			if(string.IsNullOrEmpty (_kProjectDefinesInternal[i].DefineString) ||
			   defines.Contains (_kProjectDefinesInternal[i].DefineString))
			{
				_kProjectDefinesInternal.RemoveAt (i);
			}
			else
			{
				defines.Add (_kProjectDefinesInternal[i].DefineString);
			}
		}
	}
	
	private static void LoadFileDefines()
	{
		LoadFileDefines (AssetDatabase.GetAllAssetPaths ());
	}

	private static void LoadFileDefines(string[] assets)
	{
		SimpleLogger.LogTrace(string.Format("HiberDefiner: LoadFileDefines - IN"));
		_kFileDefinesInternal = new List<Define>();

		for(int i = 0, max = assets.Length; i < max; ++i)
		{
			if(Path.GetExtension (assets[i]) == ".def")
			{
				_kFileDefinesInternal.AddRange (ParseDefineFile (assets[i]));
			}
		}

		// Order defines alphabetically.
		_kFileDefinesInternal.Sort ((Define left, Define right)=>{
			return string.Compare (left.DefineString, right.DefineString, true);
		});

		_kFileDefinesLoaded = true;
		SimpleLogger.LogTrace(string.Format("HiberDefiner: LoadFileDefines - OUT"));
	}

	public static void LoadDefines()
	{
		SimpleLogger.LogTrace(string.Format("HiberDefiner: LoadDefines - IN"));
		LoadProjectDefines ();
		LoadFileDefines ();
		SimpleLogger.LogTrace(string.Format("HiberDefiner: LoadDefines - OUT"));
	}

	private static Define[] ParseDefineFile(string assetPath)
	{
		string[] lines = File.ReadAllLines (assetPath);
		List<Define> readDefines = new List<Define>();
		for(int i = 0; i < lines.Length; ++i)
		{
			readDefines.Add (new Define(lines[i]));
		}

		return readDefines.ToArray ();
	}

	public static void UpdateDefines(Dictionary<string, bool> updateData)
	{
		if(updateData == null){ return; }

		List<string> keys = new List<string>(updateData.Keys);
		for(int i = 0, max = keys.Count; i < max; ++i)
		{
			Define newDef = new Define(keys[i] + "|All");
			_kProjectDefinesInternal.Remove (newDef);			// Remove def in case it was already there.

			if(updateData[keys[i]])
			{
				AddDefine (newDef);			// Re-add the define if we want it; This makes sure it is enabled and present on all platforms.
			}
		}

		SaveDefines ();
	}

	public class Define
	{
		private int _groups;
		private string _define;
		private bool _enabled;

		public string[] Platforms
		{
			get
			{
				List<string> platforms = new List<string>();

				for(int i = 0; i < PlatformValues.Length; ++i)
				{
					if((_groups & (1 << i)) == (1 << i))
					{
						platforms.Add (PlatformValues[i]);
					}
				}

				return platforms.ToArray ();
			}
		}

		public int Groups
		{
			get{ return _groups; }
			set{ _groups = value; }
		}

		public string DefineString
		{
			get{ return _define; }
			set{ _define = value ?? string.Empty; }
		}

		public bool Enabled
		{
			get{ return _enabled; }
			set{ _enabled = value; }
		}

		public Define()
		{
			_groups = 0;
			_define = string.Empty;
			_enabled = true;
		}

		public Define(string data)
		{
			string[] splitData = data.Split ('|');
			_groups = 0;
			_define = splitData[0];
			_enabled = true;

			for(int i = 1, max = splitData.Length; i < max; ++i)
			{
				if(splitData[i] == "DISABLED"){ _enabled = false; continue; }

				bool allPlatforms = splitData[i] == "All";

				for(int j = 0; j < DefinerPreferences.PlatformValues.Length; ++j)
				{
					if(allPlatforms || DefinerPreferences.PlatformValues[j] == splitData[i])
					{
						_groups |= (1 << j);
					}
				}
			}
		}
		
		public static BuildTargetGroup ParseTargetGroup (string stringGroup)
		{
			return (BuildTargetGroup)System.Enum.Parse(typeof(BuildTargetGroup), stringGroup);
		}

		public override string ToString ()
		{
			string final = _define;

			for(int i = 0, max = PlatformValues.Length; i < max; ++i)
			{
				string target = PlatformValues[i];

				if((_groups & (1 << i)) == (1 << i))
				{
					final += "|" + target;
				}
			}

			if(!Enabled)
			{
				final += "|DISABLED";
			}

			return final;
		}

		public override bool Equals (object obj)
		{
			if (object.ReferenceEquals (null, obj) ||
                !(obj is Define))
            {
                return false;
            }
			return DefineString.Equals ((obj as Define).DefineString);
		}

        public override int GetHashCode()
        {
            return DefineString.GetHashCode();
        }

		public static bool operator != (Define left, Define right) { return !(left == right); }
		public static bool operator == (Define left, Define right)
		{
			if(object.ReferenceEquals (null, left))
			{
				if(object.ReferenceEquals (null, right)) { return true; }
				return false;
			}

			return left.Equals (right);
		}
	}
}

// Define wizard to find every scripting symbol in the project.
public class DefineFinderWizard : ScriptableWizard
{
	private static DefinerPreferences.Define[] _defines = null;
	private int _targetGroups;
	private bool _scanning = false;
	private float _scanProgress = 0f;
	private string _scanningText = "";
	private Vector2 _scrollPos;
	
	public static void OpenWizard()
	{
		DefineFinderWizard wiz = ScriptableWizard.DisplayWizard<DefineFinderWizard>("Defines Import", "Import Defines", "Cancel");
		wiz.minSize = new Vector2(400, 500);

		wiz.position = new Rect((Screen.currentResolution.width / 2) - 200, (Screen.currentResolution.height / 2) - 250, 400, 500);
		EditorCoroutineManager.StartCoroutine (wiz.Init ());
	}
	
	private IEnumerator Init()
	{
		// Check all files.
		isValid = false;
		_scanning = true;

		if(_defines == null)
		{
			HashSet<string> definesFound = new HashSet<string>();
			List<string> paths = new List<string>(AssetDatabase.GetAllAssetPaths ());
			paths.RemoveAll ((x)=>{ return Path.GetExtension (x) != ".cs";});

			System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
			watch.Start ();
			for(int i = 0, max = paths.Count; i < max; ++i)
			{
				_scanProgress = (float)i/(float)max;
				_scanningText = string.Format ("Scanning file: {0}", Path.GetFileName (paths[i]));
				if(watch.ElapsedMilliseconds > 15f/1f)
				{
					yield return null;
					watch.Reset ();
					watch.Start ();
				}

				if(Path.GetExtension (paths[i]) == ".cs")
				{
					StreamReader stream = new StreamReader(paths[i]);
					//						\[\s*Conditional\s*\(\s*\"(?'conditionals'.+?)(?:\"\s*\)\s*\])|(?'true'^\s*#if|^\s*#elif)(?(true)(?:[\r\t\f !()&|]*(?'defines'[a-zA-Z0-9_]*))*)
					string regexPattern = @"\[\s*Conditional\s*\(\s*\" + "\"" + @"(?'conditionals'.+?)(?:\" + "\"" + @"\s*\)\s*\])|(?'true'^\s*#if|^\s*#elif)(?(true)(?:[\r\t\f !()&|]*(?'defines'[a-zA-Z0-9_]*))*)";
					Regex regex = new Regex(regexPattern, RegexOptions.Multiline);

					MatchCollection matches = regex.Matches (stream.ReadToEnd ());

					foreach(Match match in matches)
					{
						foreach(var v in match.Groups["defines"].Captures)
						{
							if(string.IsNullOrEmpty (v.ToString())){ continue; }
							definesFound.Add (v.ToString ());
						}

						string conditional = ""; 
						if(!string.IsNullOrEmpty (conditional = match.Groups["conditionals"].Value))
						{
							definesFound.Add (conditional);
						}
					}
				}
			}
			watch.Stop ();

			// Ignore all currently added defines.
			List<DefinerPreferences.Define> allDefs = DefinerPreferences.AllDefines;
			for(int i = 0, max = allDefs.Count; i < max; ++i)
			{
				definesFound.Remove (allDefs[i].DefineString);
			}

			// Remove these defines because they are internally defined by Unity.
			string[] ignoreDefines = new string[]{
				"DEBUG",
				"UNITY_ANDROID",
				"UNITY_BLACKBERRY",
				"UNITY_EDITOR",
				"UNITY_FLASH",
				"UNITY_IOS",
				"UNITY_IPHONE",
				"UNITY_METRO",
				"UNITY_NACL",
				"UNITY_PS3",
				"UNITY_STANDALONE",
				"UNITY_STANDALONE_LINUX",
				"UNITY_STANDALONE_OSX",
				"UNITY_STANDALONE_WIN",
				"UNITY_WINRT",
				"UNITY_WP8",
				"UNITY_WEBPLAYER",
				"UNITY_2_6",
				"UNITY_2_6_1",
				"UNITY_3_0_0",
				"UNITY_3_0",
				"UNITY_3_1",
				"UNITY_3_2",
				"UNITY_3_3",
				"UNITY_3_4",
				"UNITY_3_5",
				"UNITY_4",
				"UNITY_4_0",
				"UNITY_4_0_1",
				"UNITY_4_1",
				"UNITY_4_2",
				"UNITY_4_3",
				"UNITY_4_5",
				#if UNITY_5
				"UNITY_5",
				"UNITY_5_0",
				"UNITY_5_0_0",
				#endif
                //TODO: Since opt out changes, the defines have changed
				#if HIBER_DEBUGGER_LIB
				"LOGGING_INFO",
				"LOGGING_WARNING",
				"LOGGING_ERROR",
				#endif
			};

			for(int i = 0, max = ignoreDefines.Length; i < max; ++i)
			{
				definesFound.Remove (ignoreDefines[i]);
			}

			List<string> definesList = new List<string>(definesFound);
			definesList.Sort ((string left, string right)=>{ return string.Compare (left, right, true); });
			_defines = new DefinerPreferences.Define[definesFound.Count];

			for(int i = 0, max = definesList.Count; i < max; ++i)
			{
				_defines[i] = new DefinerPreferences.Define(definesList[i] + "|DISABLED");
			}
		}
		_scanning = false;
		EditorUtility.ClearProgressBar ();
		Focus ();

		yield break;
	}
	
	private void OnGUI()
	{
		_targetGroups = EditorGUILayout.MaskField ("Target platforms", _targetGroups, DefinerPreferences.PlatformValues);
		GUILayout.BeginVertical ();
		_scrollPos = GUILayout.BeginScrollView (_scrollPos, "TextField");
		if(_defines == null)
		{
			GUILayout.Label ("Scanning for defines.");
		}
		else
		{
			for(int i = 0, max = _defines.Length; i < max; ++i)
			{
				GUILayout.BeginHorizontal ();
				_defines[i].Enabled = GUILayout.Toggle (_defines[i].Enabled, GUIContent.none, GUILayout.Width (15));
				GUILayout.Label (_defines[i].DefineString);
				GUILayout.EndHorizontal ();
			}
		}
		GUILayout.EndScrollView ();
		GUILayout.EndVertical ();

		GUILayout.FlexibleSpace();

		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		if(GUILayout.Button ("Import"))
		{
			OnWizardCreate ();
		}

		if(GUILayout.Button ("Cancel"))
		{
			OnWizardOtherButton ();
		}
		GUILayout.EndHorizontal ();
	}

	private void OnInspectorUpdate()
	{
		if(_scanning)
		{
			EditorApplication.update();
			EditorUtility.DisplayProgressBar ("Scanning for defines", _scanningText, _scanProgress);
		}
	}
	
	private void OnWizardCreate()
	{
		if(_scanning)
		{
			ShowNotification (new GUIContent("Wait for scan to be completed."));
			return;
		}

		bool added = false;
		for(int i = 0, max = _defines.Length; i < max; ++i)
		{
			if(_defines[i].Enabled)
			{
				added = true;
				_defines[i].Groups = _targetGroups;
				DefinerPreferences.AddDefine (_defines[i]);
			}
		}

		if(!added)
		{
			ShowNotification (new GUIContent("Select at least one define to add."));
			return;
		}

		DefinerPreferences.SaveDefines ();
		Close ();
	}

	private void OnLostFocus()
	{
		if(!_scanning)
		{
			Close ();
		}
	}
	
	private void OnWizardOtherButton()
	{
		if(_scanning)
		{
			_scanning = false;
			EditorUtility.ClearProgressBar ();
		}
		Close ();
	}
}