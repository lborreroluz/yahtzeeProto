﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(MinMaxInt))]
[CustomPropertyDrawer(typeof(MinMaxFloat))]
public class MinMaxDrawer : PropertyDrawer
{
	private SerializedProperty _minProperty;
	private SerializedProperty _maxProperty;

	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		position = EditorGUI.PrefixLabel(position, label);
		Rect val1Rect = new Rect(position.x, position.y, position.width / 2, position.height);
		Rect val2Rect = new Rect(position.x + position.width / 2, position.y, position.width / 2, position.height);

		float lastWidth = EditorGUIUtility.labelWidth;
		EditorGUIUtility.labelWidth = 30;

		if(null == _minProperty){
			_minProperty = property.FindPropertyRelative("min");
		}

		if(null == _maxProperty){
			_maxProperty = property.FindPropertyRelative("max");
		}

		Color col = GUI.backgroundColor;

		switch(_minProperty.propertyType){

		// Display the fields in red if the max is smaller or equal to the min
		case SerializedPropertyType.Float:
			if(_minProperty.floatValue > _maxProperty.floatValue){
				GUI.backgroundColor = Color.red;
			}
			break;

		case SerializedPropertyType.Integer:
			if(_minProperty.intValue > _maxProperty.intValue){
				GUI.backgroundColor = Color.red;
			}
			break;
		}

		EditorGUI.PropertyField(val1Rect, _minProperty, new GUIContent ("Min"));
		EditorGUI.PropertyField(val2Rect, _maxProperty, new GUIContent ("Max"));

		GUI.backgroundColor = col;
		EditorGUIUtility.labelWidth = lastWidth;
	}
}
