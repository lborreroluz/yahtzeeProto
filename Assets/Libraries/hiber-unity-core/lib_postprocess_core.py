#!/usr/bin/python

import os
import sys

import hiber.build.ConfigData
import hiber.build.ConfigKeys

def main():
    scriptDirPath = os.path.dirname(os.path.realpath(__file__))
    libId = os.path.basename(scriptDirPath)
    
    configData = hiber.build.ConfigData.load_finalized_config_data()

    if "iOS" == configData[hiber.build.ConfigKeys.clientUnityAppBuildTargetPlatformKey]:
        srcPluginsPath = os.path.join(scriptDirPath, "Plugins")
        hiber.build.ConfigData.sync_plugins_to_xcode_project(configData, aLibId=libId, aSrcPluginsPath=srcPluginsPath)
       
if __name__ == "__main__":
    logger = hiber.Util.create_logger(__name__)

    exitCode = 0

    try:
        main()
                
    except Exception as ex:
        exitCode = 2

        logger.fatal("Exception occured!")
        logger.exception(ex)

    except:
        exitCode = 3

        logger.fatal("Un-caught problem occured!")
        logger.fatal("{}".format(sys.exc_info()))
                
    finally:            
        logger.info("Exiting with code [{}]".format(exitCode))
        
    sys.exit(exitCode)
