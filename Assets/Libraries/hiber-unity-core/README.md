Hibernum Core
=============
**Owner:** [Patrice Michaud](mailto:pmichaud@hibernum.com)

Description
-----------
A collection of scripts which add core functionality for all Hibernum products.
