﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Xml;
using System.Xml.Serialization;
using System.Text; 

namespace Hibernum.Core
{
    /// <summary>
    /// Allows the runtime selection of specific data serializers.
    /// </summary>
    public static class Serializer
    {
		static IJsonSerializationAdapter _adapter;

		/// <summary>
		/// Sets the json serialization adapter to be used.
		/// </summary>
		/// <param name="adapter">An instance of <c>IJsonSerializationAdapter</c></param>
		public static void SetJsonSerializationAdapter(IJsonSerializationAdapter adapter)
		{
			CoreAssert.Fatal(adapter != null, "Can't set a null adapter.");

			_adapter = adapter;
		}

		// TODO: Include a default adapter (in core) to make Serializer useable out of the box.
		/// <summary>
		/// Wether or not a JSON serialization adapter was set
		/// </summary>
		/// <value><c>true</c> if a JSON serialization adapter was set; otherwise, <c>false</c>.</value>
		public static bool HasJsonAdapter { get { return _adapter != null; } }

        /// <summary>
        /// Encodes the specified object to string format.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string JsonEncode(object data)
        {
			CoreAssert.Fatal(HasJsonAdapter, "A JSON adapter needs to be specified.");

			return _adapter.JsonEncode(data);
        }

		/// <summary>
		/// Encodes the specified object to string format, using the specified adapter.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static string JsonEncode(object data, IJsonSerializationAdapter adapter)
		{
			CoreAssert.Fatal(adapter != null, "Specified JSON adapter can't be null.");
			
			return adapter.JsonEncode(data);
		}
		
		/// <summary>
		/// Decodes the specified string to base object.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static object JsonDecode(string data)
        {
			CoreAssert.Fatal(HasJsonAdapter, "A JSON adapter needs to be specified.");

			return _adapter.JsonDecode(data);
        }

		/// <summary>
		/// Decodes the specified string to base object.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static object JsonDecode(string data, IJsonSerializationAdapter adapter)
		{
			CoreAssert.Fatal(adapter != null, "Specified JSON adapter can't be null.");
			
			return adapter.JsonDecode(data);
		}
		
		/// <summary>
		/// Decodes the specified string to the required type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
		public static T JsonDecode<T>(string data)
        {
			CoreAssert.Fatal(HasJsonAdapter, "A JSON adapter needs to be specified.");
			
			return _adapter.JsonDecode<T>(data);
        }

		/// <summary>
		/// Decodes the specified string to the required type.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="data"></param>
		/// <returns></returns>
		public static T JsonDecode<T>(string data, IJsonSerializationAdapter adapter)
		{
			CoreAssert.Fatal(adapter != null, "Specified JSON adapter can't be null.");
			
			return adapter.JsonDecode<T>(data);
		}
		
		/// <summary>
		/// Xmls the encode.
		/// </summary>
		/// <returns>The encode.</returns>
		/// <param name="data">Data.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static string XmlEncode<T>(T data)
		{ 	
			string xml = null; 
			using (StringWriter writer = new StringWriter()) {
				XmlSerializer serializer = new XmlSerializer(typeof(T)); 
				serializer.Serialize(new XmlTextWriter(writer), data); 
				xml = writer.ToString(); 
			}
			return xml;
		}

		/// <summary>
		/// Xmls the decode.
		/// </summary>
		/// <returns>The decode.</returns>
		/// <param name="data">Data.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static T XmlDecode<T>(string data)
		{	
			T obj;
			using (StringReader reader = new StringReader(data)) {
				XmlSerializer serializer = new XmlSerializer(typeof(T));
				
				obj = (T)serializer.Deserialize(new XmlTextReader(reader)); 
			}
			return obj;
		}
		
    }
}