#if !UNITY_EDITOR

using UnityEngine;

using System;
using System.Diagnostics;

/// <summary>
/// This class is intended to hijack UnityEngine.Debug when it isn't used with a full qualification. It also has a few
/// additional features. These will eventually be moved elsewhere and this hijack will be removed.
/// </summary>
public static class Debug
{
	#region Opt-Out Mechanism

	private const string kFalse = "FALSE";

#if UNITY_4_3
	private const string kTrue = "UNITY_4_3";
#elif UNITY_4_5
	private const string kTrue = "UNITY_4_5";
#elif UNITY_4_6
	private const string kTrue = "UNITY_4_6";
#elif UNITY_5
	private const string kTrue = "UNITY_5";
#else
#error Unsupported Unity Version
#endif

#if LOG_INFO_DISABLED
	private const string kInfoEnabled = kFalse;
#else
	private const string kInfoEnabled = kTrue;
#endif

#if LOG_WARNING_DISABLED
	private const string kWarningEnabled = kFalse;
#else
	private const string kWarningEnabled = kTrue;
#endif

#if LOG_ERROR_DISABLED
	private const string kErrorEnabled = kFalse;
#else
	private const string kErrorEnabled = kTrue;
#endif

#if LOG_DEBUG_DISABLED
	private const string kDebugEnabled = kFalse;
#else
	private const string kDebugEnabled = kTrue;
#endif

#if ASSERT_DISABLED
	private const string kAssertEnabled = kFalse;
#else
	private const string kAssertEnabled = kTrue;
#endif

	#endregion Opt-Out Mechanism

	#region Custom Content

	[Obsolete("Talk to Sean/Phil for details on how to fix this warning.")]
	public delegate void LogCallback(string message, string stackTrace, LogType type);

	[Obsolete("Talk to Sean/Phil for details on how to fix this warning.")]
	public static LogCallback logCallback;

	static Debug()
	{
#if UNITY_5
		Application.logMessageReceived += HandleUnityLogCallback;
#elif OVERRIDE_LOG_CALLBACK
		Application.RegisterLogCallback(HandleUnityLogCallback);
#endif
	}

	static void HandleUnityLogCallback(string message, string stackTrace, LogType type)
	{
#pragma warning disable 0618
		if (null != logCallback)
		{
			logCallback(message, stackTrace, type);
		}
#pragma warning restore 0618
	}

	[Obsolete("Use LogError instead.")]
	[Conditional(kErrorEnabled)]
	public static void LogFatal(object message, UnityEngine.Object context = null)
	{
		UnityEngine.Debug.LogError(message, context);
	}

	[Obsolete("Talk to James for details on how to fix this warning.")]
	[Conditional(kDebugEnabled)]
	public static void LogDebug(object message, UnityEngine.Object context = null)
	{
		Console.WriteLine("DEBUG: " + message);

#pragma warning disable 0618
		if (null != logCallback)
		{
			logCallback(message.ToString(), StackTraceUtility.ExtractStackTrace(), LogType.Log);
		}
#pragma warning restore 0618
	}

#if !UNITY_5
	[Obsolete("Use LogError and string.Format instead.")]
	[Conditional(kErrorEnabled)]
	public static void LogErrorFormat(string message, params object[] args)
	{
		UnityEngine.Debug.LogError(string.Format (message, args));
	}
#endif

#if !UNITY_5
	[Obsolete("Use LogError and string.Format instead.")]
	[Conditional(kErrorEnabled)]
	public static void LogErrorFormat(UnityEngine.Object context, string message, params object[] args)
	{
		UnityEngine.Debug.LogError(string.Format (message, args));
	}
#endif

#if !UNITY_5
	[Obsolete("Use Log instead and string.Format instead.")]
	[Conditional(kInfoEnabled)]
	public static void LogFormat(string message, params object[] args)
	{
		UnityEngine.Debug.Log(string.Format (message, args));
	}
#endif

#if !UNITY_5
	[Obsolete("Use Log and string.Format instead.")]
	[Conditional(kInfoEnabled)]
	public static void LogFormat(UnityEngine.Object context, string message, params object[] args)
	{
		UnityEngine.Debug.Log(string.Format (message, args));
	}
#endif

#if !UNITY_5
	[Obsolete("Use LogWarning and string.Format instead.")]
	[Conditional(kWarningEnabled)]
	public static void LogWarningFormat(string message, params object[] args)
	{
		UnityEngine.Debug.LogWarning(string.Format (message, args));
	}
#endif

#if !UNITY_5
	[Obsolete("Use LogWarning with string.Format instead.")]
	[Conditional(kWarningEnabled)]
	public static void LogWarningFormat(UnityEngine.Object context, string message, params object[] args)
	{
		UnityEngine.Debug.LogWarning(string.Format (message, args));
	}
#endif

	#endregion Custom Content

	#region UnityEngine.Debug Content

	public static bool developerConsoleVisible
	{
		get
		{
			return UnityEngine.Debug.developerConsoleVisible;
		}
		set
		{
			UnityEngine.Debug.developerConsoleVisible = value;
		}
	}

	public static bool isDebugBuild
	{
		get
		{
			return UnityEngine.Debug.isDebugBuild;
		}
	}

#if UNITY_5
	[Conditional(kAssertEnabled)]
	public static void Assert(bool condition)
	{
		UnityEngine.Debug.Assert(condition);
	}

	[Conditional(kAssertEnabled)]
	public static void Assert(bool condition, string message)
	{
		UnityEngine.Debug.Assert(condition, message);
	}

	[Conditional(kAssertEnabled)]
	public static void Assert(bool condition, string format, params object[] args)
	{
		#if UNITY_5_3_OR_NEWER
		UnityEngine.Debug.AssertFormat(condition, format, args);
		#else
		UnityEngine.Debug.Assert(condition, format, args);
		#endif
	}
#endif

	[Conditional(kInfoEnabled)]
	public static void Break()
	{
		UnityEngine.Debug.Break();
	}

	public static void ClearDeveloperConsole()
	{
		UnityEngine.Debug.ClearDeveloperConsole();
	}

	public static void DebugBreak()
	{
		UnityEngine.Debug.DebugBreak();
	}

	[Conditional(kInfoEnabled)]
	public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration, bool depthTest)
	{
		UnityEngine.Debug.DrawLine(start, end, color, duration, depthTest);
	}

	[Conditional(kInfoEnabled)]
	public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration)
	{
		UnityEngine.Debug.DrawLine(start, end, color, duration);
	}

	[Conditional(kInfoEnabled)]
	public static void DrawLine(Vector3 start, Vector3 end, Color color)
	{
		UnityEngine.Debug.DrawLine(start, end, color);
	}

	[Conditional(kInfoEnabled)]
	public static void DrawLine(Vector3 start, Vector3 end)
	{
		UnityEngine.Debug.DrawLine(start, end);
	}

	[Conditional(kInfoEnabled)]
	public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration)
	{
		UnityEngine.Debug.DrawRay(start, dir, color, duration);
	}

	[Conditional(kInfoEnabled)]
	public static void DrawRay(Vector3 start, Vector3 dir, Color color)
	{
		UnityEngine.Debug.DrawRay(start, dir, color);
	}

	[Conditional(kInfoEnabled)]
	public static void DrawRay(Vector3 startPos, Vector3 value)
	{
		UnityEngine.Debug.DrawRay(startPos, value);
	}

	[Conditional(kInfoEnabled)]
	public static void DrawRay(Vector3 startPos, Vector3 value, Color value2, float duration, bool depthTest)
	{
		UnityEngine.Debug.DrawRay(startPos, value, value2, duration, depthTest);
	}

	[Conditional(kInfoEnabled)]
	public static void Log(object message)
	{
		UnityEngine.Debug.Log(message);
	}

	[Conditional(kInfoEnabled)]
	public static void Log(object message, UnityEngine.Object context)
	{
		UnityEngine.Debug.Log(message, context);
	}

	[Conditional(kErrorEnabled)]
	public static void LogError(object message)
	{
		UnityEngine.Debug.LogError(message);
	}

	[Conditional(kErrorEnabled)]
	public static void LogError(object message, UnityEngine.Object context)
	{
		UnityEngine.Debug.LogError(message, context);
	}

#if UNITY_5
	[Conditional(kErrorEnabled)]
	public static void LogErrorFormat(string message, params object[] args)
	{
		UnityEngine.Debug.LogErrorFormat(message, args);
	}
#endif

#if UNITY_5
	[Conditional(kErrorEnabled)]
	public static void LogErrorFormat(UnityEngine.Object context, string message, params object[] args)
	{
		UnityEngine.Debug.LogErrorFormat(context, message, args);
	}
#endif

	[Conditional(kErrorEnabled)]
	public static void LogException(Exception exception)
	{
		UnityEngine.Debug.LogException(exception);
	}

	[Conditional(kErrorEnabled)]
	public static void LogException(Exception exception, UnityEngine.Object context)
	{
		UnityEngine.Debug.LogException(exception, context);
	}

#if UNITY_5
	[Conditional(kInfoEnabled)]
	public static void LogFormat(string message, params object[] args)
	{
		UnityEngine.Debug.LogFormat(message, args);
	}
#endif

#if UNITY_5
	[Conditional(kInfoEnabled)]
	public static void LogFormat(UnityEngine.Object context, string message, params object[] args)
	{
		UnityEngine.Debug.LogFormat(context, message, args);
	}
#endif

	[Conditional(kWarningEnabled)]
	public static void LogWarning(object message)
	{
		UnityEngine.Debug.LogWarning(message);
	}

	[Conditional(kWarningEnabled)]
	public static void LogWarning(object message, UnityEngine.Object context)
	{
		UnityEngine.Debug.LogWarning(message, context);
	}

#if UNITY_5
	[Conditional(kWarningEnabled)]
	public static void LogWarningFormat(string message, params object[] args)
	{
		UnityEngine.Debug.LogWarningFormat(message, args);
	}
#endif

#if UNITY_5
	[Conditional(kWarningEnabled)]
	public static void LogWarningFormat(UnityEngine.Object context, string message, params object[] args)
	{
		UnityEngine.Debug.LogWarningFormat(context, message, args);
	}
#endif

	#endregion UnityEngine.Debug Content
}

#endif