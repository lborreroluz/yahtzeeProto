﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct MinMaxFloat 
{
	public float min;
	public float max;

	public float Value { get { return Random.Range(min,max); } }

	public static implicit operator float(MinMaxFloat val)
	{
		return val.Value;
	}
}

[System.Serializable]
public struct MinMaxInt
{
	public int min;
	public int max;

	public int Value { get { return Random.Range(min,max+1); } }

	public static implicit operator int(MinMaxInt val)
	{
		return val.Value;
	}
}
