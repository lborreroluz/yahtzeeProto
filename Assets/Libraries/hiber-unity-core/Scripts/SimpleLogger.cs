﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Hibernum.Core
{
	public static class SimpleLogger
	{
		public static void LogException(Exception aException)
		{
			Console.WriteLine("EXCEPTION: " + aException.Message);
			Console.WriteLine("EXCEPTION: " + aException.StackTrace);
		}

		public static void LogError(IEnumerable<Exception> aExceptionList)
		{
			foreach(var ex in aExceptionList)
			{
				LogError(ex.Message);
			}
		}

		public static void LogError(object aMessage)
		{
			Console.WriteLine("ERROR: " + aMessage);
		}

		public static void LogFatal(object aMessage)
		{
			Console.WriteLine("FATAL: " + aMessage);
		}

		public static void LogDebug(object aMessage)
		{
			Console.WriteLine("DEBUG: " + aMessage);
		}

		public static void LogTrace(object aMessage)
		{
			Console.WriteLine("TRACE: " + aMessage);
		}
			
		public static void LogInfo(object aMessage)
		{
			Console.WriteLine("INFO: " + aMessage);
		}
	}
}

