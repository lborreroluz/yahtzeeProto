using System;

namespace Hibernum.Core
{
	/// <summary>
	/// Randomizer offer a few helper functions to get random values all using a common SystemRandom object instance
	/// It offers extra getters over the normal Random albeit with some round inefficiencies due to typecasting
	/// </summary>

	public static class Randomizer
	{
		private static Random _global = new Random();
		[ThreadStatic] 
		private static Random
			_local;

		private static void Init()
		{
			Random inst = _local; 
			if (inst == null)
			{ 
				int seed; 
				lock (_global)
				{
					seed = _global.Next();
				} 
				_local = inst = new Random(seed); 
			} 
		}

		/// <summary>
		/// Returns a non-negative random integer.
		/// </summary>
		/// <returns>Random int between 0 and MAX_INT</returns>
		public static int NextInt()
		{
			Init();
			return _local.Next();

		}

		/// <summary>
		/// Returns a non-negative random integer that is less than the specified maximum
		/// </summary>
		/// <param name="maxValue">Upper bound of random range (excl.)</param>
		/// <returns>Random int between 0 and max excluded</returns>
		public static int NextInt(int maxValue)
		{
			Init();
			return _local.Next(maxValue);
		}

		/// <summary>
		/// Returns a random integer that is within a specified range
		/// </summary>
		/// <param name="minValue">Lower bound of random range</param>
		/// <param name="maxValue">Upper bound of random range (excl.)</param>
		/// <returns>Random int between min and max excluded</returns>
		public static int NextInt(int minValue, int maxValue)
		{
			Init();
			CoreAssert.Warning(minValue <= maxValue, "Min greater than Max in random");
			return _local.Next(minValue, maxValue);
		}

		public static void NextBytes(byte[] buffer)
		{
			Init();
			_local.NextBytes(buffer);
		}

		/// <summary>
		/// Get a random double 0.0 -> 1.0
		/// </summary>
		/// <returns>Random double between 0.0 and 1.0 included</returns>
		public static double NextDouble()
		{
			Init();
			return _local.NextDouble();
		}

		/// <summary>
		/// Get a random float 0.0 -> 1.0
		/// </summary>
		/// <returns>Random float between 0.0 and 1.0 included</returns>
		public static float NextFloatNormalized()
		{
			Init();
			return (float) _local.NextDouble();
		}

		/// <summary>
		/// Returns a non-negative random float (rounded down due to int casting).
		/// </summary>
		/// <returns>Random int between 0 and MAX_INT</returns>
		public static float NextFloat()
		{
			Init();
			return (float) _local.Next();

		}

		/// <summary>
		/// Returns a boolean value 
		/// </summary>
		/// <returns>Random bool</returns>
		public static bool NextBool()
		{
			Init();
			int value = _local.Next(2);

			return value == 0;

		}

		/// <summary>
		/// Returns a weighted boolean value based on percent towards true 
		/// </summary>
		/// <param name="percent">Ratio to provide true</param>
		/// <returns>Random bool</returns>
		public static bool NextBoolWeighted(float percent)
		{
			Init();

			bool isPercent = percent >= 0.0f && percent <= 1.0f;
			CoreAssert.Warning(isPercent, "Percent value (" + percent + ") out of range (0.0f -> 1.0f), defaulting to 50%");
			percent = isPercent ? percent : 0.5f;
				
			float value = NextFloat(100f);
			float target = 100f * percent;
			return value <= target;

		}

		/// <summary>
		/// Returns a non-negative random float that is less than the specified maximum (rounded down due to int casting)
		/// </summary>
		/// <param name="maxValue">Upper bound of random range (excl.)</param>
		/// <returns>Random int between 0 and max excluded</returns>
		public static float NextFloat(float maxValue)
		{
			Init();
			return (float) _local.Next((int) maxValue);

		}
	
		/// <summary>
		/// Returns a random float that is within a specified range (rounded down due to int casting)
		/// </summary>
		/// <param name="minValue">Lower bound of random range</param>
		/// <param name="maxValue">Upper bound of random range (excl.)</param>
		/// <returns>Random float between min and max excluded</returns>
		public static float NextFloat(float minValue, float maxValue)
		{
			Init();

			CoreAssert.Warning(minValue <= maxValue, "Min greater than Max in random");
			return (float) _local.Next((int) minValue, (int) maxValue);

		}
	}

}