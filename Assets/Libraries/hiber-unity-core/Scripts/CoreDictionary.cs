using System.Collections.Generic;

public sealed class CoreDictionary<K,V> : Dictionary<K,V>
{
	public new V this[K key]
	{
		get
		{
			V v = default(V);
			if(!TryGetValue(key, out v))
			{
				Add(key, v);
			}
			return v;
		}
		set
		{
			if(ContainsKey(key))
			{
				base[key] = value;
			}
			else
			{
				Add(key, value);
			}
		}
	}
}