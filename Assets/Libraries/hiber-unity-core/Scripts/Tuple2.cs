using System;

namespace Hibernum.Core
{
	public struct Tuple<T1, T2>
	{
		public Tuple(T1 item1, T2 item2)
		{
			this = new Tuple<T1, T2>();
			this.Item1 = item1;
			this.Item2 = item2;
		}
		
		public T1 Item1 { get; private set; }
		public T2 Item2 { get; private set; }
		
		public override bool Equals(object obj)
		{
			if (obj is Tuple<T1, T2>)
			{
				Tuple<T1, T2> that = (Tuple<T1, T2>)obj;
				return object.Equals(this.Item1, that.Item1) && object.Equals(this.Item2, that.Item2);
			}
			else
			{
				return false;
			}
		}
		
		public override int GetHashCode()
		{
			return ((this.Item1 != null) ? this.Item1.GetHashCode() : 0) ^ ((this.Item2 != null) ? this.Item2.GetHashCode() : 0);
		}
		
		public static bool operator ==(Tuple<T1, T2> left, Tuple<T1, T2> right)
		{
			return left.Equals(right);
		}
		
		public static bool operator !=(Tuple<T1, T2> left, Tuple<T1, T2> right)
		{
			return !left.Equals(right);
		}
	}

	public struct Tuple<T1, T2, T3>
	{
		public Tuple(T1 item1, T2 item2, T3 item3)
		{
			this = new Tuple<T1, T2, T3>();
			this.Item1 = item1;
			this.Item2 = item2;
			this.Item3 = item3;
		}
		
		public T1 Item1 { get; private set; }
		public T2 Item2 { get; private set; }
		public T3 Item3 { get; private set; }
		
		public override bool Equals(object obj)
		{
			if (obj is Tuple<T1, T2, T3>)
			{
				Tuple<T1, T2, T3> that = (Tuple<T1, T2, T3>)obj;
				return object.Equals(this.Item1, that.Item1) && object.Equals(this.Item2, that.Item2) && object.Equals(this.Item3, that.Item3);
			}
			else
			{
				return false;
			}
		}
		
		public override int GetHashCode()
		{
			return ((this.Item1 != null) ? this.Item1.GetHashCode() : 0) ^ ((this.Item2 != null) ? this.Item2.GetHashCode() : 0) ^ ((this.Item3 != null) ? this.Item3.GetHashCode() : 0);
		}
		
		public static bool operator ==(Tuple<T1, T2, T3> left, Tuple<T1, T2, T3> right)
		{
			return left.Equals(right);
		}
		
		public static bool operator !=(Tuple<T1, T2, T3> left, Tuple<T1, T2, T3> right)
		{
			return !left.Equals(right);
		}
	}

	public struct Tuple<T1, T2, T3, T4>
	{
		public Tuple(T1 item1, T2 item2, T3 item3, T4 item4)
		{
			this = new Tuple<T1, T2, T3, T4>();
			this.Item1 = item1;
			this.Item2 = item2;
			this.Item3 = item3;
			this.Item4 = item4;
		}
		
		public T1 Item1 { get; private set; }
		public T2 Item2 { get; private set; }
		public T3 Item3 { get; private set; }
		public T4 Item4 { get; private set; }
		
		public override bool Equals(object obj)
		{
			if (obj is Tuple<T1, T2, T3, T4>)
			{
				Tuple<T1, T2, T3, T4> that = (Tuple<T1, T2, T3, T4>)obj;
				return object.Equals(this.Item1, that.Item1) && object.Equals(this.Item2, that.Item2) && object.Equals(this.Item3, that.Item3) && object.Equals(this.Item4, that.Item4);
			}
			else
			{
				return false;
			}
		}
		
		public override int GetHashCode()
		{
			return ((this.Item1 != null) ? this.Item1.GetHashCode() : 0) ^ ((this.Item2 != null) ? this.Item2.GetHashCode() : 0) ^ ((this.Item3 != null) ? this.Item3.GetHashCode() : 0) ^ ((this.Item4 != null) ? this.Item4.GetHashCode() : 0);
		}
		
		public static bool operator ==(Tuple<T1, T2, T3, T4> left, Tuple<T1, T2, T3, T4> right)
		{
			return left.Equals(right);
		}
		
		public static bool operator !=(Tuple<T1, T2, T3, T4> left, Tuple<T1, T2, T3, T4> right)
		{
			return !left.Equals(right);
		}
	}
}