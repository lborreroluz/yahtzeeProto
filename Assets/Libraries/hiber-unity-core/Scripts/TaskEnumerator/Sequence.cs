﻿using System.Collections;
using System.Collections.Generic;

namespace Hibernum.Core
{
	public static partial class TaskEnumeratorExtensions
	{
		public static TaskEnumerator ToSequence(this IEnumerable<IEnumerator<ITaskEnumeratorStatus>> self)
		{
			return new Sequence(self).ToTaskEnumerator();
		}

		public static TaskEnumerator Then(this IEnumerator<ITaskEnumeratorStatus> self, IEnumerator<ITaskEnumeratorStatus> other)
		{
			return new Sequence(self, other).ToTaskEnumerator();
		}
	}

	public class Sequence : IEnumerator<ITaskEnumeratorStatus>
	{ 
		private Queue<IEnumerator<ITaskEnumeratorStatus>> _tasks;

		public ITaskEnumeratorStatus Current 
		{
			get 
			{
				return _tasks.Count > 0 ? TaskEnumeratorStatus.Active : TaskEnumeratorStatus.Success;
			}
		}

		object IEnumerator.Current 
		{
			get 
			{
				return this.Current;
			}
		}

		public Sequence (IEnumerable<IEnumerator<ITaskEnumeratorStatus>> tasks)
		{
			_tasks = new Queue<IEnumerator<ITaskEnumeratorStatus>>(tasks);
		}

		public Sequence (params IEnumerator<ITaskEnumeratorStatus>[] tasks)
		{
			_tasks = new Queue<IEnumerator<ITaskEnumeratorStatus>>(tasks);
		}

		public bool MoveNext ()
		{
			bool moveNext;

			do 
			{
				IEnumerator task = _tasks.Peek();
				moveNext = task.MoveNext();

				if (!moveNext) 
				{
					_tasks.Dequeue();
				}
			} 
			while (!moveNext && _tasks.Count > 0);

			return _tasks.Count > 0;
		}

		public void Reset ()
		{
			throw new System.NotImplementedException ();
		}

		public void Dispose ()
		{

		}
	}
}
