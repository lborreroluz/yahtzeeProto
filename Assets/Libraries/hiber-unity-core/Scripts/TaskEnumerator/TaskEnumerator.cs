using System.Collections;
using System.Collections.Generic;

namespace Hibernum.Core
{
	public static partial class TaskEnumeratorExtensions
	{
		public static TaskEnumerator ToTaskEnumerator(this IEnumerator<ITaskEnumeratorStatus> self)
		{
			return TaskEnumerator.Create(self);
		}
	}

	public class TaskEnumerator : IEnumerator<ITaskEnumeratorStatus>
	{
		private IEnumerator<ITaskEnumeratorStatus> _task;
		private bool? _prime;

		protected IEnumerator<ITaskEnumeratorStatus> Task
		{
			get { return _task; }
			set { _task = value; }
		}

		public ITaskEnumeratorStatus Current 
		{
			get 
			{
				return _task.Current;
			}
		}

		object IEnumerator.Current 
		{ 
			get 
			{ 
				return this.Current;
			} 
		}
		
		public ITaskEnumeratorStatus Status 
		{
			get 
			{
				return _task.Current;
			}
		}

		public bool IsDone 
		{
			get 
			{
				return _task.Current.IsDone;
			}
		}

		public bool IsActive 
		{
			get 
			{
				return _task.Current.IsActive;
			}
		}
		
		public bool HasError 
		{
			get 
			{
				return _task.Current.IsError;
			}
		}
		
		public bool HasBeenCanceled 
		{
			get 
			{
				return _task.Current.IsCanceled;
			}
		}
		
		public bool HasSucceeded 
		{
			get 
			{
				return _task.Current.IsSuccess;
			}
		}
		
        public object Data 
        {
            get
            {
                return _task.Current.Data;
            }
        }

        public object Error 
		{
			get
			{
				return _task.Current.Error;
			}
		}

		public float Progress
		{
			get
			{
				return _task.Current.Progress;
			}
		}

		public TaskEnumerator(IEnumerator<ITaskEnumeratorStatus> task)
		{
			_task = task;
		}

		public static TaskEnumerator Create(IEnumerator<ITaskEnumeratorStatus> task)
		{
			return new TaskEnumerator(task);
		}

		public virtual bool MoveNext()
		{
			if(_prime.HasValue)
			{
				bool moveNext = _prime.Value;
				_prime = null;
				return moveNext;
			}
			else
			{

				bool moveNext = _task.MoveNext();
				bool done = !moveNext;

				if (!done)
				{
					switch (_task.Current.State)
					{
						case TaskEnumeratorState.Error:
						case TaskEnumeratorState.Success:
						case TaskEnumeratorState.Canceled:
							return false;
					}
				}

				return moveNext;
			}
		}

		public bool Prime()
		{
			_prime = true;

			bool moveNext = _task.MoveNext();
			bool done = !moveNext;
			
			if (!done)
			{
				switch (_task.Current.State)
				{
					case TaskEnumeratorState.Error:
					case TaskEnumeratorState.Success:
					case TaskEnumeratorState.Canceled:
						return false;
				}
			}

			return moveNext;
		}

		public void Reset()
		{
			throw new System.NotImplementedException ();
		}

		public void Dispose()
		{
			_task.Dispose();
		}
	}
}
