using System.Collections;

namespace Hibernum.Core
{
	public class TaskEnumeratorStatus : ITaskEnumeratorStatus
	{
		#region Fields
		protected readonly TaskEnumeratorState _state;
		protected readonly object _data;
        protected readonly object _error;
		protected float _progress;
		protected CoreDictionary<object, object> _properties = new CoreDictionary<object, object> ();
		#endregion
		
		#region Properties
		public TaskEnumeratorState State 
		{ 
			get 
			{ 
				return _state; 
			} 
		}
		
        public object Error 
		{ 
			get 
			{ 
				return _error; 
			} 
		}
		
		public object Data 
		{ 
			get 
			{
				return _data;
			}
		}
		
		public bool IsDone
		{
			get 
			{
				return _state != TaskEnumeratorState.Active;
			}
		}
		
		public bool IsActive
		{
			get 
			{
				return _state == TaskEnumeratorState.Active;
			}
		}
		
		public bool IsSuccess 
		{
			get 
			{
				return _state == TaskEnumeratorState.Success;
			}
		}
		
		public bool IsError
		{
			get 
			{
				return _state == TaskEnumeratorState.Error;
			}
		}
		
		public bool IsCanceled
		{
			get 
			{
				return _state == TaskEnumeratorState.Canceled;
			}
		}

		public float Progress
		{
			get
			{
				return _progress;
			}
		}
		#endregion
		
		#region Constructor
        private TaskEnumeratorStatus(TaskEnumeratorState state, object data = null, object error = null)
		{
			this._state = state;
			this._data = data;
			this._error = error;
		}
		#endregion
		
		#region Static States
		public static readonly TaskEnumeratorStatus Active = new TaskEnumeratorStatus(TaskEnumeratorState.Active);
		public static readonly TaskEnumeratorStatus Success = new TaskEnumeratorStatus(TaskEnumeratorState.Success);
		public static readonly TaskEnumeratorStatus Canceled = new TaskEnumeratorStatus(TaskEnumeratorState.Canceled);
		#endregion

		#region Public Methods
		public TaskEnumeratorStatus SetProgress(float progress)
		{
			_progress = progress;
			return this;
		}
		
		ITaskEnumeratorStatus ITaskEnumeratorStatus.SetProgress(float progress)
		{
			_progress = progress;
			return this;
		}
		
		public TaskEnumeratorStatus SetProperty(object key, object value)
		{
			_properties [key] = value;
			return this;
		}
		
		ITaskEnumeratorStatus ITaskEnumeratorStatus.SetProperty(object key, object value)
		{
			_properties [key] = value;
			return this;
		}
		
		object ITaskEnumeratorStatus.GetProperty(object key)
		{
			return _properties [key];
		}
		#endregion

		#region Static Methods
		public static TaskEnumeratorStatus CreateActive()
		{
			return new TaskEnumeratorStatus(TaskEnumeratorState.Active, null, null);
		}

		public static TaskEnumeratorStatus CreateSuccess(object data)
		{
			return new TaskEnumeratorStatus(TaskEnumeratorState.Success, data, null);
		}
		
        public static TaskEnumeratorStatus CreateError(object error)
		{
			return new TaskEnumeratorStatus(TaskEnumeratorState.Error, null, error);
		}
		
		public static TaskEnumeratorStatus CreateCanceled()
		{
			return new TaskEnumeratorStatus(TaskEnumeratorState.Canceled, null, null);
		}
		#endregion
	}
}
