using System.Collections;
using System.Collections.Generic;

namespace Hibernum.Core
{
	public class TaskEnumeratorFactory : ITaskEnumeratorFactory
	{
		private readonly System.Func<IEnumerator<ITaskEnumeratorStatus>> _getRequest;

		private TaskEnumeratorFactory(System.Func<IEnumerator<ITaskEnumeratorStatus>> getRequest)
		{
			_getRequest = getRequest;
		}

		public static ITaskEnumeratorFactory CreateFactory(System.Func<IEnumerator<ITaskEnumeratorStatus>> getRequest)
		{
			return new TaskEnumeratorFactory(getRequest);
		}

		public TaskEnumerator GetTaskEnumerator()
		{
			return new TaskEnumerator(_getRequest());
		}
	}
}