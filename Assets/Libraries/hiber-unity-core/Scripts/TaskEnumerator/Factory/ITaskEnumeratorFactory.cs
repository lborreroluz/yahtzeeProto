namespace Hibernum.Core
{
	public interface ITaskEnumeratorFactory
	{
		TaskEnumerator GetTaskEnumerator();
	}
}
