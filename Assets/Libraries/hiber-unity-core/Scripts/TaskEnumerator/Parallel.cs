﻿using System.Collections;
using System.Collections.Generic;

namespace Hibernum.Core
{
	public static partial class TaskEnumeratorExtensions
	{
		public static TaskEnumerator ToParallel(this IEnumerable<IEnumerator<ITaskEnumeratorStatus>> self)
		{
			return new Parallel(self).ToTaskEnumerator();
		}

		public static TaskEnumerator And(this IEnumerator<ITaskEnumeratorStatus> self, IEnumerator<ITaskEnumeratorStatus> other)
		{
			return new Parallel(self, other).ToTaskEnumerator();
		}
	}

	/// <summary>
	/// Class for running TaskEnumerators or IEnumerator<ITaskEnumeratorStatus> in parallel
	/// </summary>
	public class Parallel : IEnumerator<ITaskEnumeratorStatus>
	{ 
		private IList<IEnumerator<ITaskEnumeratorStatus>> _tasks;

		public ITaskEnumeratorStatus Current 
		{
			get 
			{
				return _tasks.Count > 0 ? TaskEnumeratorStatus.Active : TaskEnumeratorStatus.Success;
			}
		}

		object IEnumerator.Current 
		{
			get 
			{
				return this.Current;
			}
		}

		public Parallel (IEnumerable<IEnumerator<ITaskEnumeratorStatus>> tasks)
		{
			_tasks = new List<IEnumerator<ITaskEnumeratorStatus>>(tasks);
		}

		public Parallel (params IEnumerator<ITaskEnumeratorStatus>[] tasks)
		{
			_tasks = new List<IEnumerator<ITaskEnumeratorStatus>>(tasks);
		}

		public bool MoveNext ()
		{
			for (int i=0; i<_tasks.Count; i++) 
			{
				IEnumerator task = _tasks[i];

				bool moveNext = task.MoveNext();
				bool done = !moveNext;

				if (done) 
				{
					_tasks.RemoveAt(i);
					i--;
				}
			}

			return _tasks.Count > 0;
		}

		public void Reset ()
		{
			throw new System.NotImplementedException ();
		}

		public void Dispose ()
		{

		}
	}
}
