using System.Collections;
using System.Collections.Generic;

namespace Hibernum.Core.Generic
{
	public static partial class TaskEnumeratorExtensions
	{
		public static TaskEnumerator<T> ToTaskEnumerator<T>(this IEnumerator<TaskEnumeratorStatus<T>> self)
		{
			return TaskEnumerator<T>.Create(self);
		}

        public static TaskEnumerator<T,V> ToTaskEnumerator<T,V>(this IEnumerator<TaskEnumeratorStatus<T,V>> self)
        {
            return TaskEnumerator<T,V>.Create(self);
        }
    }

	public class TaskEnumerator<T> : IEnumerator<ITaskEnumeratorStatus>
	{
		private IEnumerator<TaskEnumeratorStatus<T>> _task;
		private bool? _prime;

		protected IEnumerator<TaskEnumeratorStatus<T>> Task
		{
			get { return _task; }
			set { _task = value; }
		}

		public ITaskEnumeratorStatus Current 
		{
			get
			{
				return _task.Current;
			}
		}

		object IEnumerator.Current 
		{
			get 
			{
				return this.Current;
			}
		}
		
		public ITaskEnumeratorStatus Status 
		{
			get
			{
				return _task.Current;
			}
		}

		public bool IsDone 
		{
			get 
			{
				return _task.Current.IsDone;
			}
		}
		
		public bool IsActive
		{
			get 
			{
				return _task.Current.IsActive;
			}
		}
		
		public bool HasError 
		{
			get 
			{
				return _task.Current.IsError;
			}
		}
		
		public bool HasBeenCanceled 
		{
			get 
			{
				return _task.Current.IsCanceled;
			}
		}
		
		public bool HasSucceeded 
		{
			get 
			{
				return _task.Current.IsSuccess;
			}
		}

        public T Data 
        {
            get
            {
                return _task.Current.Data;
            }
        }
		
        public object Error 
		{
			get 
			{
				return _task.Current.Error;
			}
		}

		public float Progress
		{
			get
			{
				return _task.Current.Progress;
			}
		}
		
		public TaskEnumerator(IEnumerator<TaskEnumeratorStatus<T>> task)
		{
			_task = task;
		}

		public static TaskEnumerator<T> Create(IEnumerator<TaskEnumeratorStatus<T>> task)
		{
			return new TaskEnumerator<T>(task);
		}
		
		public virtual bool MoveNext()
		{
			if(_prime.HasValue)
			{
				bool moveNext = _prime.Value;
				_prime = null;
				return moveNext;
			}
			else
			{
				
				bool moveNext = _task.MoveNext();
				bool done = !moveNext;
				
				if (!done)
				{
					switch (_task.Current.State)
					{
						case TaskEnumeratorState.Error:
						case TaskEnumeratorState.Success:
						case TaskEnumeratorState.Canceled:
							return false;
					}
				}
				
				return moveNext;
			}
		}
		
		public bool Prime()
		{
			_prime = true;
			
			bool moveNext = _task.MoveNext();
			bool done = !moveNext;
			
			if (!done)
			{
				switch (_task.Current.State)
				{
					case TaskEnumeratorState.Error:
					case TaskEnumeratorState.Success:
					case TaskEnumeratorState.Canceled:
						return false;
				}
			}
			
			return moveNext;
		}
		
		public void Reset ()
		{
			throw new System.NotImplementedException ();
		}
		
		public void Dispose ()
		{
			_task.Dispose();
		}
	}


    public class TaskEnumerator<T,V> : IEnumerator<ITaskEnumeratorStatus>
    {
        private IEnumerator<TaskEnumeratorStatus<T,V>> _task;
		private bool? _prime;
        
		protected IEnumerator<TaskEnumeratorStatus<T,V>> Task
		{
			get { return _task; }
			set { _task = value; }
		}

        public ITaskEnumeratorStatus Current 
        {
            get
            {
                return _task.Current;
            }
        }
        
        object IEnumerator.Current 
        {
            get 
            {
                return this.Current;
            }
        }
        
        public ITaskEnumeratorStatus Status 
        {
            get
            {
                return _task.Current;
            }
        }
        
        public bool IsDone 
        {
            get 
            {
                return _task.Current.IsDone;
            }
        }
        
        public bool IsActive
        {
            get 
            {
                return _task.Current.IsActive;
            }
        }
        
        public bool HasError 
        {
            get 
            {
                return _task.Current.IsError;
            }
        }
        
        public bool HasBeenCanceled 
        {
            get 
            {
                return _task.Current.IsCanceled;
            }
        }
        
        public bool HasSucceeded 
        {
            get 
            {
                return _task.Current.IsSuccess;
            }
        }
        
        public T Data 
        {
            get
            {
                return _task.Current.Data;
            }
        }
        
        public V Error 
        {
            get 
            {
                return _task.Current.Error;
            }
        }

		public float Progress
		{
			get
			{
				return _task.Current.Progress;
			}
		}
        
        public TaskEnumerator(IEnumerator<TaskEnumeratorStatus<T,V>> task)
        {
            _task = task;
        }
        
        public static TaskEnumerator<T,V> Create(IEnumerator<TaskEnumeratorStatus<T,V>> task)
        {
            return new TaskEnumerator<T,V>(task);
        }
        
        public virtual bool MoveNext()
        {
            if(_prime.HasValue)
            {
                bool moveNext = _prime.Value;
                _prime = null;
                return moveNext;
            }
            else
            {
                
                bool moveNext = _task.MoveNext();
                bool done = !moveNext;
                
                if (!done)
                {
                    switch (_task.Current.State)
                    {
                    case TaskEnumeratorState.Error:
                    case TaskEnumeratorState.Success:
                    case TaskEnumeratorState.Canceled:
                        return false;
                    }
                }
                
                return moveNext;
            }
        }
        
        public bool Prime()
        {
            _prime = true;
            
            bool moveNext = _task.MoveNext();
            bool done = !moveNext;
            
            if (!done)
            {
                switch (_task.Current.State)
                {
                case TaskEnumeratorState.Error:
                case TaskEnumeratorState.Success:
                case TaskEnumeratorState.Canceled:
                    return false;
                }
            }
            
            return moveNext;
        }
        
        public void Reset ()
        {
            throw new System.NotImplementedException ();
        }
        
        public void Dispose ()
        {
            _task.Dispose();
        }
    }
}

