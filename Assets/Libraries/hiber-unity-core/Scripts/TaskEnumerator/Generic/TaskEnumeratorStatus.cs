namespace Hibernum.Core.Generic
{
	public class TaskEnumeratorStatus<T> : ITaskEnumeratorStatus
	{
		#region Fields
		private readonly TaskEnumeratorState _state;
		private readonly T _data;
        private readonly object _error;
		protected float _progress;
		protected CoreDictionary<object, object> _properties = new CoreDictionary<object, object> ();
		#endregion
		
		#region Properties
		public TaskEnumeratorState State 
		{ 
			get 
			{ 
				return _state; 
			} 
		}
		
        public object Error 
		{ 
			get 
			{ 
				return _error; 
			} 
		}

		public T Data 
		{ 
			get 
			{
				return _data;
			}
		}

		object ITaskEnumeratorStatus.Data 
		{ 
			get 
			{
				return this.Data;
			}
		}

		public bool IsDone 
		{
			get 
			{
				return _state != TaskEnumeratorState.Active;	
			}
		}

		public bool IsActive 
		{
			get 
			{
				return _state == TaskEnumeratorState.Active;	
			}
		}
		
		public bool IsSuccess
		{
			get 
			{
				return _state == TaskEnumeratorState.Success;
			}
		}
		
		public bool IsError
		{
			get 
			{
				return _state == TaskEnumeratorState.Error;
			}
		}
		
		public bool IsCanceled 
		{
			get 
			{
				return _state == TaskEnumeratorState.Canceled;
			}
		}
		
		public float Progress
		{
			get
			{
				return _progress;
			}
		}
		#endregion
		
		#region Constructor
        private TaskEnumeratorStatus(TaskEnumeratorState state, T payload = default(T), object error = null)
		{
			this._state = state;
			this._data = payload;
			this._error = error;
		}
		#endregion
		
		#region Static States
		public static readonly TaskEnumeratorStatus<T> Active = new TaskEnumeratorStatus<T>(TaskEnumeratorState.Active);
		public static readonly TaskEnumeratorStatus<T> Success = new TaskEnumeratorStatus<T>(TaskEnumeratorState.Success);
		public static readonly TaskEnumeratorStatus<T> Canceled = new TaskEnumeratorStatus<T>(TaskEnumeratorState.Canceled);
		#endregion
		
		#region Public Methods
		public TaskEnumeratorStatus<T> SetProgress(float progress)
		{
			_progress = progress;
			return this;
		}

		ITaskEnumeratorStatus ITaskEnumeratorStatus.SetProgress(float progress)
		{
			_progress = progress;
			return this;
		}

		public TaskEnumeratorStatus<T> SetProperty(object key, object value)
		{
			_properties [key] = value;
			return this;
		}

		ITaskEnumeratorStatus ITaskEnumeratorStatus.SetProperty(object key, object value)
		{
			_properties [key] = value;
			return this;
		}

		object ITaskEnumeratorStatus.GetProperty(object key)
		{
			return _properties [key];
		}
		#endregion
		
		#region Static Methods
		public static TaskEnumeratorStatus<T> CreateActive()
		{
			return new TaskEnumeratorStatus<T>(TaskEnumeratorState.Active, default(T), null);
		}

		public static TaskEnumeratorStatus<T> CreateSuccess(T payload)
		{
			return new TaskEnumeratorStatus<T>(TaskEnumeratorState.Success, payload, null);
		}
		
        public static TaskEnumeratorStatus<T> CreateError(object error)
		{
			return new TaskEnumeratorStatus<T>(TaskEnumeratorState.Error, default(T), error);
		}
		
		public static TaskEnumeratorStatus<T> CreateCanceled()
		{
			return new TaskEnumeratorStatus<T>(TaskEnumeratorState.Canceled, default(T), null);
		}
		#endregion
	}


    public class TaskEnumeratorStatus<T,V> : ITaskEnumeratorStatus
    {
        #region Fields
        private readonly TaskEnumeratorState _state;
        private readonly T _data;
		private readonly V _error;
		protected float _progress;
		protected CoreDictionary<object, object> _properties = new CoreDictionary<object, object> ();
        #endregion
        
        #region Properties
        public TaskEnumeratorState State 
        { 
            get 
            { 
                return _state; 
            } 
        }
        
        public V Error 
        { 
            get 
            { 
                return _error; 
            } 
        }
        
        object ITaskEnumeratorStatus.Error 
        { 
            get 
            {
                return this.Error;
            }
        }

        public T Data 
        { 
            get 
            {
                return _data;
            }
        }
        
        object ITaskEnumeratorStatus.Data 
        { 
            get 
            {
                return this.Data;
            }
        }
        
        public bool IsDone 
        {
            get 
            {
                return _state != TaskEnumeratorState.Active;    
            }
        }
        
        public bool IsActive 
        {
            get 
            {
                return _state == TaskEnumeratorState.Active;    
            }
        }
        
        public bool IsSuccess
        {
            get 
            {
                return _state == TaskEnumeratorState.Success;
            }
        }
        
        public bool IsError
        {
            get 
            {
                return _state == TaskEnumeratorState.Error;
            }
        }
        
        public bool IsCanceled 
        {
            get 
            {
                return _state == TaskEnumeratorState.Canceled;
            }
		}
		
		public float Progress
		{
			get
			{
				return _progress;
			}
		}
        #endregion
        
        #region Constructor
        private TaskEnumeratorStatus(TaskEnumeratorState state, T payload = default(T), V error = default(V))
        {
            this._state = state;
            this._data = payload;
            this._error = error;
        }
        #endregion
        
        #region Static States
        public static readonly TaskEnumeratorStatus<T,V> Active = new TaskEnumeratorStatus<T,V>(TaskEnumeratorState.Active);
        public static readonly TaskEnumeratorStatus<T,V> Success = new TaskEnumeratorStatus<T,V>(TaskEnumeratorState.Success);
        public static readonly TaskEnumeratorStatus<T,V> Canceled = new TaskEnumeratorStatus<T,V>(TaskEnumeratorState.Canceled);
		#endregion
		
		#region Public Methods
		public TaskEnumeratorStatus<T,V> SetProgress(float progress)
		{
			_progress = progress;
			return this;
		}
		
		ITaskEnumeratorStatus ITaskEnumeratorStatus.SetProgress(float progress)
		{
			_progress = progress;
			return this;
		}
		
		public TaskEnumeratorStatus<T,V> SetProperty(object key, object value)
		{
			_properties [key] = value;
			return this;
		}
		
		ITaskEnumeratorStatus ITaskEnumeratorStatus.SetProperty(object key, object value)
		{
			_properties [key] = value;
			return this;
		}
		
		object ITaskEnumeratorStatus.GetProperty(object key)
		{
			return _properties [key];
		}
		#endregion
        
		#region Static Methods
		public static TaskEnumeratorStatus<T,V> CreateActive()
		{
			return new TaskEnumeratorStatus<T,V>(TaskEnumeratorState.Active, default(T), default(V));
		}

		public static TaskEnumeratorStatus<T,V> CreateSuccess(T payload)
        {
            return new TaskEnumeratorStatus<T,V>(TaskEnumeratorState.Success, payload, default(V));
        }
        
        public static TaskEnumeratorStatus<T,V> CreateError(V error)
        {
            return new TaskEnumeratorStatus<T,V>(TaskEnumeratorState.Error, default(T), error);
        }
        
        public static TaskEnumeratorStatus<T,V> CreateCanceled()
        {
            return new TaskEnumeratorStatus<T,V>(TaskEnumeratorState.Canceled, default(T), default(V));
        }
        #endregion
    }
}
