using System.Collections.Generic;

namespace Hibernum.Core
{
	public interface ITaskEnumeratorStatus
	{
		TaskEnumeratorState State { get; }
		object Data { get; }
        object Error { get; }
		bool IsDone { get; }
		bool IsActive { get; }
		bool IsSuccess { get; }
		bool IsError { get; }
		bool IsCanceled { get; }
		float Progress { get; }
		ITaskEnumeratorStatus SetProgress(float progress);
		ITaskEnumeratorStatus SetProperty(object key, object value);
		object GetProperty(object key);
	}
}
