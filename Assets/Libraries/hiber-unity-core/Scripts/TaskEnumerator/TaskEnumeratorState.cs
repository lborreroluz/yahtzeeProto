namespace Hibernum.Core
{
	public enum TaskEnumeratorState
	{
		Active,
		Success,
		Error,
		Canceled,
	}
}
