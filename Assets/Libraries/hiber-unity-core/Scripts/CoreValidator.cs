using System;
using UnityEngine;

/// <summary>
/// This class is used to assign a variable depending on the resulting validation
/// applied to its pretended new value. This is to be used in properties to avoid
/// code duplication.
/// </summary>
public static class CoreValidator
{
	/// <summary>
	/// Clamps newValue between min and max and assigns it to curValue if it is different.
	/// </summary>
	/// <returns><c>true</c> if curValue has changed, <c>false</c> otherwise.</returns>
	public static bool Clamp<T>(ref T curValue, T newValue, T min, T max, Action changedFunc = null, bool forceCheck = false) where T : IComparable<T>
	{
		if (forceCheck || curValue.CompareTo(newValue) != 0)
		{
			T clamped = newValue;
			if (clamped.CompareTo(min) < 0) clamped = min;
			if (clamped.CompareTo(max) > 0) clamped = max;
			if (clamped.CompareTo(curValue) != 0)
			{
				curValue = clamped;
				if (changedFunc != null)
				{
					changedFunc();
				}
				return true;
			}
		}
		return false;
	}

	/// <summary>
	/// Calculates the highest number between newValue and min and assigns it to curValue if it is different.
	/// </summary>
	/// <returns><c>true</c> if curValue has changed, <c>false</c> otherwise.</returns>
	public static bool Floor<T>(ref T curValue, T newValue, T min, Action changedFunc = null, bool forceCheck = false) where T : IComparable<T>
	{
		if (forceCheck || curValue.CompareTo(newValue) != 0)
		{
			T clamped = newValue;
			if (clamped.CompareTo(min) < 0) clamped = min;
			if (clamped.CompareTo(curValue) != 0)
			{
				curValue = clamped;
				if (changedFunc != null)
				{
					changedFunc();
				}
				return true;
			}
		}
		return false;
	}

	/// <summary>
	/// Calculates the lowest number between newValue and max and assigns it to curValue if it is different.
	/// </summary>
	/// <returns><c>true</c> if curValue has changed, <c>false</c> otherwise.</returns>
	public static bool Ceil<T>(ref T curValue, T newValue, T max, Action changedFunc = null, bool forceCheck = false) where T : IComparable<T>
	{
		if (forceCheck || curValue.CompareTo(newValue) != 0)
		{
			T clamped = newValue;
			if (clamped.CompareTo(max) > 0) clamped = max;
			if (clamped.CompareTo(curValue) != 0)
			{
				curValue = clamped;
				if (changedFunc != null)
				{
					changedFunc();
				}
				return true;
			}
		}
		return false;
	}

	/// <summary>
	/// Assigns newValue to curValue if it is different.
	/// </summary>
	/// <returns><c>true</c> if curValue has changed, <c>false</c> otherwise.</returns>
	public static bool Different<T>(ref T curValue, T newValue, Action changedFunc = null)
	{
		if (!object.Equals(curValue, newValue))
		{
			curValue = newValue;
			if (changedFunc != null)
			{
				changedFunc();
			}
			return true;
		}
		return false;
	}
}
