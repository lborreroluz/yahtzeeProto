﻿namespace Hibernum.Core
{
	public static class FlagsHelper
	{
		public static bool IsSet<T>(T flags, T flag) where T : struct
		{
			int flagsValue = (int)(object)flags;
			int flagValue = (int)(object)flag;

			return (flagsValue & flagValue) != 0;
		}

		public static void Set<T>(ref T flags, T flag) where T : struct
		{
			int flagsValue = (int)(object)flags;
			int flagValue = (int)(object)flag;

			flags = (T)(object)(flagsValue | flagValue);
		}

		public static void Unset<T>(ref T flags, T flag) where T : struct
		{
			int flagsValue = (int)(object)flags;
			int flagValue = (int)(object)flag;

			flags = (T)(object)(flagsValue & (~flagValue));
		}

		public static bool IsSet(int flags, int flag)
		{
			return (flags & flag) != 0;
		}
		
		public static void Set(ref int flags, int flag)
		{
			flags = (flags | flag);
		}
		
		public static void Unset(int flags, int flag)
		{
			flags = (flags & (~flag));
		}
	}
}