using Conditional = System.Diagnostics.ConditionalAttribute;
using UnityEngine;

/// <summary>
/// CoreAssert, like UnityEngine.Assertions.Assert, is enabled if the UNITY_ASSERTIONS symbol is defined. If you want to
/// force them off, define ASSERT_DISABLED in your project.
/// 
/// You should use UnityEngine.Assertions.Assert instead of CoreAssert. The only reason why we keep this is that
/// there is no reliable way to catch the exceptions thrown by the Unity assertions with Visual Studio. When this is
/// fixed, this class might be deprecated. With this class, we can add a breakpoint to debug the failed assertions.
/// </summary>
/// <remarks>
/// Previously, CoreAssert used an Opt-In mechanism: by default, the asserts were disabled and ASSERT_ENABLED had
/// to be defined in the project. From now on, defining ASSERT_ENABLED in the project has no effect.
/// </remarks>
public static class CoreAssert
{
	// Opt-Out mechanism.
#if ASSERT_DISABLED
	private const string kEnabled = "FALSE";
#else
	private const string kEnabled = "UNITY_ASSERTIONS";
#endif

	/// <summary>
	/// Warning assert when the specified condition fails.
	/// </summary>
	/// <param name="condition">If set to <c>true</c> condition.</param>
	/// <param name="context">Context.</param>
	[Conditional(kEnabled)]
	public static void Warning( bool condition, UnityEngine.Object context )
	{
		Warning( condition, context.ToString(), context );
	}

	/// <summary>
	/// Warning assert when the specified condition fails.
	/// </summary>
	/// <param name="condition">If set to <c>true</c> condition.</param>
	[Conditional(kEnabled)]
	public static void Warning( bool condition )
	{
		Warning( condition, "Assert Failed", null );
	}
	
	/// <summary>
	/// Warning assert when the specified condition fails.
	/// </summary>
	/// <param name="condition">If set to <c>true</c> condition.</param>
	/// <param name="message">Message.</param>
	[Conditional(kEnabled)]
	public static void Warning( bool condition, System.Object message )
	{
		Warning( condition, message, null );
	}
	
	/// <summary>
	/// Warning assert when the specified condition fails.
	/// </summary>
	/// <param name="condition">If set to <c>true</c> condition.</param>
	/// <param name="message">Message.</param>
	/// <param name="context">Context.</param>
	[Conditional(kEnabled)]
	public static void Warning( bool condition, System.Object message, UnityEngine.Object context )
	{
		if ( !condition ) {
			Debug.LogWarning(message, context);
		}
	}

	/// <summary>
	/// Fatal assert when the specified condition fails.
	/// </summary>
	/// <param name='condition'>
	/// Condition.
	/// </param>
	/// <param name='context'>
	/// Context.
	/// </param>
	[Conditional(kEnabled)]
	public static void Fatal( bool condition, UnityEngine.Object context )
	{
		Fatal( condition, context.ToString(), context );
	}

	/// <summary>
	/// Fatal assert when the specified condition fails.
	/// </summary>
	/// <param name='condition'>
	/// Condition.
	/// </param>
	[Conditional(kEnabled)]
	public static void Fatal( bool condition )
	{
		Fatal( condition, "Assert Failed", null );
	}

	/// <summary>
	/// Fatal assert when the specified condition fails.
	/// </summary>
	/// <param name='condition'>
	/// Condition.
	/// </param>
	/// <param name='message'>
	/// Message.
	/// </param>
	[Conditional(kEnabled)]
	public static void Fatal( bool condition, System.Object message )
	{
		Fatal( condition, message, null );
	}

	/// <summary>
	/// Fatal assert when the specified condition fails.
	/// </summary>
	/// <param name='condition'>
	/// Condition.
	/// </param>
	/// <param name='message'>
	/// Message.
	/// </param>
	/// <param name='context'>
	/// Context.
	/// </param>
	[Conditional(kEnabled)]
	public static void Fatal( bool condition, System.Object message, UnityEngine.Object context )
	{
		if ( !condition ) {
			Debug.LogError(message, context);
			Debug.Break();
		}
	}
}
