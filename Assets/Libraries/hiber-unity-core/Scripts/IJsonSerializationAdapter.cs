using System;

namespace Hibernum.Core
{
	public interface IJsonSerializationAdapter
	{
		T JsonDecode<T>(string data);
		object JsonDecode(string data);
		string JsonEncode(object data);
	}

}

