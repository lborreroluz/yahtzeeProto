using UnityEngine;
using System.Runtime.InteropServices;

namespace Hibernum.Core
{
    public class BundleUtility
    {
    	#if UNITY_IPHONE && !UNITY_EDITOR
    	[DllImport("__Internal")]
    	private static extern string _GetCFBundleId();

    	[DllImport("__Internal")]
    	private static extern string _GetCFBundleVersion();
    	#endif

    	public static string BundleId
    	{
    		get {
    			if (_bundleId == null)
    			{
    				GetBundleId();
    			}
    			return _bundleId;
    		}
    	}
    	protected static string _bundleId;

    	public static string BundleVersion
    	{
    		get {
    			if (_bundleVersion == null)
    			{
    				GetBundleVersion();
    			}
    			return _bundleVersion;
    		}
    	}
    	protected static string _bundleVersion;

    	protected static void GetBundleId() {
    		#if UNITY_IPHONE && !UNITY_EDITOR
    		_bundleId = _GetCFBundleId();
    		#endif
    	}

    	protected static void GetBundleVersion() {
    		#if UNITY_IPHONE && !UNITY_EDITOR
    		_bundleVersion = _GetCFBundleVersion();
    		#endif
    	}
    }
}