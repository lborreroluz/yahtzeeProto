//
//  MemoryCheck.m
//  Unity-iPhone
//
//  Created by Ingrid Del Real on 2014-06-26.
//
//

#import "MemoryCheck.h"
#import <mach/mach.h>

@implementation MemoryCheck

- (id)init
{
    self = [super init];
     NSLog(@" init initialization ");
    return self;
}

-(long) getFreeDiskspace
{
    long totalSpace = 0;
        long totalFreeSpace = 0;
        NSError *error = nil;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
        
        if (dictionary) {
            NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
            NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
            totalSpace = [fileSystemSizeInBytes longValue];
            totalFreeSpace = [freeFileSystemSizeInBytes longValue];
            NSLog(@"Memory Capacity of %l MiB with %l MiB Free memory available.", ((totalSpace/1024l)/1024l), ((totalFreeSpace/1024l)/1024l));
        } else {
            NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %@", [error domain], [error code]);
        }
        
        return totalFreeSpace;
}

-(long) getUsedMemory
{
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(),
                                 TASK_BASIC_INFO,
                                 (task_info_t)&info,
                                 &size);

    if( kerr == KERN_SUCCESS ) {
        return info.resident_size;
    } 
    else {
        NSLog(@"Error with task_info(): %s", mach_error_string(kerr));
        return 0;
    }
}

-(long) getAvailableVirtualMemory
{
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(),
                                 TASK_BASIC_INFO,
                                 (task_info_t)&info,
                                 &size);

    if( kerr == KERN_SUCCESS ) {
        return info.virtual_size;
    } 
    else {
        NSLog(@"Error with task_info(): %s", mach_error_string(kerr));
        return 0;
    }
}

@end

static MemoryCheck* delegateObject = nil;

extern "C"
{
    long _getExternalAvailableSpaceInBytes()
    {
        if(delegateObject == nil)
        {
            delegateObject = [[MemoryCheck alloc] init];
        }
        return [delegateObject getFreeDiskspace];
    }

    long _getUsedMemoryInBytes()
    {
        if(delegateObject == nil)
        {
            delegateObject = [[MemoryCheck alloc] init];
        }
        return [delegateObject getUsedMemory];
    }

    long _getAvailableVirtualMemoryInBytes()
    {
        if(delegateObject == nil)
        {
            delegateObject = [[MemoryCheck alloc] init];
        }
        return [delegateObject getAvailableVirtualMemory];
    }
}

