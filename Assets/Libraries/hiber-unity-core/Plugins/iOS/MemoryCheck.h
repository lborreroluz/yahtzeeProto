//
//  MemoryCheck.h
//  Unity-iPhone
//
//  Created by Ingrid Del Real on 2014-06-26.
//
//

#import <Foundation/Foundation.h>

@interface MemoryCheck : NSObject

- (long)getFreeDiskspace;
- (long)getUsedMemory;
- (long)getAvailableVirtualMemory;

@end
