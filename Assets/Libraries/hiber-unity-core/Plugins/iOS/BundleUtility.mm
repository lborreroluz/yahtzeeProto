
extern "C" {
    extern "C" {
    	const char * _GetCFBundleId() {
            NSString *identifier = [[NSBundle mainBundle] bundleIdentifier];
            return strdup([identifier UTF8String]);
        }

        const char * _GetCFBundleVersion() {
            NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
            return strdup([version UTF8String]);
        }
    }
}

