﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

#if UNITY_IPHONE && !UNITY_EDITOR
public class MemoryCheckPlugin 
{
	[DllImport ("__Internal")]
	private static extern long _getExternalAvailableSpaceInBytes();
	
	[DllImport ("__Internal")]
	private static extern long _getUsedMemoryInBytes();
	
	[DllImport ("__Internal")]
	private static extern long _getAvailableVirtualMemoryInBytes();
	
	public static long getExternalAvailableSpaceInBytes()
	{
		return _getExternalAvailableSpaceInBytes();
	}
	
	public static long getUsedMemoryInBytes()
	{
		return _getUsedMemoryInBytes();
	}
	
	public static long getAvailableVirtualMemoryInBytes()
	{
		return _getAvailableVirtualMemoryInBytes();
	}
}
#elif UNITY_ANDROID && !UNITY_EDITOR
public class MemoryCheckPlugin 
{
	private static AndroidJavaClass memoryCheck;
	
	public static long getExternalAvailableSpaceInBytes() 
	{
		long availableSpace = 0L;
		AndroidJavaClass ajc = new AndroidJavaClass("com.example.memorycheck.MemoryCheck");
		availableSpace = ajc.CallStatic<long>("getExternalAvailableSpaceInBytes");
		
		return availableSpace;
	}
	
	public static long getUsedMemoryInBytes()
	{
		return 0;
	}
	
	public static long getAvailableVirtualMemoryInBytes()
	{
		return 0;
	}
}
#else
public class MemoryCheckPlugin 
{
	//TODO: fix the .bundle file ... for some reason it is not working

	public static long getExternalAvailableSpaceInBytes()
	{
        // http://docs.unity3d.com/ScriptReference/SystemInfo-systemMemorySize.html
//        return (long)SystemInfo.systemMemorySize << 20;
		return 0;
	}

	public static long getUsedMemoryInBytes()
	{
		return 0;
	}
	
	public static long getAvailableVirtualMemoryInBytes()
	{
		return 0;
	}
}
#endif
