# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/) and [Keep a CHANGELOG](http://keepachangelog.com).

## [Unreleased]
### Added
- [Config] Added submodule config
- [BundleUtility] Added plugin to determine bundle identifier at run time on iOS
- [Extensions] Added component extensions

### Changed
- [Logging] Use SimpleLogger instead of dependency on build tool
- [Extensions] All extensions now have namespace, will now need to import hibernum.core to use them

### Deprecated
- [category] item

### Removed
- [category] item

### Fixed
- [HiberDefiner] Fix issues with reloading / recompiling when clearing DEFINES
- [HiberDefiner] Ensure support for tvOS

### Security
- [category] item


## [1.8.0] - 2015-12-07
### Added
- [category] item

### Changed
- [category] item

### Deprecated
- [category] item

### Removed
- [category] item

### Fixed
- [category] item

### Security
- [category] item

[Unreleased]: https://gitlab.hibernum.com/libs/hiber-unity-core/compare/1.8.0...HEAD
[1.8.0]: https://gitlab.hibernum.com/libs/hiber-unity-core/compare/1.7.0...1.8.0
[1.7.0]: https://gitlab.hibernum.com/libs/hiber-unity-core/compare/1.6.0...1.7.0
[1.6.0]: https://gitlab.hibernum.com/libs/hiber-unity-core/compare/1.5.0...1.6.0

