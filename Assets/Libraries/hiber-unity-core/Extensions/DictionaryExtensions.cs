﻿using System.Collections.Generic;

namespace Hibernum.Core
{
    public static class DictionaryExtensions
    {
    	public static V GetValueOrDefault<K, V>(this IDictionary<K, V> dictionary, K key, V defaultValue)
    	{
    		V value;
    		return dictionary.TryGetValue(key, out value) ? value : defaultValue;
    	}
    }
}