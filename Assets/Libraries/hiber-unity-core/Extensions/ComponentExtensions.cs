﻿using UnityEngine;
using System.Reflection;
using System.Collections.Generic;

namespace Hibernum.Core
{
    public static class ComponentExtensions
    {
    	/// <summary>
    	/// Copies the component to the destination GameObjects.
    	/// Copies data from public and serialized private/protected(inherited) fields.
    	/// </summary>
    	/// <returns>The new component instances.</returns>
    	/// <param name="original">Original component.</param>
    	/// <param name="destination">Destination GameObjects.</param>
    	public static Component[] CopyComponentToMultiple(this Component original, params GameObject[] destinations)
    	{
    		return original.CopyComponentToMultiple<Component> (destinations);
    	}
    	
    	/// <summary>
    	/// Copies the component to the destination GameObjects.
    	/// Copies data from public and serialized private/protected(inherited) fields.
    	/// </summary>
    	/// <returns>The new component instances.</returns>
    	/// <param name="original">Original component.</param>
    	/// <param name="destination">Destination GameObjects.</param>
    	/// <typeparam name="T">The return type.</typeparam>
    	public static T[] CopyComponentToMultiple<T>(this T original, params GameObject[] destinations) where T : Component
    	{
    		T[] newComponents = new T[destinations.Length];
    		for(int i = 0; i < destinations.Length; ++i){
    			newComponents[i] = original.CopyComponentTo<T>(destinations[i]);
    		}
    		
    		return newComponents;
    	}

    	/// <summary>
    	/// Copies the component to the destination GameObject.
    	/// Copies data from public and serialized private/protected(inherited) fields.
    	/// </summary>
    	/// <returns>The new component instance.</returns>
    	/// <param name="original">Original component.</param>
    	/// <param name="destination">Destination GameObject.</param>
    	public static Component CopyComponentTo(this Component original, GameObject destination)
    	{
    		return original.CopyComponentTo<Component>(destination);
    	}

    	/// <summary>
    	/// Copies the component to the destination GameObject.
    	/// Copies data from public and serialized private/protected(inherited) fields.
    	/// </summary>
    	/// <returns>The new component instance.</returns>
    	/// <param name="original">Original component.</param>
    	/// <param name="destination">Destination GameObject.</param>
    	/// <typeparam name="T">The return type.</typeparam>
    	public static T CopyComponentTo<T>(this T original, GameObject destination) where T : Component
    	{
    		if(destination == null){ return null; }

    		System.Type type = original.GetType();
    		Component copy = destination.AddComponent (type);

			// Copy the property values from the source component.
			PropertyInfo[] properties = GetPropertyInfos (type);
			for(int i = 0; i < properties.Length; ++i)
			{
				if(properties[i].Name == "name" || properties[i].Name == "tag"){ continue; }

				if (properties[i].CanWrite && properties[i].GetCustomAttributes (typeof(System.ObsoleteAttribute), false).Length <= 0)
				{
					try{
						properties[i].SetValue(copy, properties[i].GetValue(original, null), null);
					}
					catch { } // In case of NotImplementedException being thrown.
				}
			}

    		// Copy the field values from the source component.
    		FieldInfo[] fields = GetFieldInfos (type);
    		for(int i = 0; i < fields.Length; ++i)
    		{
				// Check if obsolete
				if(properties[i].GetCustomAttributes (typeof(System.ObsoleteAttribute), false).Length > 0){
					continue;
				}

				if(!fields[i].IsPrivate || fields[i].GetCustomAttributes (typeof(SerializeField), false).Length > 0){
    				fields[i].SetValue(copy, fields[i].GetValue (original));
    			}
    		}

    		return copy as T;
		}

		public static PropertyInfo[] GetPropertyInfos(System.Type type, bool recursive = true)
		{
			List<PropertyInfo> allFields = new List<PropertyInfo>();

			do
			{
				allFields.AddRange (type.GetProperties (BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic));
				type = recursive ? type.BaseType : null;
			}
			while(null != type);

			return allFields.ToArray ();
		}

    	public static FieldInfo[] GetFieldInfos(System.Type type, bool recursive = true)
    	{
    		List<FieldInfo> allFields = new List<FieldInfo>();

    		do
    		{
    			allFields.AddRange (type.GetFields (BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic));
    			type = recursive ? type.BaseType : null;
    		}
    		while(null != type);

    		return allFields.ToArray ();
		}
    }
}