using System;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;

namespace Hibernum.Core
{
    public static class EnumExtensions
    {
    	public static string GetEnumDescription(Enum value)
    	{
    	    FieldInfo fi = value.GetType().GetField(value.ToString());
    	
    	    DescriptionAttribute[] attributes = 
    	        (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
    	
    	    if (attributes != null && attributes.Length > 0)
    	        return attributes[0].Description;
    	    else
    	        return value.ToString();
    	}
    	
    	public static int GetLength(this Enum value) {
    		return Enum.GetValues(value.GetType()).Length;
    	}
    	
    	public static int GetLength<T>(){
    		return Enum.GetValues(typeof(T)).Length;
    	}

    	public static bool TryParse<T>(string s, out T result)
    	{
    		result = default(T);    
    		if (Enum.IsDefined(typeof(T), s))
    		{
    			TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
    			result = (T)converter.ConvertFromString(s);
    			return true;
    		}
    		return false;
    	}
    	
    	public static T GetEnumFromString<T>(string s) {
    		T result = default(T); 
    		
    		if (TryParse<T>(s, out result)) {
    			return result;
    		}
    		
    		return default(T);
    	}
    	
    //	public static string ToDescription(this Enum value)
    //	{
    //		var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
    //		return da.Length > 0 ? da[0].Description : value.ToString();
    //	}

    }
}