﻿using UnityEngine;
using System.Collections;

using Hibernum.Core;

namespace Hibernum.Core
{
    public static class ScalarExtensions 
    {
    	public static string ToFileSize(this int i)
    	{
    		return string.Format(new FileSizeFormatProvider(), "{0:fs}", i);
    	}

    	public static string ToFileSize(this long l)
    	{
    		return string.Format(new FileSizeFormatProvider(), "{0:fs}", l);
    	}
    }
}