using System;
using System.Collections.Generic;

namespace Hibernum.Core
{
    public static class ListExtensions
    {
    	private static Random random = new Random();
    	
    	public static void Shuffle<T>(this IList<T> list)  
    	{  
    	    int n = list.Count;  
    	    while (n > 1) {  
    	        n--;  
    	        int k = random.Next(n + 1);  
    	        T value = list[k];  
    	        list[k] = list[n];  
    	        list[n] = value;  
    	    }  
    	}

    	public static void Set<T>(this IList<T> list, int index, T t)
    	{
    		if(index >= list.Count)
    		{
    			list.GrowTo(index);
    		}
    		
    		list[index] = t;
    	}

    	public static T Get<T>(this IList<T> list, int index)
    	{
    		if(index >= list.Count)
    		{
    			return default(T);
    		}
    		
    		return list[index];
    	}
    	
    	public static void GrowTo<T>(this IList<T> list, int index)
    	{
    		var count = index - list.Count + 1;
    		for(var i = 0; i < count; i++)
    		{
    			list.Add(default(T));
    		}
    	}

    	/**
    	 * Removes an element from a list in O(1).
    	 * WARNING: Order of the list is not preserved.
    	 */
    	public static void SwapRemove<T>(this List<T> list, int index)
    	{
    		list[index] = list[list.Count - 1];
    		list.RemoveAt(list.Count - 1);
    	}
    	
    	/**
    	 * Removes an element from a list in O(n).
    	 * WARNING: Order of the list is not preserved.
    	 */
    	public static void SwapRemove<T>(this List<T> list, T item)
    	{
    		int index = list.IndexOf(item);
    		SwapRemove(list, index);
    	}
    	
    	/**
    	 * Removes an element from a list in O(n).
    	 * WARNING: Order of the list is not preserved.
    	 */
    	public static void SwapRemove<T>(this List<T> list, Predicate<T> predicate)
    	{
    		int index = list.FindIndex(predicate);
    		SwapRemove(list, index);
    	}
    }
}
