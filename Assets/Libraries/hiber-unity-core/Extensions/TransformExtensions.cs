using UnityEngine;
using System.Collections.Generic;

namespace Hibernum.Core
{
    public static class TransformExtensions
    {
    	public static bool TryGetComponent<T>(this Transform node, ref T t) where T : Component
    	{
    		t = node.GetComponent<T>();
    		return t != null;
    	}
    	
    	public static T GetComponentInParent<T>(this Transform node) where T : Component
    	{	
    		if (node.parent)
    		{
    			return node.parent.GetComponent<T>();
    		}
    		
    		return null;
    	}
    	
    	public static T GetComponentInParents<T>(this Transform node) where T : Component
    	{	
    		while (node.parent != null)
    		{
    			node = node.parent;
    			
    			T t = node.GetComponent<T>();
    			
    			if (t != null)
    				return t;
    		}
    		
    		return null;
    	}
    	
    	public static List<T> GetListComponentsInChildren<T>(this Transform node) where T : Component
    	{
    		T[] components = node.GetComponentsInChildren<T>();
    		
    		List<T> list = new List<T>();
    		foreach (T c in components) {
    			list.Add(c);
    		}
    		
    		return list;
    	}
    	
    	public static T GetOrAddComponent<T>(this Transform node) where T : Component
    	{
    		T component = node.GetComponent<T>();
    		if(component == null)
    		{
    			component = node.gameObject.AddComponent<T>();
    		}
    		
    		return component;
    	}

    	public static void UnparentAllChildren(this Transform node) 
    	{
    		ReparentAllChildren(node, null);
    	}

    	public static void ReparentAllChildren(this Transform node, Transform parent) 
    	{
    		if(node == parent)
    			return;

    		while(node.childCount != 0)
    		{
    			node.GetChild(0).parent = parent;
    		}
    	}
    	
    	public static void DestroyAllChildren(this Transform node) 
    	{
    		for ( int i=node.childCount-1; i>=0; --i ) {
    		    GameObject.Destroy(node.GetChild(i).gameObject);
    		}
    	}
    	
    	public static void DestroyAllChildrenImmediate(this Transform node)
    	{
    		for ( int i=node.childCount-1; i>=0; --i ) {
    		    GameObject.DestroyImmediate(node.GetChild(i).gameObject);
    		}
    	}

    	/// <summary>
    	/// Search the transform hierarchy for a child by name
    	/// </summary>
    	public static Transform Search(this Transform target, string name)
    	{
    		if (target.name == name) return target;
    		
    		for (int i = 0; i < target.childCount; ++i)
    		{
    			var result = Search(target.GetChild(i), name);
    			
    			if (result != null) return result;
    		}
    		
    		return null;
    	}

    	public static int GetActiveChildCount(this Transform node)
    	{
    		int count = 0;
    		for ( int i=node.childCount-1; i>=0; --i ) {
    			if (node.GetChild(i).gameObject.activeInHierarchy) {
    				count++;
    			}
    		}
    		return count;
    	}
    	
    	public static Transform SearchContains(this Transform target, string str)
    	{	
    		if (target.name.ToLower().Contains(str.ToLower())) return target;
    		
    		for (int i = 0; i < target.childCount; ++i)
    		{
    			var result = SearchContains(target.GetChild(i), str);
    			
    			if (result != null) return result;
    		}
    		
    		return null;
    	}

		public static bool IsChildOf(this Transform target, Transform parentToCheckFor)
		{
			Transform parent = target.parent;
			while(parent != null)
			{
				if(parent == parentToCheckFor){ return true; }
				parent = parent.parent;
			}

			return false;
		}

		public static bool IsParentOf(this Transform target, Transform childToCheckFor)
		{
			Transform parent = childToCheckFor.parent;
			while(parent != null)
			{
				if(parent == target){ return true; }
				parent = parent.parent;
			}

			return false;
		}

    	public static Vector3 GetWorldScale(this Transform t)
    	{
    		return t.localToWorldMatrix.MultiplyVector(t.localScale);
    	}

    }
}