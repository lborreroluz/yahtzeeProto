﻿using UnityEngine;
using System.Collections;

namespace Hibernum.Core
{
    public static class HashtableExtensions
    {
    	public static object GetValueOrDefault(this Hashtable table, string key, object defaultValue = default(object))
    	{
    		if(!table.ContainsKey (key)){ return defaultValue; }
    		return table[key];
    	}
    }

}