using UnityEngine;
using System.Collections.Generic;

namespace Hibernum.Core
{
    public static class GameObjectExtensions
    {
    	public static void DisableAllBehaviours(this GameObject node)
    	{
    		var comps = node.GetComponents<Behaviour>();
    		for(var i = 0; i < comps.Length; i++)
    		{
    			comps[i].enabled = false;
    		}
    	}
    	public static void EnableAllBehaviours(this GameObject node)
    	{
    		var comps = node.GetComponents<Behaviour>();
    		for(var i = 0; i < comps.Length; i++)
    		{
    			comps[i].enabled = true;
    		}
    	}

    	public static GameObject[] FindChildrenWithTag(this GameObject node, string tag) 
    	{
    		List<GameObject> objects = new List<GameObject>();
    		
    		foreach (Transform child in node.transform) {
    			if (child.gameObject.CompareTag(tag)) {
    				objects.Add(child.gameObject);    
    			}
    			
    			objects.AddRange(child.gameObject.FindChildrenWithTag(tag));
    		}
    		
    		return objects.ToArray();
    	}
    	
    	public static bool TryGetComponent<T>(this GameObject node, ref T t) where T : Component
    	{
    		return node.transform.TryGetComponent<T>(ref t);
    	}
    	
    	public static T GetComponentInParent<T>(this GameObject node) where T : Component
    	{	
    		return node.transform.GetComponentInParent<T>();
    	}
    	
    	public static T GetComponentInParents<T>(this GameObject node) where T : Component
    	{	
    		return node.transform.GetComponentInParents<T>();
    	}
    	
    	public static List<T> GetListComponentsInChildren<T>(this GameObject node) where T : Component
    	{
    		return node.transform.GetListComponentsInChildren<T>();	
    	}

    	public static T GetOrAddComponent<T>(this GameObject node) where T : Component
    	{
    		T component = node.GetComponent<T>();
    		if(component == null)
    		{
    			component = node.AddComponent<T>();
    		}

    		return component;
    	}

    	public static void DestroyAllChildren(this GameObject node) 
    	{
    		node.transform.DestroyAllChildren();	
    	}
    	
    	public static void DestroyAllChildrenImmediate(this GameObject node)
    	{
    		node.transform.DestroyAllChildrenImmediate();
    	}

    	public static void SetHideFlagsRecursively(this GameObject node, HideFlags flags) 
    	{
    		node.hideFlags = flags;
    		foreach (Transform child in node.transform) {
    			child.gameObject.SetHideFlagsRecursively(flags);
    		}
    	}
    	
    	public static void SetLayerRecursively(this GameObject node, int layer) 
    	{
    		node.layer = layer;
    		foreach (Transform child in node.transform) {
    			child.gameObject.SetLayerRecursively(layer);
    		}
    	}

    	public static void SetChildrenActive(this GameObject node, bool flag) 
    	{
    		foreach (Transform child in node.transform) {
    			child.gameObject.SetActive(flag);	
    		}
    	}

    	public static void SendMessageDelayed(this GameObject node, string message, SendMessageOptions options = SendMessageOptions.RequireReceiver)
    	{
    		Hibernum.Core.CoroutineHelper.Instance.ExecuteActionNextFrame(() => node.SendMessage(message, options));
    	}

    	public static void SendMessageDelayed<T>(this GameObject node, string message, T param, SendMessageOptions options = SendMessageOptions.RequireReceiver)
    	{
    		Hibernum.Core.CoroutineHelper.Instance.ExecuteActionNextFrame(() => node.SendMessage(message, param, options));
    	}

    	public static Transform[] GetFullPath(this GameObject node)
    	{
    		var current = node.transform;
    		var stack = new Stack<Transform>();
    		stack.Push(current);

    		while(current.parent != null)
    		{
    			current = current.parent;
    			stack.Push(current);
    		}

    		return stack.ToArray();
    	}

    	public static void SetAllActive(this GameObject[] goArray, bool active)
    	{
    		for(int i = 0, max = goArray.Length; i < max; ++i)
    		{
    			if(null == goArray[i]){ continue; }
    			goArray[i].SetActive (active);
    		}
    	}

    	public static void SetAllActive(this List<GameObject> goList, bool active)
    	{
    		for(int i = 0, max = goList.Count; i < max; ++i)
    		{
    			if(null == goList[i]){ continue; }
    			goList[i].SetActive (active);
    		}
    	}

    #if UNITY_EDITOR
    	public static void SetStaticRecursively(this GameObject node, bool flag) 
    	{
    		node.isStatic = flag;
    		foreach (Transform child in node.transform) {
    			child.gameObject.SetStaticRecursively(flag);
    		}
    	}
    #endif
    }

}
