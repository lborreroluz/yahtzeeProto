Audio Manager
=============
**Owner:** [Phillipe Côté](mailto:pcote@hibernum.com)

Description
-----------
A set of utilities to manage and play sounds (2D or 3D) and BGMs. Uses Unity 5's audio mixing as a base and adds audio clip variations, audio source pooling and management, and BGM layering and synchronization.

To use, add `using Hibernum.Unity.Audio` to your file. This give you access to the BaseClipVariator class. Using the BaseClipVariator, just call Play(), Play3D(), PlayInMixer(), or Play3DInMixer() to play a clip. A handle class is then returned (of type PlayingClipInfo) for easy manipulation of the playing sound.

Using the Audio Manager window (Windows/Audio Manager) in the editor gives an overview of all the audio assets present in the project. It is also possible in this window to create clip variators that can then be used to play the clips using the Audio Manager system.
