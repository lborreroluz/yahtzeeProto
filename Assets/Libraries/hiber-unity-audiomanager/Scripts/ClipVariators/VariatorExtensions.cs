﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio
{
	/// <summary>
	/// Extension class for variators. Allow variators to be played with an inline null-check
	/// </summary>
	public static class VariatorExtensions
	{
		#region Base Variator
		[HideInInspector]
		public static PlayingClipInfo PlayClip(this BaseClipVariator variator, bool autoplay = true)
		{
			if(variator == null)
			{
				Warn ();
				return PlayingClipInfo.GetNull();
			}

			return AudioManager.Instance.PlayClip (variator.GetClipDefinition (), autoplay); 
		}

		[HideInInspector]
		public static PlayingClipInfo PlayClipInMixerGroup(this BaseClipVariator variator, AudioMixerGroup mixerGroup, bool autoplay = true)
		{
			if(variator == null)
			{
				Warn ();
				return PlayingClipInfo.GetNull();
			}

			return AudioManager.Instance.PlayClipInMixerGroup (variator.GetClipDefinition (), mixerGroup, autoplay);
		}

		[HideInInspector]
		public static PlayingClipInfo PlayClip3D(this BaseClipVariator variator, Transform parent, bool autoplay = true)
		{
			if(variator == null)
			{
				Warn ();
				return PlayingClipInfo.GetNull();
			}

			return AudioManager.Instance.PlayClip3D (variator.GetClipDefinition (), parent, autoplay);
		}

		[HideInInspector]
		public static PlayingClipInfo PlayClip3D(this BaseClipVariator variator, Vector3 position, bool autoplay = true)
		{
			if(variator == null)
			{
				Warn ();
				return PlayingClipInfo.GetNull();
			}

			return AudioManager.Instance.PlayClip3D (variator.GetClipDefinition (), position, autoplay);
		}

		[HideInInspector]
		public static PlayingClipInfo PlayClip3DInMixerGroup(this BaseClipVariator variator, AudioMixerGroup mixerGroup, Transform parent, bool autoplay = true)
		{
			if(variator == null)
			{
				Warn ();
				return PlayingClipInfo.GetNull();
			}

			return AudioManager.Instance.PlayClip3DInMixerGroup (variator.GetClipDefinition (), mixerGroup, parent, autoplay);
		}

		[HideInInspector]
		public static PlayingClipInfo PlayClip3DInMixerGroup(this BaseClipVariator variator, AudioMixerGroup mixerGroup, Vector3 position, bool autoplay = true)
		{
			if(variator == null)
			{
				Warn ();
				return PlayingClipInfo.GetNull();
			}

			return AudioManager.Instance.PlayClip3DInMixerGroup (variator.GetClipDefinition (), mixerGroup, position, autoplay);
		}
		#endregion Base Variator

		#region Clip Variator
		[HideInInspector]
		public static PlayingClipInfo PlayClip(this ClipVariator variator, int index, bool autoplay = true)
		{
			if(variator == null)
			{
				Warn ();
				return PlayingClipInfo.GetNull();
			}

			return AudioManager.Instance.PlayClip (variator.GetClipDefinition (index), autoplay); 
		}

		[HideInInspector]
		public static PlayingClipInfo PlayClipInMixerGroup(this ClipVariator variator, int index, AudioMixerGroup mixerGroup, bool autoplay = true)
		{
			if(variator == null)
			{
				Warn ();
				return PlayingClipInfo.GetNull();
			}
			return AudioManager.Instance.PlayClipInMixerGroup (variator.GetClipDefinition (index), mixerGroup, autoplay);
		}

		[HideInInspector]
		public static PlayingClipInfo PlayClip3D(this ClipVariator variator, int index, Transform parent, bool autoplay = true)
		{
			if(variator == null)
			{
				Warn ();
				return PlayingClipInfo.GetNull();
			}

			return AudioManager.Instance.PlayClip3D (variator.GetClipDefinition (index), parent, autoplay);
		}

		[HideInInspector]
		public static PlayingClipInfo PlayClip3D(this ClipVariator variator, int index, Vector3 position, bool autoplay = true)
		{
			if(variator == null)
			{
				Warn ();
				return PlayingClipInfo.GetNull();
			}

			return AudioManager.Instance.PlayClip3D (variator.GetClipDefinition (index), position, autoplay);
		}

		[HideInInspector]
		public static PlayingClipInfo PlayClip3DInMixerGroup(this ClipVariator variator, int index, AudioMixerGroup mixerGroup, Transform parent, bool autoplay = true)
		{
			if(variator == null)
			{
				Warn ();
				return PlayingClipInfo.GetNull();
			}

			return AudioManager.Instance.PlayClip3DInMixerGroup (variator.GetClipDefinition (index), mixerGroup, parent, autoplay);
		}

		[HideInInspector]
		public static PlayingClipInfo PlayClip3DInMixerGroup(this ClipVariator variator, int index, AudioMixerGroup mixerGroup, Vector3 position, bool autoplay = true)
		{
			if(variator == null)
			{
				Warn ();
				return PlayingClipInfo.GetNull();
			}

			return AudioManager.Instance.PlayClip3DInMixerGroup (variator.GetClipDefinition (index), mixerGroup, position, autoplay);
		}
		#endregion

		#region BGM Variator
		[HideInInspector]
		public static PlayingBGMInfo Play(this BGMClipVariator variator, float fadeTime)
		{
			if(variator == null)
			{
				Warn ();
				return new PlayingBGMInfo(PlayingClipInfo.GetNull (), "INVALID");
			}

			return AudioManager.Instance.BGMModule.Play (variator, fadeTime);
		}

		[HideInInspector]
		public static PlayingBGMInfo PlaySingle(this BGMClipVariator variator, float fadeTime)
		{
			return AudioManager.Instance.BGMModule.PlaySingle (variator, fadeTime);
		}
		#endregion

		[System.Diagnostics.Conditional ("UNITY_EDITOR")]
		public static void Warn()
		{
			switch(AudioManager.Instance.NullVariatorLogLevel)
			{
			case AudioManager.LogLevel.Info:
				Debug.Log ("[AUDIO] Trying to play a null Variator (Check stacktrace for details)"); 
				break;

			case AudioManager.LogLevel.Warning:
				Debug.LogWarning ("[AUDIO] Trying to play a null Variator (Check stacktrace for details)"); 
				break;

			case AudioManager.LogLevel.Error:
				Debug.LogError ("[AUDIO] Trying to play a null Variator (Check stacktrace for details)"); 
				break;

			case AudioManager.LogLevel.Exception:
				throw new UnityException ("[AUDIO] Trying to play a null Variator (Check stacktrace for details)"); 
			}
		}
	}
}