﻿using UnityEngine;
using System.Collections;
using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio
{
	public class BGMClipVariator : BaseClipVariator
	{
		[Tooltip ("The BGM audio clip")]
		[SerializeField] private AudioClip _bgm;
		[Tooltip ("Used to get the playing layer by name from the manager")]
		[SerializeField] private string _layerName;

		public string LayerName { get { return _layerName; } }
		public AudioClip BGM { get { return _bgm; } }

		public override AudioClipDefinition GetClipDefinition ()
		{
			AudioClipDefinition def = new AudioClipDefinition();

			def.Clip = _bgm;
			def.Volume = 1f;
			def.Pitch = 1f;
			def.MixerGroup = _defaultMixerGroup;
			def.SourceVariator = this;

			return def;
		}

		public override bool HasClip (AudioClip clip)
		{
			return _bgm == clip;
		}

		public static BGMClipVariator Create(AudioClip bgm)
		{
			BGMClipVariator newVariator = ScriptableObject.CreateInstance<BGMClipVariator>();
			newVariator._bgm = bgm;

			return newVariator;
		}
	}
}
