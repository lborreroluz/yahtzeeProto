﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio
{
	public class ClipVariator : BaseClipVariator
	{
		public enum PlayType
		{
			Random,
			Sequence,
		}

		[System.Serializable]
		public struct Variation
		{
			public bool volumeOverride;
			public MinMaxFloat volume;

			public bool pitchOverride;
			public MinMaxFloat pitch;

			public AudioClip clip;

			public Variation(AudioClip audioClip)
			{
				volumeOverride = false;
				volume.min = volume.max = 1f;

				pitchOverride = false;
				pitch.min = pitch.max = 1f;

				clip = audioClip;
			}
		}

		[SerializeField] private PlayType _playType;
		[SerializeField] private MinMaxFloat _volume;
		[SerializeField] private MinMaxFloat _pitch;
		[SerializeField] private List<Variation> _variations;

		public int ClipCount { get { return _variations.Count; } }

		private int _sequenceIndex = 0;

		public override AudioClipDefinition GetClipDefinition ()
		{
			switch(_playType)
			{
			case PlayType.Random:
				return GetClipDefinition (Random.Range (0, _variations.Count));

			case PlayType.Sequence:
				AudioClipDefinition def = GetClipDefinition(_sequenceIndex);
				_sequenceIndex = Mathf.Min (++_sequenceIndex, ClipCount-1);
				return def;
			}

			return AudioClipDefinition.Null;
		}

		public AudioClipDefinition GetClipDefinition(int index)
		{
			Variation variation = _variations[index];
			AudioClipDefinition def = new AudioClipDefinition();

			def.Clip = variation.clip;
			def.Volume = (variation.volumeOverride ? variation.volume : _volume).Value;
			def.Pitch = (variation.pitchOverride ? variation.pitch : _pitch).Value;
			def.MixerGroup = _defaultMixerGroup;
			def.SourceVariator = this;

			return def;
		}

		public void ResetSequence()
		{
			_sequenceIndex = 0;
		}

		public override bool HasClip (AudioClip clip)
		{
			for(int i = 0; i < _variations.Count; ++i)
			{
				if(_variations[i].clip == clip)
					return true;
			}

			return false;
		}

		public AudioClipDefinition this[int index]
		{
			get {
				return GetClipDefinition (index);
			}
		}

		public static ClipVariator Create(AudioClip clip){ return Create (new AudioClip[]{clip}); }
		public static ClipVariator Create(params AudioClip[] clips)
		{
			ClipVariator newVariator = ScriptableObject.CreateInstance<ClipVariator>();
			newVariator._volume.min = newVariator._volume.max = 1f;
			newVariator._pitch.min = newVariator._pitch.max = 1f;

			newVariator._variations = new List<Variation>();
			for(int i = 0; i < clips.Length; ++i)
				newVariator._variations.Add (new Variation(clips[i]));

			return newVariator;
		}
	}
}
