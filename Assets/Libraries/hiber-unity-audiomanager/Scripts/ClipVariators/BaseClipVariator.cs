﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

namespace Hibernum.Unity.Audio.Internal
{
	[System.Serializable]
	public abstract class BaseClipVariator : ScriptableObject
	{
		[Tooltip ("The mixer group used by this variator. Can be overriden by using PlayInMixer() methods.")]
		[SerializeField] protected AudioMixerGroup _defaultMixerGroup;
		[Tooltip ("The maximum count of this variator that can be playing at one time [0 = unlimited]")]
		[SerializeField] protected int _maxConcurrentPlaying = 0;
		[Tooltip ("Does this sound persist across scene loads?")]
		[SerializeField] protected bool _persistent = true;
		[Tooltip ("Is this sound looping?")]
		[SerializeField] protected bool _looping = false;

		public AudioMixerGroup DefaultMixerGroup { get { return _defaultMixerGroup; } }
		public int MaxConcurrentPlaying { get { return _maxConcurrentPlaying; } }
		public bool Persistent { get { return _persistent; } }
		public bool Looping { get { return _looping; } }

		public abstract AudioClipDefinition GetClipDefinition();
		public abstract bool HasClip(AudioClip clip);

		public static implicit operator AudioClipDefinition(BaseClipVariator variator)
		{
			if(variator == null)
				return AudioClipDefinition.Null;
			
			AudioClipDefinition def = variator.GetClipDefinition ();
			return def;
		}

		#if UNITY_EDITOR
		private void OnValidate()
		{
			_maxConcurrentPlaying = Mathf.Max (_maxConcurrentPlaying, 0);
		}
		#endif
	}
}