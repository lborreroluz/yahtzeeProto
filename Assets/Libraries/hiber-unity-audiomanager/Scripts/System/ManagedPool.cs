﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Hibernum.Unity.Audio
{
	/// <summary>
	/// A generic managed pool.
	/// </summary>
	public class ManagedPool<T>
	{
		private Stack<T> _pooled;
		private System.Func<T> _createMethod;

		/// <summary>
		/// Initializes a new instance of the <see cref="Hibernum.Unity.Audio.ManagedPool"/> class.
		/// </summary>
		/// <param name="startCount">Initial instance count.</param>
		/// <param name="createMethod">Method to create a new instance.</param>
		public ManagedPool(int startCount, System.Func<T> createMethod)
		{
			_pooled = new Stack<T>(startCount);
			_createMethod = createMethod;

			for(int i = 0; i < startCount; ++i)
				_pooled.Push (createMethod ());
		}

		/// <summary>
		/// Get the next instance.
		/// </summary>
		public T Get()
		{
			if(_pooled.Count > 0)
				return _pooled.Pop();
			else
				return _createMethod();
		}

		/// <summary>
		/// Free the specified instance.
		/// </summary>
		/// <param name="toFree">To free.</param>
		public void Free(T toFree)
		{
			_pooled.Push (toFree);
		}
	}
}
