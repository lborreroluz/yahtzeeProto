﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;

namespace Hibernum.Unity.Audio
{
	/// <summary>
	/// BGM Module used by the AudioManger to wrap
	/// BGM layering and fade in/out
	/// </summary>
	[System.Serializable]
	public class BGMModule
	{
		private List<PlayingBGMInfo> _bgmLayers = new List<PlayingBGMInfo>();

		public bool allowFadeToSelf;
		public bool layersAreSynced;
		public AudioMixerGroup defaultMixerGroup;

		public int BGMLayerCount { get { return _bgmLayers.Count; } }

		/// <summary>
		/// Plays a bgm track and stops all other layers
		/// </summary>
		/// <returns>The playing info.</returns>
		/// <param name="bgm">Bgm to play.</param>
		/// <param name="fadeTime">Fade time.</param>
		[HideInInspector]
		public PlayingBGMInfo PlaySingle(BGMClipVariator bgm, float fadeTime = -1f)
		{
			PlayingBGMInfo selfInfo = null;
			for(int i = _bgmLayers.Count - 1; i >= 0; --i)
			{
				// Stop the layer if it's null or not the same BGM
				if(bgm == null || _bgmLayers[i].ClipInfo.Clip != bgm.BGM)
					Stop (_bgmLayers[i], fadeTime);
				else
				{
					selfInfo = _bgmLayers[i];
					// If we can fade to self, stop this layer also
					if(allowFadeToSelf)
						Stop (_bgmLayers[i], fadeTime);
				}
			}

			if(selfInfo == null || allowFadeToSelf)
				return Play (bgm, fadeTime);

			return selfInfo;
		}

		/// <summary>
		/// Fade all the currently playing layers to the specified bgm
		/// </summary>
		/// <param name="bgm">Bgm.</param>
		/// <param name="fadeTime">Fade time.</param>
		[HideInInspector]
		public PlayingBGMInfo Play(BGMClipVariator bgm, float fadeTime = -1f)
		{
			PlayingBGMInfo oldInfo = _bgmLayers.Find ((x)=>{ return x.ClipInfo.Clip == bgm.BGM; });
			float startTime = 0f;

			if(oldInfo != null)
			{
				if(allowFadeToSelf)
				{
					startTime = oldInfo.ClipInfo.Source.time;
					Stop (oldInfo, fadeTime);
				}
				else
				{
					oldInfo.FadeIn (fadeTime);
					return oldInfo;
				}
			}

			if(bgm == null)
				return new PlayingBGMInfo(PlayingClipInfo.GetNull(), "");

			PlayingBGMInfo newBGMInfo = null;
			if(bgm.DefaultMixerGroup == null)
				newBGMInfo = new PlayingBGMInfo(bgm.PlayClipInMixerGroup (defaultMixerGroup), bgm.LayerName);
			else
				newBGMInfo = new PlayingBGMInfo(bgm.PlayClip (), bgm.LayerName);

			_bgmLayers.Add (newBGMInfo);

			newBGMInfo.ClipInfo.Source.volume = 0f;
			if(fadeTime < 0)
				newBGMInfo.Mute ();
			else
				newBGMInfo.FadeIn (fadeTime);

			if(layersAreSynced && _bgmLayers.Count > 0)
				startTime = _bgmLayers[0].ClipInfo.Source.time;

			newBGMInfo.ClipInfo.SetTime (startTime);

			return newBGMInfo;
		}

		/// <summary>
		/// Gets the layer witht the specified name.
		/// </summary>
		/// <returns>The layer.</returns>
		/// <param name="layerName">Layer name.</param>
		public PlayingBGMInfo GetLayer(string layerName)
		{
			return _bgmLayers.Find ((x)=>{ return x.LayerName == layerName; });
		}

		/// <summary>
		/// Stop the specified bgm using the provided fade-out time.
		/// </summary>
		/// <param name="info">Info.</param>
		/// <param name="fadeTime">Fade out time.</param>
		public void Stop(PlayingBGMInfo info, float fadeTime = 0f)
		{
			_bgmLayers.Remove (info);
			info.Stop (fadeTime);
		}

		/// <summary>
		/// Stop the specified bgm using the provided fade-out time.
		/// </summary>
		/// <param name="layerName">Name of the layer to stop.</param>
		/// <param name="fadeTime">Fade out time.</param>
		public void Stop(string layerName, float fadeTime = 0f)
		{
			PlayingBGMInfo info = GetLayer (layerName);
			if(info != null)
				Stop (info, fadeTime);
		}

		/// <summary>
		/// Stops all the playing bgm layers.
		/// </summary>
		/// <param name="fadeTime">Fade out time.</param>
		public void StopAll(float fadeTime = 0f)
		{
			for(int i = _bgmLayers.Count - 1; i >= 0; --i)
				Stop (_bgmLayers[i], fadeTime);
		}

		/// <summary>
		/// Is a layer with the bgm's name already loaded?
		/// </summary>
		/// <param name="bgm">BGM to check for.</param>
		public bool LayerIsLoaded(BGMClipVariator bgm)
		{
			return LayerIsLoaded (bgm.LayerName);
		}

		/// <summary>
		/// Is a layer with this name already loaded?
		/// </summary>
		/// <param name="layerName">Layer name.</param>
		public bool LayerIsLoaded(string layerName)
		{
			return _bgmLayers.Find((x)=>{ return x.LayerName == layerName; }) != null;
		}
	}
}
