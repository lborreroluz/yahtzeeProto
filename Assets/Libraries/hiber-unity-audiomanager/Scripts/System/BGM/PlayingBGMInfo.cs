﻿using UnityEngine;
using System.Collections;

namespace Hibernum.Unity.Audio
{
	/// <summary>
	/// Wraps the playing clip info for BGM
	/// </summary>
	public class PlayingBGMInfo
	{
		/// <summary>
		/// The clip info of the BGM
		/// </summary>
		/// <value>The clip info.</value>
		public PlayingClipInfo ClipInfo { get; private set; }
		/// <summary>
		/// The layer name of this playing BGM
		/// </summary>
		/// <value>The name of the layer.</value>
		public string LayerName { get; set; }
		/// <summary>
		/// Gets a value indicating whether this <see cref="PlayingBGMInfo"/> is invalid.
		/// </summary>
		public bool Invalid { get { return ClipInfo == null || ClipInfo.Invalid;  } }
		/// <summary>
		/// Gets the currrent volume of the BGM (in percentage).
		/// </summary>
		/// <value>The currrent volume.</value>
		public float CurrrentVolume { get { return ClipInfo.Source.volume / ClipInfo.ClipDefinition.Volume; } }

		private Coroutine _fadeRoutine;

		/// <summary>
		/// Initializes a new instance of the <see cref="PlayingBGMInfo"/> class.
		/// </summary>
		/// <param name="baseInfo">Base clip info.</param>
		/// <param name="layerName">Layer name.</param>
		public PlayingBGMInfo(PlayingClipInfo baseInfo, string layerName)
		{
			ClipInfo = baseInfo;
			ClipInfo.SetLooping (true);
			ClipInfo.Play ();
			ClipInfo.MarkPersistent (true);

			LayerName = layerName;
		}

		/// <summary>
		/// Stop the BGM by fading over fade time.
		/// </summary>
		/// <param name="fadeTime">Time to fade out.</param>
		public void Stop(float fadeTime = 0f)
		{
			ClipInfo.Stop (fadeTime);
		}

		/// <summary>
		/// Sets the volume to 0%
		/// </summary>
		public PlayingBGMInfo Mute()
		{
			if(Invalid)
				return this;
			
			ClipInfo.Mute ();
			return this;
		}

		/// <summary>
		/// Sets the volume to 100%
		/// </summary>
		public PlayingBGMInfo Unmute()
		{
			if(Invalid)
				return this;
			
			ClipInfo.Unmute ();
			return this;
		}

		/// <summary>
		/// Fades the BGM volume in.
		/// </summary>
		/// <param name="time">Time ot fade in.</param>
		/// <param name="onDone">Callback when fade in is done.</param>
		public PlayingBGMInfo FadeIn(float time, System.Action onDone = null)
		{
			if(Invalid)
				return this;
			
			ClipInfo.FadeIn (time, onDone);
			return this;
		}

		/// <summary>
		/// Fades out the volume over time
		/// </summary>
		/// <param name="time">Time to fade out.</param>
		/// <param name="onDone">Callback when fade out is done.</param>
		public PlayingBGMInfo FadeOut(float time, System.Action onDone = null)
		{
			if(Invalid)
				return this;
			
			ClipInfo.FadeOut (time, onDone);
			return this;
		}
	}
}
