﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

namespace Hibernum.Unity.Audio.Internal
{
	/// <summary>
	/// Audio clip definition used by the audio manager to play clips.
	/// </summary>
	public struct AudioClipDefinition
	{
		/// <summary>
		/// The clip to play
		/// </summary>
		public AudioClip Clip { get; set; }
		/// <summary>
		/// The volume of the source
		/// </summary>
		public float Volume { get; set; }
		/// <summary>
		/// The pitch of the source
		/// </summary>
		public float Pitch { get; set; }
		/// <summary>
		/// The mixer to output to
		/// </summary>
		public AudioMixerGroup MixerGroup { get; set; }
		/// <summary>
		/// The variator that was used to create this clip definition
		/// </summary>
		/// <value>The source variator.</value>
		public BaseClipVariator SourceVariator { get; set; }

		/// <summary>
		/// Returns a definition containing a null clip and null mixer
		/// </summary>
		public static AudioClipDefinition Null{
			get {
				return new AudioClipDefinition(){
					Clip = null,
					Volume = 1f,
					Pitch = 1f,
					MixerGroup = null,
					SourceVariator = null,
				};
			}
		}

		public static implicit operator AudioClipDefinition(AudioClip clip)
		{
			return new AudioClipDefinition(){
				Clip = clip,
				Volume = 1f,
				Pitch = 1f,
				MixerGroup = null,
				SourceVariator = null,
			};
		}
	}
}