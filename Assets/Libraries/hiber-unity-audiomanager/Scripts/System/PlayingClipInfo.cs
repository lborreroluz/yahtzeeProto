﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using Hibernum.Unity.Audio.Internal;
using System.Collections.Generic;

namespace Hibernum.Unity.Audio
{
	/// <summary>
	/// Wraps a clip that is currently playing
	/// </summary>
	public class PlayingClipInfo
	{
		/// <summary>
		/// Raised when the info is disposed of
		/// </summary>
		public event System.Action<PlayingClipInfo> OnDisposed;

		/// <summary>
		/// The audio source that is playing this clip
		/// </summary>
		public AudioSource Source { get { return _source; } }
		/// <summary>
		/// The clip definition used to play this clip
		/// </summary>
		public AudioClipDefinition ClipDefinition { get { return _clipDefintion; } }
		/// <summary>
		/// The audio clip playing
		/// </summary>
		public AudioClip Clip { get { return _clipDefintion.Clip; } }
		/// <summary>
		/// Gets a value indicating whether the clip is playing.
		/// </summary>
		public bool IsPlaying { get { return !_disposed && _source.isPlaying && _source.clip == _clipDefintion.Clip; } }
		/// <summary>
		/// Gets a value indicating whether this <see cref="PlayingClipInfo"/> is disposed.
		/// </summary>
		public bool Disposed { get { return _disposed; } }
		/// <summary>
		/// Gets a value indicating whether this clip is persistent.
		/// Which means it survives a scene change.
		/// </summary>
		public bool Persistent { get; private set; }
		/// <summary>
		/// Gets a value indicating whether this clip is paused
		/// </summary>
		public bool IsPaused { get; private set; }
		/// <summary>
		/// Gets a value indicating whether this <see cref="PlayingClipInfo"/> is invalid.
		/// </summary>
		public bool Invalid { get { return _disposed || null == _source; } }
		/// <summary>
		/// Gets the current volume of the audio source.
		/// </summary>
		public float Volume { get { return _source == null ? -1f : _source.volume; } }
		/// <summary>
		/// Gets the current pitch of the audio source.
		/// </summary>
		public float Pitch { get { return _source == null ? -1f : _source.pitch; } }
		/// <summary>
		/// Gets the normalized current time
		/// </summary>
		public float NormalizedTime { get { return Invalid ? -1f : _source.time / Clip.length; } }

		#if UNITY_EDITOR
		public System.Diagnostics.StackFrame[] EDITOR_StackTrace { get; private set; }
		#endif

		private AudioSource _source;
		private AudioClipDefinition _clipDefintion;

		private bool _disposed = false;
		private Coroutine _fadeRoutine;

		/// <summary>
		/// Initializes a new instance of the <see cref="PlayingClipInfo"/> class.
		/// </summary>
		/// <param name="source">Source to play from.</param>
		/// <param name="clipDef">Clip definition to use.</param>
		[HideInInspector]
		public PlayingClipInfo(AudioSource source, AudioClipDefinition clipDef)
		{
			_source = source;
			_clipDefintion = clipDef;

			#if UNITY_EDITOR
			Profiler.BeginSample ("EDITOR_ONLY");
			var frames =  new System.Diagnostics.StackTrace (true).GetFrames ();
			List<System.Diagnostics.StackFrame> finalFrames = new List<System.Diagnostics.StackFrame>();
			for(int i = 0; i < frames.Length; ++i)
			{
				if(frames[i].GetFileLineNumber () > 0 && frames[i].GetMethod ().GetCustomAttributes (typeof(HideInInspector), true).Length <= 0)
					finalFrames.Add (frames[i]);
			}

			EDITOR_StackTrace = finalFrames.ToArray ();
			Profiler.EndSample ();
			#endif
		}

		/// <summary>
		/// Starts the audio clip.
		/// </summary>
		public PlayingClipInfo Play()
		{
			if(Invalid)
				return this;

			FadeStop ();
			_source.Play ();
			_source.volume = ClipDefinition.Volume;
			IsPaused = false;

			return this;
		}

		/// <summary>
		/// Starts the audio clip and fades in.
		/// </summary>
		/// <param name="time">Fade in time.</param>
		public PlayingClipInfo Play(float time)
		{
			Play ();
			FadeIn (time);

			return this;
		}

		/// <summary>
		/// Resumes the audio clip.
		/// </summary>
		public PlayingClipInfo Resume()
		{
			if(Invalid)
				return this;

			FadeStop ();
			_source.UnPause ();
			_source.volume = ClipDefinition.Volume;
			IsPaused = false;

			return this;
		}

		/// <summary>
		/// Resumes the audio clip and fades in.
		/// </summary>
		/// <param name="time">Fade int time.</param>
		public PlayingClipInfo Resume(float time)
		{
			Resume ();
			FadeIn (time);

			return this;
		}

		/// <summary>
		/// Pauses the audio clip
		/// </summary>
		public PlayingClipInfo Pause()
		{
			if(Invalid)
				return this;

			FadeStop ();
			_source.Pause ();
			IsPaused = true;

			return this;
		}

		/// <summary>
		/// Pauses the audio clip after fading out
		/// </summary>
		/// <param name="time">Fade out time.</param>
		public PlayingClipInfo Pause(float time)
		{
			FadeOut (time, ()=>{Pause();});
			return this;
		}

		/// <summary>
		/// Restarts the audio clip
		/// </summary>
		public PlayingClipInfo Restart()
		{
			if(Invalid)
				return this;

			SetTime (0f);
			Play ();

			return this;
		}

		/// <summary>
		/// Restarts the audio clip using a fade out and fade in
		/// </summary>
		/// <param name="fadeOutTime">Fade out time.</param>
		/// <param name="fadeInTime">Fade in time.</param>
		public PlayingClipInfo Restart(float fadeOutTime, float fadeInTime)
		{
			FadeOut (fadeOutTime, ()=>{
				Restart ();
				FadeIn (fadeInTime);
			});

			return this;
		}

		/// <summary>
		/// Stops the clip and disposes of the <see cref="PlayingClipInfo"/>
		/// </summary>
		public PlayingClipInfo Stop()
		{
			if(Invalid)
				return this;

			FadeStop ();
			IsPaused = false;
			_source.Stop ();
			_source.clip = null;

			Dispose ();
			return this;
		}

		/// <summary>
		/// Stops the clip after fading out and disposes of the <see cref="PlayingClipInfo"/>
		/// </summary>
		/// <param name="time">Fade out time.</param>
		public PlayingClipInfo Stop(float time)
		{
			FadeOut (time, ()=>{Stop();});
			return this;
		}

		/// <summary>
		/// Sets wether the clip is looping.
		/// </summary>
		/// <param name="loop">If set to <c>true</c> loop.</param>
		public PlayingClipInfo SetLooping(bool loop)
		{
			if(Invalid)
				return this;
			
			_source.loop = loop;
			return this;
		}

		/// <summary>
		/// Sets the current time of the clip
		/// </summary>
		/// <param name="time">Time.</param>
		public PlayingClipInfo SetTime(float time)
		{
			if(Invalid)
				return this;
			
			_source.time = time;
			return this;
		}

		/// <summary>
		/// Releases all resource used by the <see cref="PlayingClipInfo"/> object.
		/// </summary>
		/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="Hibernum.Unity.Audio.PlayingClipInfo"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="Hibernum.Unity.Audio.PlayingClipInfo"/> in an unusable state.
		/// After calling <see cref="Dispose"/>, you must release all references to the
		/// <see cref="Hibernum.Unity.Audio.PlayingClipInfo"/> so the garbage collector can reclaim the memory that the
		/// <see cref="Hibernum.Unity.Audio.PlayingClipInfo"/> was occupying.</remarks>
		public PlayingClipInfo Dispose()
		{
			if(_disposed)
				return this;

			if(AudioManager.Exists)
				AudioManager.Instance.DisposeClipInfo (this);

			IsPaused = false;
			_source = null;
			_disposed = true;

			if(OnDisposed != null)
				OnDisposed.Invoke (this);
			
			return this;
		}

		/// <summary>
		/// Sets the 3D settings.
		/// </summary>
		/// <param name="minDistance">Minimum distance.</param>
		/// <param name="maxDistance">Max distance.</param>
		public PlayingClipInfo Set3DSettings(float minDistance, float maxDistance)
		{
			if(Invalid)
				return this;

			_source.minDistance = minDistance;
			_source.maxDistance = maxDistance;

			return this;
		}

		/// <summary>
		/// Chain another clip when this one is done.
		/// </summary>
		/// <param name="nextInfo">Next clip.</param>
		public PlayingClipInfo Chain(PlayingClipInfo nextInfo)
		{
			if(Invalid)
				return this;

			nextInfo.Pause ();
			OnDisposed += (i)=>{ nextInfo.Play (); };
			return this;
		}

		/// <summary>
		/// Makes the sound persistent.
		/// Meaning it will keep playing through a scene change.
		/// </summary>
		public PlayingClipInfo MarkPersistent(bool persistent)
		{
			Persistent = persistent;
			return this;
		}

		/// <summary>
		/// Sets the volume to 0%
		/// </summary>
		public PlayingClipInfo Mute()
		{
			FadeOut (0);
			return this;
		}

		/// <summary>
		/// Sets the volume to 100%
		/// </summary>
		public PlayingClipInfo Unmute()
		{
			FadeIn (0);
			return this;
		}

		/// <summary>
		/// Fades in the source's volume over time.
		/// </summary>
		/// <param name="time">Fade in time.</param>
		/// <param name="onDone">On done callback.</param>
		public PlayingClipInfo FadeIn(float time, System.Action onDone = null)
		{
			if(_disposed)
				return this;

			if(null != _fadeRoutine)
				AudioManager.Instance.StopCoroutine (_fadeRoutine);
			
			_fadeRoutine = AudioManager.Instance.StartCoroutine (FadeFromTo (time, 0f, ClipDefinition.Volume, onDone));

			return this;
		}

		/// <summary>
		/// Fades out the source's volume over time.
		/// </summary>
		/// <param name="time">Fade out time.</param>
		/// <param name="onDone">On done callback.</param>
		public PlayingClipInfo FadeOut(float time, System.Action onDone = null)
		{
			if(_disposed)
				return this;

			FadeStop ();
			_fadeRoutine = AudioManager.Instance.StartCoroutine (FadeFromTo (time, Source.volume, 0f, onDone));

			return this;
		}

		/// <summary>
		/// Sets the volume over time.
		/// </summary>
		/// <returns>The volume.</returns>
		/// <param name="volume">Target volume.</param>
		/// <param name="time">Time over which to set the volume.</param>
		/// <param name="onDone">On done callback.</param>
		public PlayingClipInfo SetVolume(float volume, float time, System.Action onDone = null)
		{
			if(_disposed)
				return this;

			FadeStop ();
			_fadeRoutine = AudioManager.Instance.StartCoroutine (FadeFromTo (time, Source.volume, volume, onDone));

			return this;
		}

		/// <summary>
		/// Fades the source's volume over time
		/// </summary>
		/// <param name="time">Fade time.</param>
		/// <param name="fromVolume">Fade from this volume.</param>
		/// <param name="toVolume">Fade to this volume.</param>
		/// <param name="onDone">On done callback.</param>
		private IEnumerator FadeFromTo(float time, float fromVolume, float toVolume, System.Action onDone)
		{
			for(float f = 0; f < time; f += Time.unscaledDeltaTime)
			{
				Source.volume = Mathf.Lerp (fromVolume, toVolume, f / time);
				yield return null;
			}

			Source.volume = toVolume;
			_fadeRoutine = null;

			if(null != onDone)
				onDone.Invoke ();

			yield break;
		}

		/// <summary>
		/// Stops the fading routine if it exists.
		/// </summary>
		private void FadeStop()
		{
			if(null != _fadeRoutine)
				AudioManager.Instance.StopCoroutine (_fadeRoutine);
		}

		public static bool operator !=(PlayingClipInfo l, PlayingClipInfo r){ return !(l == r); }
		public static bool operator ==(PlayingClipInfo l, PlayingClipInfo r)
		{
			// Left is null
			if(object.ReferenceEquals (l, null))
			{
				// Right is null
				if(object.ReferenceEquals (r, null))
					return true;
			
				// Right is not null
				return r._disposed;
			}
			// Left is not null & Right is null
			else if(object.ReferenceEquals (r, null))
				return l._disposed;

			return object.ReferenceEquals (l, r);
		}

		public override bool Equals (object obj)
		{
			return this == (PlayingClipInfo)obj;
		}

		public override int GetHashCode ()
		{
			return base.GetHashCode ();
		}

		/// <summary>
		/// Returns a <see cref="PlayingClipInfo"/> with null source and definition
		/// </summary>
		/// <returns>The null.</returns>
		[HideInInspector]
		public static PlayingClipInfo GetNull()
		{
			return new PlayingClipInfo(null, AudioClipDefinition.Null);
		}
	}
}