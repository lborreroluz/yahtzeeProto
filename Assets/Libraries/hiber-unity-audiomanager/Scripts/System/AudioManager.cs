﻿using UnityEngine;
using System.Collections;
using Hibernum.Core;
using System.Collections.Generic;
using UnityEngine.Audio;
using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio
{
	public partial class AudioManager : IndestructibleSingletonBehaviour<AudioManager>
	{
		public enum LogLevel
		{
			Disabled,
			Info,
			Warning,
			Error,
			Exception,
		}

		[Header ("BGM Settings")]
		[SerializeField] private BGMModule _bgmModule;

		[Header ("Clips Settings")]
		[SerializeField] private int _startingPoolSize = 10;
		[SerializeField] private AudioSource _clipSourceTemplate;

		[Header ("Generic Settings")]
		[SerializeField] private LogLevel _nullVariatorLogLevel;
		[SerializeField] private LogLevel _nullClipLogLevel;
		[SerializeField] private AudioMixer _mixer;

		private ManagedPool<AudioSource> _audioSourcePool;
		private List<KeyValuePair<AudioSource, Transform>> _currentSources = new List<KeyValuePair<AudioSource, Transform>>();
		private List<PlayingClipInfo> _playingClips = new List<PlayingClipInfo>();

		/// <summary>
		/// The BGM Module instance
		/// </summary>
		public BGMModule BGMModule { get { return _bgmModule; } }
		/// <summary>
		/// The level at which to log null variators when encountered.
		/// </summary>
		public LogLevel NullVariatorLogLevel { get { return _nullVariatorLogLevel; } set { _nullVariatorLogLevel = value; } }
		/// <summary>
		/// The level at which to log null clips when encountered.
		/// </summary>
		public LogLevel NullClipLogLevel { get { return _nullVariatorLogLevel; } set { _nullVariatorLogLevel = value; } }

		#region Monobehaviour Methods
		protected override void OnSingletonAwake ()
		{
			if(_clipSourceTemplate == null)
				throw new UnityException ("[AUDIO] The AudioManger needs an AudioSource for the Clip Source Template");

			// Create the new pool
			_audioSourcePool = new ManagedPool<AudioSource>(_startingPoolSize, CreateNewSource);

			// Call the partial awake for project-specific implementations
			OnAudioManagerAwake ();
		}

		private void OnLevelWasLoaded(int level)
		{
			// Stop all sounds that are not persistent on a scene change
			for(int i = _playingClips.Count - 1; i >= 0; --i)
			{
				if(!_playingClips[i].Persistent)
					_playingClips[i].Stop ();
			}
		}

		private void Update()
		{
			// Clean any clips that aren't playing
			CleanupPlayingClips ();

			// Update the position of sources bound to a transform
			for(int i = 0; i < _currentSources.Count; ++i)
			{
				if(_currentSources[i].Value != null)
					_currentSources[i].Key.transform.position = _currentSources[i].Value.position;
			}

			// Call the partial update for project-specific implementations
			OnAudioManagerUpdate ();
		}

		protected override void OnSingletonDestroy ()
		{
			StopAll ();

			// Call the partial destroy for project-specific implementations
			OnAudioManagerDestroy ();
		}
		#endregion Monobehaviour Methods

		partial void OnAudioManagerAwake();
		partial void OnAudioManagerUpdate();
		partial void OnAudioManagerDestroy();

		/// <summary>
		/// Determines whether this a variator can be played, based on the max concurrent count
		/// </summary>
		/// <returns><c>true</c> if this instance can play variator the specified variator; otherwise, <c>false</c>.</returns>
		/// <param name="variator">Variator to test.</param>
		private bool CanPlayVariator(BaseClipVariator variator)
		{
			if(variator.MaxConcurrentPlaying <= 0)
				return true;

			int count = 0;
			for(int i = 0; i < _playingClips.Count; ++i)
			{
				if(_playingClips[i].ClipDefinition.SourceVariator == variator)
				{
					if(++count >= variator.MaxConcurrentPlaying)
						return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Gets a new list containing all playing clips.
		/// </summary>
		/// <returns>The playing clips.</returns>
		public List<PlayingClipInfo> GetPlayingClips()
		{
			List<PlayingClipInfo> clipList = new List<PlayingClipInfo>();
			GetPlayingClips (clipList);
			return clipList;
		}

		/// <summary>
		/// Fills the list with the currently playing clips
		/// </summary>
		/// <param name="clipList">Clip list to fill.</param>
		public void GetPlayingClips(List<PlayingClipInfo> clipList)
		{
			CleanupPlayingClips ();
			clipList.Clear ();
			clipList.AddRange (_playingClips);
		}

		#region Clip Playing
		/// <summary>
		/// Plays a clip with a 3D position in the specified mixer group
		/// </summary>
		/// <returns>The playing clip info.</returns>
		/// <param name="def">Clip definition.</param>
		/// <param name="group">mixer group to target.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="loop">If set to <c>true</c> loop.</param>
		/// <param name="autoStart">If set to <c>true</c> auto start.</param>
		[HideInInspector]
		public PlayingClipInfo PlayClip3DInMixerGroup(AudioClipDefinition def, AudioMixerGroup group, Vector3 position, bool autoStart = true)
		{
			return PlayClip3DInMixerGroup (def, group, position, autoStart);
		}

		/// <summary>
		/// Plays a clip following a transform position in the specified mixer group
		/// </summary>
		/// <returns>The playing clip info.</returns>
		/// <param name="def">Clip definition.</param>
		/// <param name="group">mixer group to target.</param>
		/// <param name="parent">Transform to follow.</param>
		/// <param name="loop">If set to <c>true</c> loop.</param>
		/// <param name="autoStart">If set to <c>true</c> auto start.</param>
		[HideInInspector]
		public PlayingClipInfo PlayClip3DInMixerGroup(AudioClipDefinition def, AudioMixerGroup group, Transform parent, bool autoStart = true)
		{
			def.MixerGroup = group;
			return PlayClip3D (def, parent, autoStart);
		}

		/// <summary>
		/// Plays a clip with a 3D position
		/// </summary>
		/// <returns>The playing clip info.</returns>
		/// <param name="def">Clip definition.</param>
		/// <param name="position">Position of the sound.</param>
		/// <param name="loop">If set to <c>true</c> loop.</param>
		/// <param name="autoStart">If set to <c>true</c> auto start.</param>
		[HideInInspector]
		public PlayingClipInfo PlayClip3D(AudioClipDefinition def, Vector3 position, bool autoStart = true)
		{
			return PlayClipInternal (def, autoStart, position, null, true);
		}

		/// <summary>
		/// Plays a clip following a transform position in the specified mixer group
		/// </summary>
		/// <returns>The playing clip info.</returns>
		/// <param name="def">Clip definition.</param>
		/// <param name="parent">Transform to follow.</param>
		/// <param name="loop">If set to <c>true</c> loop.</param>
		/// <param name="autoStart">If set to <c>true</c> auto start.</param>
		[HideInInspector]
		public PlayingClipInfo PlayClip3D(AudioClipDefinition def, Transform parent, bool autoStart = true)
		{
			return PlayClipInternal (def, autoStart, Vector3.zero, parent, true);
		}

		/// <summary>
		/// Plays a clip in the specified mixer group
		/// </summary>
		/// <returns>The playing clip info.</returns>
		/// <param name="def">Clip definition.</param>
		/// <param name="group">mixer group to target.</param>
		/// <param name="loop">If set to <c>true</c> loop.</param>
		/// <param name="autoStart">If set to <c>true</c> auto start.</param>
		[HideInInspector]
		public PlayingClipInfo PlayClipInMixerGroup(AudioClipDefinition def, AudioMixerGroup group, bool autoStart = true)
		{
			def.MixerGroup = group;
			return PlayClip (def, autoStart);
		}

		/// <summary>
		/// Plays a clip
		/// </summary>
		/// <returns>The playing clip info.</returns>
		/// <param name="def">Clip definition.</param>
		/// <param name="loop">If set to <c>true</c> loop.</param>
		/// <param name="autoStart">If set to <c>true</c> auto start.</param>
		[HideInInspector]
		public PlayingClipInfo PlayClip(AudioClipDefinition def, bool autoStart = true)
		{
			return PlayClipInternal (def, autoStart, Vector3.zero, null, false);
		}

		/// <summary>
		/// Plays the clip
		/// </summary>
		/// <returns>The playing clip info.</returns>
		/// <param name="def">Clip defenition.</param>
		/// <param name="loop">If set to <c>true</c> loop.</param>
		/// <param name="position">World position to play at.</param>
		/// <param name="parent">Parent to follow.</param>
		/// <param name="is3D">If set to <c>true</c> sound is 3d.</param>
		[HideInInspector]
		private PlayingClipInfo PlayClipInternal(AudioClipDefinition def, bool autoStart, Vector3 position, Transform parent, bool is3D)
		{
			CleanupAudioSources ();

			if(def.SourceVariator == null)
				Debug.LogError (string.Format ("[AUDIO] An AudioClipDefinition was played without a variator. What black magic is this?!"));
			else if(!CanPlayVariator (def.SourceVariator))
				return PlayingClipInfo.GetNull ();

			if(!is3D)
				parent = null;

			AudioSource audioSource = _audioSourcePool.Get ();
			audioSource.gameObject.hideFlags = HideFlags.None;
			audioSource.enabled = true;

			if(parent == null)
				audioSource.transform.position = position;

			audioSource.volume = def.Volume;
			audioSource.pitch = def.Pitch;
			audioSource.clip = def.Clip;
			audioSource.loop = def.SourceVariator.Looping;

			audioSource.spatialBlend = is3D ? 1f : 0f;
			audioSource.minDistance = _clipSourceTemplate.minDistance;
			audioSource.maxDistance = _clipSourceTemplate.maxDistance;
			audioSource.rolloffMode = _clipSourceTemplate.rolloffMode;
			audioSource.spread = _clipSourceTemplate.spread;

			#if UNITY_EDITOR
			Profiler.BeginSample ("EDITOR_ONLY");
			audioSource.name = def.Clip.name;
			Profiler.EndSample ();
			#endif

			if(def.MixerGroup != null)
				audioSource.outputAudioMixerGroup = def.MixerGroup;
			else
				audioSource.outputAudioMixerGroup = _clipSourceTemplate.outputAudioMixerGroup;

			audioSource.Play ();

			if(!autoStart)
				audioSource.Pause ();

			_currentSources.Add (new KeyValuePair<AudioSource, Transform>(audioSource, parent));

			#if UNITY_EDITOR
			Profiler.BeginSample ("EDITOR_ONLY");
			if(def.Clip == null)
			{
				switch(AudioManager.Instance.NullClipLogLevel)
				{
				case AudioManager.LogLevel.Info:
					Debug.Log ("[AUDIO] Trying to play a sound without an AudioClip (Check stacktrace for details)");
					break;

				case AudioManager.LogLevel.Warning:
					Debug.LogWarning ("[AUDIO] Trying to play a sound without an AudioClip (Check stacktrace for details)");
					break;

				case AudioManager.LogLevel.Error:
					Debug.LogError ("[AUDIO] Trying to play a sound without an AudioClip (Check stacktrace for details)");
					break;

				case AudioManager.LogLevel.Exception:
					throw new UnityException ("[AUDIO] Trying to play a sound without an AudioClip (Check stacktrace for details)");
				}
			}

			if(EDITOR_runtimeClipPlayed != null)
				EDITOR_runtimeClipPlayed.Invoke (def.Clip);
			Profiler.EndSample ();
			#endif

			PlayingClipInfo info = new PlayingClipInfo(audioSource, def);

			info.MarkPersistent (def.SourceVariator.Persistent);

			if(!info.Invalid)
				_playingClips.Add (info);

			return info;
		}

		/// <summary>
		/// Stops all clips that were played from the specified variator.
		/// </summary>
		/// <param name="variator">Variator.</param>
		public void StopAllClipsFromVariator(BaseClipVariator variator)
		{
			if (variator == null)
				return;

			for (int i = _playingClips.Count - 1; i >= 0; --i)
			{
				if (_playingClips [i].ClipDefinition.SourceVariator == variator)
					_playingClips [i].Stop ();
			}
		}
		#endregion Clip Playing

		#region Snapshots
		/// <summary>
		/// Finds the named snapshot in the mixer.
		/// </summary>
		/// <returns>The snapshot.</returns>
		/// <param name="name">Snapshot name.</param>
		public AudioMixerSnapshot FindSnapshot(string name)
		{
			if(_mixer == null)
			{
				Debug.LogError (string.Format ("[AUDIO] No mixer set on AudioManager"));
				return null;
			}

			return _mixer.FindSnapshot (name);
		}
			
		/// <summary>
		/// Transitions to snapshots using weight.
		/// </summary>
		/// <param name="snapshots">Snapshots.</param>
		/// <param name="weights">Snapshot weights.</param>
		/// <param name="timeToReach">Time to reach snapshots.</param>
		public void TransitionToSnapshots(AudioMixerSnapshot[] snapshots, float[] weights, float timeToReach)
		{
			if(_mixer == null)
			{
				Debug.LogError (string.Format ("[AUDIO] No mixer set on AudioManager"));
				return;
			}

			_mixer.TransitionToSnapshots (snapshots, weights, timeToReach);
		}
		#endregion

		#region Cleanup
		/// <summary>
		/// Stops all the currently playing sources
		/// </summary>
		public void StopAll()
		{
			// Stop all clips
			for(int i = _playingClips.Count - 1; i >= 0; --i)
				_playingClips[i].Stop ();

			_playingClips.Clear ();

			// Clean up any leftover source
			for(int i = _currentSources.Count-1; i >= 0; --i)
			{
				if(_currentSources[i].Key != null)
					FreeSource (_currentSources[i].Key);
			}
			_currentSources.Clear ();
		}

		/// <summary>
		/// Cleanups the playing clips.
		/// </summary>
		private void CleanupPlayingClips()
		{
			for(int i = _playingClips.Count-1; i >= 0; --i)
			{
				// If a clip is stopped
				if(!_playingClips[i].IsPlaying && !_playingClips[i].IsPaused)
				{
					_playingClips[i].Dispose ();
					_playingClips.RemoveAt (i);
				}
				// if a clip is already disposed
				else if(_playingClips[i].Disposed)
					_playingClips.RemoveAt (i);
			}
		}

		/// <summary>
		/// Remove any nulled audio source
		/// </summary>
		private void CleanupAudioSources()
		{
			for(int i = _currentSources.Count-1; i >= 0; --i)
			{
				if(_currentSources[i].Key == null)
					_currentSources.RemoveAt (i);
			}
		}
		#endregion Cleanup

		#region Pooling
		/// <summary>
		/// Frees the audio source associated with the clip
		/// Should only be called by a clip info
		/// </summary>
		/// <param name="info">Info.</param>
		public void DisposeClipInfo(PlayingClipInfo info)
		{
			FreeSource (info.Source);
		}

		/// <summary>
		/// Method used to return an AudioSource to the pool
		/// </summary>
		/// <param name="source">Source.</param>
		private void FreeSource(AudioSource source)
		{
			if(source == null)
				return;

			int index = _currentSources.FindIndex ((s)=>{ return object.ReferenceEquals (s.Key, source); });
			if(index >= 0)
				_currentSources.RemoveAt (index);

			source.enabled = false;
			source.clip = null;

			source.gameObject.hideFlags = HideFlags.HideInHierarchy;

			_audioSourcePool.Free (source);
		}

		/// <summary>
		/// Method used by the pool to create a new AudioSource
		/// </summary>
		/// <returns>The new source.</returns>
		private AudioSource CreateNewSource()
		{
			GameObject newGo = new GameObject(string.Format ("AudioSource"));
			newGo.transform.SetParent (this.transform, false);

			newGo.hideFlags = HideFlags.HideInHierarchy;

			AudioSource source = _clipSourceTemplate.CopyComponentTo<AudioSource>(newGo);
			source.enabled = false;

			return source;
		}
		#endregion Pooling

		#if UNITY_EDITOR
		public delegate void RuntimeClipPlayedDelegate(AudioClip clip);
		public static RuntimeClipPlayedDelegate EDITOR_runtimeClipPlayed;

		public static void EDITOR_RegisterCallback(RuntimeClipPlayedDelegate del)
		{
			EDITOR_runtimeClipPlayed = del;
		}
		#endif
	}
}
