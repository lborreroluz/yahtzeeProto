﻿using UnityEngine;
using System.Collections;
using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio
{
	/// <summary>
	/// Utility class to make a sound play in the environment
	/// </summary>
	public class EnvironmentSound : MonoBehaviour 
	{
		/// <summary>
		/// The variator to play from
		/// </summary>
		[SerializeField] private BaseClipVariator _variator;
		/// <summary>
		/// Should the sound auto start
		/// </summary>
		[SerializeField] private bool _autoStart;

		[Header ("3D Settings")]
		[SerializeField, Range (0f, 360f)] private float _spread;
		/// <summary>
		/// 3D min distance for the sound
		/// </summary>
		[SerializeField] private float _minDistance;
		/// <summary>
		/// 3D max distance for the sound
		/// </summary>
		[SerializeField] private float _maxDistance;
		[SerializeField] private AudioRolloffMode _rolloffMode;

		[SerializeField] private MinMaxFloat _loopDelay;

		private PlayingClipInfo _playingClip;

		private void Start()
		{
			if(_autoStart)
				PlaySound ();
		}

		private void OnDestroy()
		{
			StopSound ();
		}

		public void PlaySound()
		{
			StopAllCoroutines ();

			if(_playingClip != null)
			{
				_playingClip.SetTime (0);
				_playingClip.Play ();
			}
			else
			{
				_playingClip = _variator.PlayClip3D (transform);

				if(_playingClip.Invalid)
					_playingClip = null;
				else
				{
					_playingClip.Set3DSettings (_minDistance, _maxDistance);
					_playingClip.Source.rolloffMode = _rolloffMode;
					_playingClip.Source.spread = _spread;
				}
			}

			float loopTime = _loopDelay.Value;
			if(loopTime > 0 && null != _playingClip && !_playingClip.Invalid)
			{
				_playingClip.SetLooping (false);
				_playingClip.OnDisposed += (LoopSound);
			}
		}

		public void PauseSound()
		{
			if(null != _playingClip)
				_playingClip.Pause ();
		}

		public void ResumeSound()
		{
			if(null != _playingClip)
				_playingClip.Resume ();
			else
				PlaySound ();
		}

		public void StopSound()
		{
			StopAllCoroutines ();
			if(null != _playingClip)
			{
				_playingClip.Stop ();
				_playingClip = null;
			}
		}

		public EnvironmentSound SetVariator(BaseClipVariator newVariator)
		{
			StopSound ();
			_variator = newVariator;
			return this;
		}

		private void OnDrawGizmosSelected()
		{
			Gizmos.DrawWireSphere (transform.position, _minDistance);
			Gizmos.DrawWireSphere (transform.position, _maxDistance);
		}

		private void LoopSound(PlayingClipInfo doneClip)
		{
			StartCoroutine (PlayWithDelay (_loopDelay));
		}

		private IEnumerator PlayWithDelay(float delay)
		{
			yield return new WaitForSeconds (delay);
			PlaySound ();
		}
	}
}
