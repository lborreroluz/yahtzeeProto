﻿using UnityEngine;
using System.Collections;
using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio
{
	/// <summary>
	/// Utility class to make a sound play in the environment
	/// </summary>
	public class AmbiantSound : MonoBehaviour 
	{
		/// <summary>
		/// The variator to play from
		/// </summary>
		[SerializeField] private BaseClipVariator _variator;
		/// <summary>
		/// Should the sound auto start
		/// </summary>
		[SerializeField] private bool _autoStart;
		/// <summary>
		/// Over how much time should the ambiant sound fade in
		/// </summary>
		[SerializeField] private float _fadeInTime;
		/// <summary>
		/// Should the fade in be applied on resume
		/// </summary>
		[SerializeField] private bool _fadeInOnResume;

		private PlayingClipInfo _playingClip;

		private void Start()
		{
			if(_autoStart)
				PlaySound ();
		}

		private void OnDestroy()
		{
			StopSound ();
		}

		public void PlaySound()
		{
			if(null != _playingClip)
				_playingClip.Play ();
			else
				_playingClip = _variator.PlayClip ();

			if(_fadeInTime > 0f)
				FadeIn ();
		}

		public void PauseSound()
		{
			if(null != _playingClip)
				_playingClip.Pause ();
		}

		public void ResumeSound()
		{
			if(null != _playingClip)
			{
				_playingClip.Resume ();
				if(_fadeInOnResume && _fadeInTime > 0f)
					FadeIn ();
			}
		}

		public void StopSound()
		{
			if(null != _playingClip)
			{
				_playingClip.Stop ();
				_playingClip = null;
			}
		}

		private IEnumerator FadeIn()
		{
			float toVol = _playingClip.ClipDefinition.Volume;
			for(float t = 0; t < _fadeInTime; t += Mathf.Min (Time.deltaTime, 1f/ 30f))
			{
				if(_playingClip == null || _playingClip.Source == null)
					yield break;

				_playingClip.Source.volume = Mathf.Lerp (0f, toVol, t / _fadeInTime);
				yield return null;
			}

			_playingClip.Source.volume = toVol;
		}
	}
}
