﻿using UnityEngine;
using System.Collections;
using Hibernum.Unity.Audio;

namespace Hibernum.Unity.Audio
{
	public class AutoPlayBGM : MonoBehaviour 
	{
		/// <summary>
		/// The fade in time.
		/// </summary>
		[SerializeField] private float _fadeTime;
		/// <summary>
		/// The BGM to play
		/// </summary>
		[SerializeField] private BGMClipVariator _bgm;

		private void Start()
		{
			_bgm.PlaySingle (_fadeTime);
		}
	}
}
