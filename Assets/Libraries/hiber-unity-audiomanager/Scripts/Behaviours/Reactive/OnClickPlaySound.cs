﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio
{
	/// <summary>
	/// Plays a sound when the object receives an OnPointerClick event
	/// </summary>
	public class OnClickPlaySound : MonoBehaviour, IPointerClickHandler 
	{
		[FormerlySerializedAsAttribute ("_audioClip")]
		[SerializeField] private BaseClipVariator _clip;
		[SerializeField] private bool _interruptsSelf;
		[SerializeField] private bool _solo;

		private PlayingClipInfo _playing;

		#region IPointerClickHandler implementation
		public void OnPointerClick (PointerEventData eventData)
		{
			if(_interruptsSelf && _playing != null)
			{
				_playing.Stop ();
				_playing = null;
			}

			if(!_solo || _playing == null)
				_playing = _clip.PlayClip ();
		}
		#endregion
	}
}
