﻿using UnityEngine;
using System.Collections;
using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio
{
	/// <summary>
	/// Plays or stops a sound when the default Monobehaviour events are raised
	/// </summary>
	public class OnStateChangePlayClip : MonoBehaviour
	{
		public enum PlayAction
		{
			Nothing,
			PlayClip,
			StopClip
		}

		/// <summary>
		/// The variator to play when an event with the PlayAction.PlayClip is raised
		/// </summary>
		[SerializeField] private BaseClipVariator _clip;
		/// <summary>
		/// Should the playing clip be stopped when a play event is raised?
		/// </summary>
		[SerializeField] private bool _stopClipOnPlay;

		[Header ("Actions")]
		[SerializeField] private PlayAction _onAwake = PlayAction.Nothing;
		[SerializeField] private PlayAction _onStart = PlayAction.Nothing;
		[SerializeField] private PlayAction _onEnable = PlayAction.Nothing;
		[SerializeField] private PlayAction _onDisable = PlayAction.Nothing;
		[SerializeField] private PlayAction _onDestroy = PlayAction.Nothing;

		private PlayingClipInfo _info;

		private void Awake()
		{
			ProcessAction (_onAwake);
		}

		private void Start()
		{
			ProcessAction (_onStart);
		}

		private void OnEnable()
		{
			ProcessAction (_onEnable);
		}

		private void OnDisable()
		{
			ProcessAction (_onDisable);
		}

		private void OnDestroy()
		{
			ProcessAction (_onDestroy);
		}

		/// <summary>
		/// Plays a clip, stops a playing clip, or does nothing depending on the Action passed.
		/// </summary>
		/// <param name="action">Action.</param>
		private void ProcessAction(PlayAction action)
		{
			switch(action)
			{
			case PlayAction.PlayClip:
				if(_stopClipOnPlay && _info != null)
					_info.Stop ();

				_info = _clip.PlayClip ();
				break;

			case PlayAction.StopClip:
				if(_info != null)
				{
					_info.Stop ();
					_info = null;
				}
				break;
			}
		}
	}
}
