﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Hibernum.Core;

namespace Hibernum.Unity.Audio
{
	public class BGMPlaylist : MonoBehaviour
	{
		[SerializeField] private bool _randomize = true;
		[SerializeField] private float _crossfadeTime = 1f;
		[SerializeField] private List<BGMClipVariator> _playlist;

		private List<BGMClipVariator> _currentList = new List<BGMClipVariator>();

		private PlayingBGMInfo _currentBGM = null;

		private void Start()
		{
			StartCoroutine (CrossfadeNext (_crossfadeTime));
		}

		private void FillList()
		{
			var newList = new List<BGMClipVariator>(_playlist);

			if(_randomize)
			{
				// Make sure we don't play the same song twice in a row
				do{
					newList.Shuffle ();
				}while(_currentBGM != null && newList.Count > 1 && newList[0] == _currentBGM.ClipInfo.ClipDefinition.SourceVariator);
			}

			_currentList = newList;
		}

		private IEnumerator CrossfadeNext(float crossFadeTime)
		{
			while(_currentBGM != null && !_currentBGM.Invalid && _currentBGM.ClipInfo.Source.time < _currentBGM.ClipInfo.Clip.length - crossFadeTime){
				yield return null;
			}

			if(_currentList.Count <= 0)
				FillList ();

			_currentBGM = _currentList[0].PlaySingle (crossFadeTime);
			_currentList.RemoveAt (0);

			StartCoroutine (CrossfadeNext (crossFadeTime));
		}
	}
}
