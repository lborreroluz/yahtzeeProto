﻿using UnityEngine;
using UnityEditor;

using System.IO;

using System.Collections;
using System.Collections.Generic;

using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio
{
	/// <summary>
	/// Contains all the scriptable asset creation methods
	/// </summary>
	public static class CreateAudioClipUtility
	{
		
		[MenuItem ("Assets/AudioClip/Create Clip Variator", true)]
		[MenuItem ("Assets/AudioClip/Create BGM Variator", true)]
		/// <summary>
		/// Validation method for menu entries
		/// </summary>
		private static bool ValidateCreateVariator()
		{
			for(int i = 0; i < Selection.objects.Length; ++i)
			{
				Object o = Selection.objects[i];
				if(o is AudioClip)
					return true;
			}

			return false;
		}

		[MenuItem ("Assets/AudioClip/Create BGM Variator")]
		public static void CreateBGMVariator()
		{
			List<AudioClip> clips = new List<AudioClip>();
			for(int i = 0; i < Selection.objects.Length; ++i)
			{
				Object o = Selection.objects[i];
				if(o is AudioClip)
					clips.Add (o as AudioClip);
			}

			CreateVariatorAssets<BGMClipVariator>(clips, "BGM", BGMClipVariator.Create);
		}

		[MenuItem ("Assets/AudioClip/Create Clip Variator")]
		public static void CreateClipVariator()
		{
			List<AudioClip> clips = new List<AudioClip>();
			for(int i = 0; i < Selection.objects.Length; ++i)
			{
				Object o = Selection.objects[i];
				if(o is AudioClip)
					clips.Add (o as AudioClip);
			}

			if(clips.Count <= 1 || EditorUtility.DisplayDialog ("Creation Type", "Do you want to create a single variator or multiple?", "Single Variator", "Multiple Variators"))
				CreateSingleClipVariator (clips);
			else
				CreateVariatorAssets<ClipVariator>(clips, "Clip Variator", ClipVariator.Create);
		}

		private static void CreateSingleClipVariator(List<AudioClip> clips)
		{
			string path = EditorUtility.SaveFilePanelInProject ("Create Variator", "NewVariator", "asset", "");
			if(string.IsNullOrEmpty (path))
				return;

			ClipVariator newVariator = ClipVariator.Create (clips.ToArray ());
			BaseClipVariator oldVariator = AssetDatabase.LoadAssetAtPath<BaseClipVariator> (path);

			// If an asset already existed, we update it
			if(oldVariator != null)
			{
				EditorUtility.CopySerialized (newVariator, oldVariator);
				AssetDatabase.SaveAssets ();
			}
			else
				AssetDatabase.CreateAsset (newVariator, path);

			if(newVariator != null)
			{
				AssetDatabase.Refresh ();
				Selection.activeObject = newVariator;
				EditorGUIUtility.PingObject(Selection.activeObject);
			}
		}

		private delegate T CreateDelegate<T>(AudioClip clip) where T : BaseClipVariator;

		/// <summary>
		/// Handles the creation of one or more asset(s) of the specified type
		/// </summary>
		/// <param name="clips">Clips to create assets.</param>
		/// <param name="variatorType">Variator type name to use for display.</param>
		/// <param name="createDelegate">Creation delegate.</param>
		/// <typeparam name="T">Clip type.</typeparam>
		private static void CreateVariatorAssets<T>(List<AudioClip> clips, string variatorType, CreateDelegate<T> createDelegate) where T : BaseClipVariator
		{
			T firstNew = null;

			if(clips.Count <= 1 || EditorUtility.DisplayDialog (string.Format ("Create {0} variator", variatorType), "How do you want to save the assset(s)?", "Name Individually", "Select Folder"))
			{
				// Individual naming
				for(int i = 0; i < clips.Count; ++i)
				{
					string assetPath = EditorUtility.SaveFilePanelInProject (string.Format ("Create Variator for '{0}' [{1}/{2}]", clips[i].name, i+1, clips.Count), clips[i].name, "asset", "");
					if(string.IsNullOrEmpty (assetPath))
						continue;

					T newVariator = createDelegate (clips[i]);
					BaseClipVariator oldVariator = AssetDatabase.LoadAssetAtPath<BaseClipVariator>(assetPath);

					if(oldVariator != null)
					{
						// An asset already existed, update it
						EditorUtility.CopySerialized (newVariator, oldVariator);
						AssetDatabase.SaveAssets ();
					}
					else{
						AssetDatabase.CreateAsset (newVariator, assetPath);
					}

					if(firstNew == null)
						firstNew = newVariator;
				}
			}
			else
			{
				// Folder selection
				string path = EditorUtility.SaveFolderPanel (string.Format ("Create {0} variator(s) in folder", variatorType), "Assets", "");
				if(string.IsNullOrEmpty (path))
					return;

				path = path.Substring (path.IndexOf ("Assets/"));

				for(int i = 0; i < clips.Count; ++i)
				{
					string assetPath = Path.Combine (path, string.Format ("{0}.asset", clips[i].name));
					T newVariator = createDelegate (clips[i]);
					BaseClipVariator oldVariator = AssetDatabase.LoadAssetAtPath<BaseClipVariator>(assetPath);

					if(oldVariator != null)
					{
						// An asset already existed, update it
						EditorUtility.CopySerialized (newVariator, oldVariator);
						AssetDatabase.SaveAssets ();
					}
					else
						AssetDatabase.CreateAsset (newVariator, assetPath);

					if(firstNew == null)
						firstNew = newVariator;
				}
			}

			// Select the first new asset created
			if(firstNew != null)
			{
				AssetDatabase.Refresh ();
				Selection.activeObject = firstNew;
				EditorGUIUtility.PingObject (firstNew);
			}
		}
	}
}
