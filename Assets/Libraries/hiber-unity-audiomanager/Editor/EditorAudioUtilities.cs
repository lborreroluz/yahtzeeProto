﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using UnityEditor;
using System;

namespace Hibernum.Unity.Audio
{
	/// <summary>
	/// Utility class that creates method wrappers around internal Unity methods and properties
	/// </summary>
	public static class EditorAudioUtilities
	{
		private static MethodInfo _GetOrigSize;
		private static MethodInfo _GetCompSize;
		private static MethodInfo _GetClipPosition;
		private static MethodInfo _GetClipDuration;
		private static MethodInfo _IsClipPlaying;

		private static MethodInfo _PlayClip;
		private static MethodInfo _StopAllClips;

		/// <summary>
		/// Gets the original asset size of the audio asset
		/// </summary>
		public static int GetOrigSize(AudioImporter importer){ return (int)_GetOrigSize.Invoke (importer, null); }
		/// <summary>
		/// Get the compressed asset size of the audio asset
		/// </summary>
		public static int GetCompSize(AudioImporter importer){ return (int)_GetCompSize.Invoke (importer, null); }
		/// <summary>
		/// Gets the current clip position
		/// </summary>
		public static float GetClipPosition(AudioClip clip){ return (float)_GetClipPosition.Invoke (null, new object[]{ clip }); }
		/// <summary>
		/// Gets the total clip duration
		/// </summary>
		public static double GetClipDuration(AudioClip clip){ return (double)_GetClipDuration.Invoke (null, new object[]{ clip }); }
		/// <summary>
		/// Determines if the clip is playing
		/// </summary>
		public static bool IsClipPlaying(AudioClip clip){ return (bool)_IsClipPlaying.Invoke (null, new object[]{ clip }); }

		/// <summary>
		/// Plays a clip in the editor
		/// </summary>
		public static void PlayClip(AudioClip clip){ _PlayClip.Invoke (null, new object[]{ clip }); }
		/// <summary>
		/// Stops all clips currently playing in the editor
		/// </summary>
		public static void StopAllClips(){ _StopAllClips.Invoke (null, null); }

		static EditorAudioUtilities()
		{
			GetMethods ();
		}

		/// <summary>
		/// Gets methods using reflection that are not exposed in regular the API
		/// </summary>
		private static void GetMethods()
		{
			// Compressed Size property
			PropertyInfo property = typeof(AudioImporter).GetProperty(
				"compSize",
				BindingFlags.NonPublic | BindingFlags.Instance
			);
			_GetCompSize = property.GetGetMethod (true);

			// Original Size property
			property = typeof(AudioImporter).GetProperty(
				"origSize",
				BindingFlags.NonPublic | BindingFlags.Instance
			);
			_GetOrigSize = property.GetGetMethod (true);

			Type audioUtil = typeof(AudioImporter).Assembly.GetType ("UnityEditor.AudioUtil");
			_PlayClip = audioUtil.GetMethod(
				"PlayClip",
				BindingFlags.Static | BindingFlags.Public,
				null,
				new System.Type[] {
					typeof(AudioClip)
				},
				null
			);

			_StopAllClips = audioUtil.GetMethod(
				"StopAllClips",
				BindingFlags.Static | BindingFlags.Public,
				null,
				new System.Type[] {},
				null
			);

			_GetClipPosition = audioUtil.GetMethod (
				"GetClipPosition",
				BindingFlags.Static | BindingFlags.Public,
				null,
				new System.Type[] {typeof(AudioClip)},
				null
			);

			_IsClipPlaying = audioUtil.GetMethod (
				"IsClipPlaying",
				BindingFlags.Static | BindingFlags.Public,
				null,
				new System.Type[] {typeof(AudioClip)},
				null
			);

			_GetClipDuration = audioUtil.GetMethod (
				"GetDuration",
				BindingFlags.Static | BindingFlags.Public,
				null,
				new System.Type[] {typeof(AudioClip)},
				null
			);
		}
	}
}
