﻿using UnityEngine;
using UnityEditor;

using System.Collections;
using Hibernum.Unity.Audio;
using System.Collections.Generic;
using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio
{
	public class AudioVisualizerWindow : EditorWindow
	{
		protected const float _kScrollBarWidth = 15f;

		private Texture2D _progressTex;

		private static GUIStyle _labelStyle;
		private static GUIStyle _boxStyle;
		private static GUIStyle _boxStyleStretchable;
		private static GUIStyle _stackTraceStyle;
		private static GUIStyle _foldoutStyle;
		private static GUIStyle _progressStyle;

		private static Color _progressColor;

		private Vector2 _scrollPos = new Vector2();

		private Dictionary<object, bool> _expandedState = new Dictionary<object, bool>();
		private Dictionary<object, bool> _expandedStateCache = new Dictionary<object, bool>();

		private bool _scrollBarPresent = false;

		/// <summary>
		/// A list to reuse when getting the clips each frame (prevents list allocation)
		/// </summary>
		private List<PlayingClipInfo> _clipListCache = new List<PlayingClipInfo>();

		[MenuItem("Window/Audio Visualizer")]
		private static void OpenWindow()
		{
			EditorWindow.GetWindow <AudioVisualizerWindow>(false, "Audio Visualizer", true);
		}

		/// <summary>
		/// Utility method to grab any missing style reference
		/// </summary>
		private void GetStyles()
		{
			if(_progressTex == null)
				_progressTex = new Texture2D(2,2);

			_labelStyle = new GUIStyle("Label");

			_boxStyle = new GUIStyle("Box");
			_boxStyle.normal.textColor = _labelStyle.normal.textColor; 

			_boxStyleStretchable = new GUIStyle(_boxStyle);
			_boxStyleStretchable.stretchWidth = true;

			_stackTraceStyle = new GUIStyle(_boxStyleStretchable);
			_stackTraceStyle.alignment = TextAnchor.MiddleLeft;

			_progressStyle = new GUIStyle(_boxStyle);
			_progressStyle.normal.background = _progressTex;
			_progressColor = new Color32(0, 149, 255, 255);

			_foldoutStyle = new GUIStyle("Foldout");
			_foldoutStyle.stretchWidth = false;
			_foldoutStyle.alignment = TextAnchor.MiddleCenter;
		}

		private void OnGUI()
		{
			if(_progressTex == null)
				GetStyles ();

			DrawHeader();

			if(EditorApplication.isPlaying)
			{
				// Get the currently playing clips
				AudioManager.Instance.GetPlayingClips (_clipListCache);

				using(var scrollView = new EditorGUILayout.ScrollViewScope(_scrollPos))
				{
					// We need to create a new dictionary here, because _expandedState references 
					// the same object at this point and we don't want to clear it out.
					_expandedStateCache = new Dictionary<object, bool>();

					// Draw the playing clips
					for(int i = 0; i < _clipListCache.Count; ++i)
					{
						bool isExpanded = _expandedState.ContainsKey (_clipListCache[i]) && _expandedState[_clipListCache[i]];
						DrawPlayingClip (_clipListCache[i], isExpanded);
					}

					_expandedState = _expandedStateCache;
					_scrollPos = scrollView.scrollPosition;

					// This let's us get the width of the scroll view and determine if we see the scroll bar (because Unity doesn't give us a way to do it)
					GUILayout.Box (GUIContent.none, GUIStyle.none, GUILayout.ExpandWidth (true), GUILayout.Height (0));
					if(Event.current.type == EventType.repaint)
					{
						Rect last = GUILayoutUtility.GetLastRect ();
						_scrollBarPresent = last.width < position.width - (_kScrollBarWidth - 2);
					}
				}
			}
			else 
			{
				// Draw a little something in the window when not playing
				using(new EditorGUILayout.VerticalScope ())
				{
					GUILayout.FlexibleSpace ();
					Color bg = GUI.backgroundColor;
					GUI.backgroundColor = Color.clear;
					GUILayout.Label ("You need to be in play mode to inspect playing clips", _boxStyleStretchable);
					GUI.backgroundColor = bg;
					GUILayout.FlexibleSpace ();
				}
			}
		}

		private void OnDisable()
		{
			// So we don't leak the progress texture
			DestroyImmediate (_progressTex);
		}

		private void Update()
		{
			// Repaint the window while playing so the progress bars are displayed smoothly
			if(EditorApplication.isPlaying)
				Repaint ();
		}

		/// <summary>
		/// Draws the headers for all data columns.
		/// </summary>
		private void DrawHeader()
		{
			using(new EditorGUILayout.HorizontalScope())
			{
				GUILayoutUtility.GetRect (GUIContent.none, _boxStyle, GUILayout.Width (20f));
				GUILayout.Label ("Volume", _boxStyle, GUILayout.Width (50f));
				GUILayout.Label ("Pitch", _boxStyle, GUILayout.Width (50f));
				GUILayout.Label ("Loop", _boxStyle, GUILayout.Width (50f));
				GUILayout.Label ("Clip Name", _boxStyleStretchable);
				GUILayout.Label ("Variator", _boxStyle, GUILayout.Width (200f));
				GUILayout.Label ("Mixer Group", _boxStyle, GUILayout.Width (200f));
				if(_scrollBarPresent)
					GUILayout.Space (_kScrollBarWidth);
			}
		}

		/// <summary>
		/// Draws the playing clip info.
		/// </summary>
		/// <param name="info">Info to disaply.</param>
		/// <param name="expanded">If set to <c>true</c>, draw the stacktrace as well.</param>
		private void DrawPlayingClip(PlayingClipInfo info, bool expanded)
		{
			using(new EditorGUILayout.HorizontalScope())
			{
				using(var mainColorScope = new BackgroundColorScope())
				{
					Rect foldout = GUILayoutUtility.GetRect (GUIContent.none, _boxStyle, GUILayout.Width (20f));
					// Center the foldout arrow
					foldout.x += 5f;
					foldout.y += 3f;

					expanded = EditorGUI.Foldout (foldout, expanded, GUIContent.none, _foldoutStyle);

					// Volume
					GUILayout.Label (info.Volume.ToString ("0.00"), _boxStyle, GUILayout.Width (50f));
					// Pitch
					GUILayout.Label (info.Pitch.ToString ("0.00"), _boxStyle, GUILayout.Width (50f));

					// Loop
					GUI.backgroundColor = info.Source.loop ? Color.green : Color.red;
					GUILayout.Label (info.Source.loop.ToString (), _boxStyle, GUILayout.Width (50f));
					mainColorScope.Reset ();

					// Name & clip progress
					Rect nameRect = GUILayoutUtility.GetRect (GUIContent.none, _boxStyleStretchable);
					GUI.Box (nameRect, GUIContent.none);
					Rect progressRect = new Rect(nameRect);
					progressRect.width *= info.NormalizedTime;
					GUI.backgroundColor = _progressColor;
					GUI.Box (progressRect, GUIContent.none, _progressStyle);
					GUI.backgroundColor = Color.clear;
					if(GUI.Button (nameRect, info.Clip.name, _boxStyleStretchable))
						Selection.activeObject = info.Clip;

					mainColorScope.Reset ();

					// Variator asset
					if(GUILayout.Button (info.ClipDefinition.SourceVariator.name, _boxStyle, GUILayout.Width (200f)))
						Selection.activeObject = info.ClipDefinition.SourceVariator;

					// Mixer
					if(info.Source.outputAudioMixerGroup == null)
						GUILayout.Label ("No Mixer", _boxStyle, GUILayout.Width (200f));
					else if(GUILayout.Button (info.Source.outputAudioMixerGroup.name, _boxStyle, GUILayout.Width (200f)))
						Selection.activeObject = info.Source.outputAudioMixerGroup;
				}
			}

			// Draw the stacktrace with per-stackframe code acces
			if(expanded)
			{
				using(new EditorGUILayout.HorizontalScope())
				{
					GUILayout.Space (60f);
					using(new EditorGUILayout.VerticalScope ())
					{
						for(int i = 0; i < info.EDITOR_StackTrace.Length; ++i)
						{
							var frame = info.EDITOR_StackTrace[i];
							string text = string.Format ("{0} [{1}:{2}]", frame.GetMethod ().ToString(), System.IO.Path.GetFileName (frame.GetFileName ()), frame.GetFileLineNumber ());;

							if(GUILayout.Button (text, _stackTraceStyle))
								OpenScript (frame);
						}
					}
				}
			}

			_expandedStateCache.Add (info, expanded);
		}

		/// <summary>
		/// Opens the script asset at the line specified in the stackframe
		/// </summary>
		/// <param name="frame">Frame.</param>
		private void OpenScript(System.Diagnostics.StackFrame frame)
		{
			try
			{
				string path = frame.GetFileName ();
				// Make the path relative to the project
				path = path.Substring (path.IndexOf ("Assets/"));

				MonoScript targetObject = AssetDatabase.LoadAssetAtPath<MonoScript> (path);
				AssetDatabase.OpenAsset (targetObject, frame.GetFileLineNumber ());
			}
			catch{
				Debug.LogError (string.Format ("Could not find the target script file."));
			}
		}
	}
}
