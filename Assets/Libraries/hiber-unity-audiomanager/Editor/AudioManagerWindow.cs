﻿using UnityEngine;
using UnityEditor;

using System.Collections;
using System.Collections.Generic;
using System.IO;

using Hibernum.Unity.Audio.Drawers;
using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio
{
	public class AudioManagerWindow : EditorWindow
	{
		/// <summary>
		/// The tabs that van be drawn in the window
		/// </summary>
		private enum WindowTabs
		{
			AudioClips,
			Variators,
		}

		/// <summary>
		/// Valid audio file formats to look for in the project
		/// </summary>
		private readonly string[] kFileFormats = new string[]{
			".mp3",
			".ogg",
			".wav",
			".aiff",
			".aif",
			".mod",
			".it",
			".s3m",
			".xm",
		};

		private const string _kProjectSettingsFile = "Assets/AudioProjectSettings.asset";

		public static void OnAudioAssetImported(){ if(_instance != null){ _instance.OnProjectChange (); } }
		private static AudioManagerWindow _instance;

		private WindowTabs _currentTab = WindowTabs.Variators;

		/// <summary>
		/// All audio clips in the project
		/// </summary>
		private List<AudioClipData> _allClips;
		/// <summary>
		/// All variators in the project
		/// </summary>
		private List<BaseVariatorData> _allVariators;

		public GUIStyle BoxStyle { get; private set; }
		public GUIStyle LabelStyle { get; private set; }

		private ProjectSettings _projectSettings;

		private AudioClipWindow _clipWindow;
		private VariatorWindow _variatorWindow;

		/// <summary>
		/// Returns the project-specific settings i.e.: Tags
		/// </summary>
		/// <value>The project settings.</value>
		public static ProjectSettings projectSettings { get { return _instance._projectSettings; } }

		[MenuItem("Window/Audio Manager _%#a")]
		private static void OpenWindow()
		{
			AudioManagerWindow window = EditorWindow.GetWindow <AudioManagerWindow>(false, "Audio Manager", true);
			window.OnEnable ();
		}

		public AudioManagerWindow()
		{
			this.position = new Rect(0, 0, Screen.currentResolution.width/2f, Screen.currentResolution.height/2f);
		}

		private void OnEnable()
		{
			LoadProjectSettings ();

			_clipWindow = new AudioClipWindow(this);
			_variatorWindow = new VariatorWindow(this);

			_allClips = new List<AudioClipData>();
			_allVariators = new List<BaseVariatorData>();

			_instance = this;
			_currentTab = WindowTabs.AudioClips;

			CollectVariators ();
			CollectClips ();

			SetSelected ();

			OnPlayModeChanged ();

			// In case the window was already open, we remove the listener
			EditorApplication.playmodeStateChanged -= OnPlayModeChanged;
			EditorApplication.playmodeStateChanged += OnPlayModeChanged;

			_clipWindow.OnEnable ();
			_variatorWindow.OnEnable ();
		}

		private void LoadProjectSettings()
		{
			// Find or create the project-specific asset
			_projectSettings =  AssetDatabase.LoadAssetAtPath<ProjectSettings>(_kProjectSettingsFile);
			if(_projectSettings == null)
			{
				_projectSettings = ScriptableObject.CreateInstance<ProjectSettings>();
				AssetDatabase.CreateAsset (_projectSettings, _kProjectSettingsFile);
				AssetDatabase.SaveAssets ();
			}

			_projectSettings.CleanupAssetTags ();
		}

		/// <summary>
		/// Raised when the editor changes play modes.
		/// </summary>
		private void OnPlayModeChanged()
		{
			if(EditorApplication.isPlaying)
				AudioManager.EDITOR_RegisterCallback (OnRuntimePlayClip);
			else
				AudioManager.EDITOR_RegisterCallback (null);

			_clipWindow.OnPlayModeChanged ();
		}

		private void OnDisable()
		{
			_projectSettings = null;

			// Unregister from editor callback
			EditorApplication.playmodeStateChanged -= OnPlayModeChanged;

			for(int i = 0; i < _allClips.Count; ++i)
				_allClips[i].Dispose ();
			
			_allClips.Clear ();

			_allClips = null;
			_instance = null;

			// Register null callback to runtime event
			AudioManager.EDITOR_RegisterCallback (null);

			_clipWindow.OnDisable ();
			_variatorWindow.OnDisable ();
		}

		/// <summary>
		/// Find all audio clip files in the project
		/// </summary>
		public void CollectClips()
		{
			List<AudioClip> clips = GetAssetsOfType<AudioClip>(kFileFormats);
			for(int i = 0; i < _allClips.Count; ++i)
				_allClips[i].Dispose ();

			_allClips.Clear ();

			for(int i = 0; i < clips.Count; ++i)
			{
				// Collect the variators that use this asset
				List<BaseClipVariator> variators = new List<BaseClipVariator>();
				for(int v = 0; v < _allVariators.Count; ++v)
				{
					if(_allVariators[v].Asset.HasClip (clips[i]))
						variators.Add (_allVariators[v].Asset);
				}

				AudioClipData newData = new AudioClipData(clips[i], variators, _clipWindow);
				_allClips.Add (newData);
			}

			_clipWindow.OnClipsCollcted();
		}

		public List<AudioClipData> GetAllClips()
		{
			return _allClips;
		}

		/// <summary>
		/// Find all clip variator files in the project
		/// </summary>
		private void CollectVariators()
		{
			List<BaseClipVariator> variators = GetAssetsOfType<BaseClipVariator>(".asset");
			for(int i = 0; i < _allVariators.Count; ++i)
				_allVariators[i].Dispose ();

			_allVariators.Clear ();

			for(int i = 0; i < variators.Count; ++i)
				_allVariators.Add (new BaseVariatorData(variators[i], _variatorWindow));

			_variatorWindow.OnVariatorsCollected ();
		}

		public List<BaseVariatorData> GetAllVariators()
		{
			return _allVariators;
		}

		/// <summary>
		/// Called when a clip plays during runtime
		/// increments the play count
		/// </summary>
		private void OnRuntimePlayClip(AudioClip clip)
		{
			if(clip == null)
				return;

			AudioClipData data = _allClips.Find((c)=>{
				return c.Asset == clip;
			});

			// Increment the total number of clips played if this clip hadn't played before
			if(data.RuntimePlayCount == 0)
				++_clipWindow.TotalSoundsPlayed;

			++data.RuntimePlayCount;

			// Call a repaint to update the UI
			Repaint ();

			// Should playing this clip trigger an application break?
			if(data.BreakOnPlay)
			{
				Debug.LogWarning (string.Format ("Triggered Breakpoint on audio clip '{0}'", data.Asset.name));
				Debug.Break ();
			}
		}

		#region Drawing
		/// <summary>
		/// Raised by the Editor when the selection changes
		/// </summary>
		private void OnSelectionChange()
		{
			SetSelected ();
			Repaint ();
		}

		/// <summary>
		/// Update the selction state of the clip datas and variator datas
		/// </summary>
		private void SetSelected()
		{
			// Update the selected clips.
			for(int i = 0; i < _allClips.Count; ++i)
			{
				_allClips[i].Selected = false;
				for(int s = 0; s < Selection.objects.Length; ++s)
				{
					if(object.ReferenceEquals (Selection.objects[s], _allClips[i].Asset))
					{
						_allClips[i].Selected = true;
						break;
					}
				}
			}

			// Update the selected variators
			for(int i = 0; i < _allVariators.Count; ++i)
			{
				_allVariators[i].Selected = false;
				for(int s = 0; s < Selection.objects.Length; ++s)
				{
					if(object.ReferenceEquals (Selection.objects[s], _allVariators[i].Asset))
					{
						_allVariators[i].Selected = true;
						break;
					}
				}
			}
		}

		/// <summary>
		/// Raised by the Editor when the editor updates it's view
		/// </summary>
		private void OnInspectorUpdate()
		{
			_clipWindow.OnInspectorUpdate ();
		}

		/// <summary>
		/// Raised by the Editor when a file is added/deleted/moved
		/// </summary>
		private void OnProjectChange()
		{
			RefreshInterface ();
		}

		/// <summary>
		/// Re-collects the clips & variators and redraws all
		/// </summary>
		public void RefreshInterface()
		{
			WindowTabs lastTab = _currentTab;

			_clipWindow.WillRefreshInterface ();

			OnDisable ();
			OnEnable ();

			_clipWindow.OnRefreshedInterface ();

			_currentTab = lastTab;
			Repaint ();
		}

		private void OnGUI()
		{
			// Chec for cmd+a/ctrl+a
			bool selectAll = Event.current.type == EventType.ValidateCommand && Event.current.commandName == "SelectAll";
			if(selectAll)
				Event.current.Use ();

			GetStyles ();
			// Window tab selection
			using(var scope = new EditorGUILayout.HorizontalScope (BoxStyle))
			{
				GUILayout.Label ("Window Mode: ");
				if(GUILayout.Button ("Audio Clips", GUILayout.Width (100f)))
					_currentTab = WindowTabs.AudioClips;
				
				if(GUILayout.Button ("Variators", GUILayout.Width (100f)))
					_currentTab = WindowTabs.Variators;
				
				GUILayout.FlexibleSpace ();
				if(GUILayout.Button ("Refresh", GUILayout.Width (100f)))
					CollectClips ();
			}

			// Draw currently selected window
			switch(_currentTab)
			{
			case WindowTabs.AudioClips:
				if(selectAll)
					_clipWindow.SelectAll ();
				
				_clipWindow.DrawWindow (position);
				break;

			case WindowTabs.Variators:
				if(selectAll)
					_variatorWindow.SelectAll ();

				_variatorWindow.DrawWindow (position);
				break;
			}
		}

		/// <summary>
		/// Utility method to grab any missing style reference
		/// </summary>
		private void GetStyles()
		{
			if(LabelStyle == null || LabelStyle.name != "Label")
				LabelStyle = new GUIStyle("Label");

			if(null == BoxStyle || BoxStyle.name != "Box")
			{
				BoxStyle = new GUIStyle("Box");
				BoxStyle.normal.textColor = LabelStyle.normal.textColor; 
				BoxStyle.stretchWidth = true;
			}
		}
		#endregion Drawing

		/// <summary>
		/// Utility to wrap Editor methods (Asset -> Path -> GUID)
		/// </summary>
		/// <returns>The asset GUI.</returns>
		/// <param name="asset">Asset.</param>
		public static string GetAssetGUID(UnityEngine.Object asset)
		{
			if(asset == null)
				return null;
			
			return AssetDatabase.AssetPathToGUID (AssetDatabase.GetAssetPath (asset));
		}

		/// <summary>
		/// Gets the assets corresponding to the type and passed extensions from the project
		/// </summary>
		/// <returns>The matching assets.</returns>
		/// <param name="extensions">Extensions.</param>
		/// <typeparam name="T">The type of assets to get.</typeparam>
		public static List<T> GetAssetsOfType<T>(params string[] extensions) where T : UnityEngine.Object
		{
			List<T> all = new List<T>();
			for(int i = 0; i < extensions.Length; ++i)
				all.AddRange (GetAssetsOfType<T>(extensions[i]));

			return all;
		}

		/// <summary>
		/// Gets the assets corresponding to the type and passed extension from the project
		/// </summary>
		/// <returns>The matching assets.</returns>
		/// <param name="extension">Extension.</param>
		/// <typeparam name="T">The type of assets to get.</typeparam>
		public static List<T> GetAssetsOfType<T>(string extension) where T : UnityEngine.Object
		{
			List<T> tempObjects = new List<T>();
			DirectoryInfo directory = new DirectoryInfo(Application.dataPath);
			FileInfo[] goFileInfo = directory.GetFiles("*" + extension, SearchOption.AllDirectories);

			FileInfo tempGoFileInfo; 
			string tempFilePath;

			T temp;

			for (int i = 0, iMax = goFileInfo.Length; i < iMax; i++)
			{
				if ((tempGoFileInfo = goFileInfo[i]) == null)
					continue;

				tempFilePath = tempGoFileInfo.FullName;
				tempFilePath = tempFilePath.Replace(@"\", "/").Replace(Application.dataPath, "Assets");

				temp = AssetDatabase.LoadAssetAtPath<T>(tempFilePath);
				if (temp == null)
					continue;

				tempObjects.Add(temp);
			}

			return tempObjects;
		}
	}
}
