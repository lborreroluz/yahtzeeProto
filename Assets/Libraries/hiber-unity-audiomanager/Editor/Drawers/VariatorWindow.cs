﻿using UnityEngine;
using UnityEditor;

using System.IO;
using System.Collections;
using System.Collections.Generic;

using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio.Drawers
{
	public class VariatorWindow : BaseWindow<VariatorWindow, BaseClipVariator, BaseVariatorData>
	{
		/// <summary>
		/// Sort mode for the audio files
		/// </summary>
		private enum SortMode
		{
			VariatorType,
			Name,
			Mixer,
		}

		private List<System.Type> _kVariatorTypes = new List<System.Type>(){
			typeof(ClipVariator),
			typeof(BGMClipVariator),
		};

		private SortMode _currentSortMode = SortMode.Name;
		private bool _sortDescending;

		private List<string> _mixers = new List<string>();

		private int[] _typeCounts;
		private List<int> _mixerCounts = new List<int>();

		private string _stringFilter = "";
		private int _typeFilter = -1;
		private int _mixerFilter = -1;

		public VariatorWindow(AudioManagerWindow parent) : base (parent){  }

		public override void WillRefreshInterface (){  }

		public override void OnRefreshedInterface (){  }

		public void OnVariatorsCollected()
		{
			// TODO Collect stats [PHIL]
			_mixers.Clear ();
			_mixerCounts.Clear ();

			_typeCounts = new int[_kVariatorTypes.Count];

			List<BaseVariatorData> allAssets = GetAllAssets();
			for(int i = 0; i < allAssets.Count; ++i)
			{
				string mixer = allAssets[i].Asset.DefaultMixerGroup == null ? "No Mixer" : allAssets[i].Asset.DefaultMixerGroup.name;
				int index = _mixers.IndexOf (mixer);
				if(index == -1)
				{
					index = _mixers.Count;
					_mixers.Add (mixer);
					_mixerCounts.Add (0);
				}
				++_mixerCounts[index];

				index = _kVariatorTypes.IndexOf (allAssets[i].Asset.GetType ());
				if(index != -1)
					++_typeCounts[index];
			}

			List<string> oldMixers = new List<string>(_mixers);
			List<int> oldCounts = new List<int>(_mixerCounts);

			_mixers.Sort ();
			for(int i = 0; i < oldMixers.Count; ++i)
				_mixerCounts[_mixers.IndexOf (oldMixers[i])] = oldCounts[i];

			SortVariators ();
		}

		protected override List<BaseVariatorData> GetAllAssets ()
		{
			return parentWindow.GetAllVariators ();
		}

		protected override List<BaseVariatorData> GetFilteredAssets ()
		{
			List<BaseVariatorData> filtered = new List<BaseVariatorData>(GetAllAssets ());
			string[] searchTerms = _stringFilter.Split (' ');

			for(int i = filtered.Count - 1; i >= 0; --i)
			{
				var current = filtered[i];

				bool removed = false;
				for(int s = 0; s < searchTerms.Length; ++s)
				{
					if(string.IsNullOrEmpty (searchTerms[s]))
						continue;

					if(!current.Asset.name.Contains (searchTerms[s]))
					{
						filtered.RemoveAt (i);
						removed = true;
						break;
					}
				}

				if(removed)
					continue;

				int typeIndex = _kVariatorTypes.IndexOf (current.Asset.GetType ());
				if((_typeFilter & (1 << typeIndex)) == 0)
					filtered.RemoveAt (i);
				
				string mixer = current.Asset.DefaultMixerGroup == null ? "No Mixer" : current.Asset.DefaultMixerGroup.name;
				int mixerIndex = _mixers.IndexOf (mixer);
				if((_mixerFilter & (1 << mixerIndex)) == 0)
					filtered.RemoveAt (i);
			}

			return filtered;
		}

		/// <summary>
		/// Changes the sort mode and re-orders the clips
		/// </summary>
		/// <param name="mode">Mode.</param>
		private void SetSortMode(SortMode mode)
		{
			if(mode == _currentSortMode)
				_sortDescending = !_sortDescending;
			else{
				_currentSortMode = mode;
				_sortDescending = false;
			}

			SortVariators ();
		}

		/// <summary>
		/// Sorts the clip susing the current sort mode
		/// </summary>
		private void SortVariators()
		{
			List<BaseVariatorData> allVariators = GetAllAssets ();

			switch(_currentSortMode)
			{
			case SortMode.VariatorType:
				allVariators.Sort ((l, r)=>{
					return (_sortDescending ? -1 : 1) * string.Compare (l.Asset.GetType ().ToString (), r.Asset.GetType ().ToString ());
				});
				break;

			case SortMode.Name:
				allVariators.Sort ((l, r)=>{
					return (_sortDescending ? -1 : 1) * string.Compare (l.Asset.name, r.Asset.name);
				});
				break;

			case SortMode.Mixer:
				string left = null;
				string right = null;
				allVariators.Sort ((l, r)=>{
					left = l.Asset.DefaultMixerGroup != null ? l.Asset.DefaultMixerGroup.name : "(NULL)";
					right = r.Asset.DefaultMixerGroup != null ? r.Asset.DefaultMixerGroup.name : "(NULL)";
					return (_sortDescending ? -1 : 1) * string.Compare (left, right);
				});
				break;
			}
		}

		public override void DrawWindow (Rect r)
		{
			List<BaseVariatorData> allAssets = GetAllAssets();
			List<BaseVariatorData> filtered = GetFilteredAssets();

			Rect searchRect = GUILayoutUtility.GetRect (parentWindow.position.width, 20);
			DrawSearchBar (ref _stringFilter, searchRect, 400);

			DrawHeader (allAssets, filtered);

			GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (1f));

			using(var scroll = new EditorGUILayout.ScrollViewScope(scrollPos, false, true))
			{
				if(scroll.scrollPosition != scrollPos)
					nextScrollPos = scroll.scrollPosition;

				Vector2 scrollView = new Vector2(scrollPos.y - _kPadding, scrollPos.y + r.height + _kPadding);
			
				for(int i = 0; i < filtered.Count; ++i)
				{
					if(_kElementSize * i < scrollView.x || _kElementSize * (i+1) > scrollView.y)
						GUILayout.Space (_kElementSize);
					else
						filtered[i].Draw ((BaseVariatorData.Rows)0, parentWindow.BoxStyle);
				}

				if(Event.current.type != EventType.layout)
					scrollPos = nextScrollPos;
			}
		}

		private void DrawHeader(List<BaseVariatorData> allVariators, List<BaseVariatorData> filteredVariators)
		{
			const float height = 20f;

			using(new EditorGUILayout.VerticalScope())
			{
				using(new EditorGUILayout.HorizontalScope())
				{
					if(GUILayout.Button ("Variator Type", parentWindow.BoxStyle, GUILayout.Width (BaseVariatorData._kWidths[0]), GUILayout.Height (height)))
					{
						if(Event.current.button == 0)
							SetSortMode (SortMode.VariatorType);
						else if(Event.current.button == 1)
							ShowTypeFilter ();
					}

					if(GUILayout.Button ("Variator Name", parentWindow.BoxStyle, GUILayout.Height (height), GUILayout.ExpandWidth (true)))
						SetSortMode (SortMode.Name);

					if(GUILayout.Button ("Mixer Group", parentWindow.BoxStyle, GUILayout.Width (BaseVariatorData._kWidths[2]), GUILayout.Height (height)))
					{
						if(Event.current.button == 0)
							SetSortMode (SortMode.Mixer);
						else if(Event.current.button == 1)
							ShowMixerFilter ();
					}

					GUILayout.Space (_kScrollBarWidth);
				}

				using(new EditorGUILayout.HorizontalScope())
				{
					GUILayoutUtility.GetRect (GUIContent.none, parentWindow.BoxStyle, GUILayout.Width (BaseVariatorData._kWidths[0]));

					if(allVariators.Count != filteredVariators.Count)
						GUILayout.Label (string.Format ("Variator Files: {0} | Total Variators: {1}", filteredVariators.Count, allVariators.Count), parentWindow.BoxStyle, GUILayout.Height (height), GUILayout.ExpandWidth (true));
					else
						GUILayout.Label (string.Format ("Total Variator Files: {0}", allVariators.Count), parentWindow.BoxStyle, GUILayout.Height (height), GUILayout.ExpandWidth (true));

					GUILayoutUtility.GetRect (GUIContent.none, parentWindow.BoxStyle, GUILayout.Width (BaseVariatorData._kWidths[2]));
					
					GUILayout.Space (_kScrollBarWidth);
				}
			}
		}

		private void ShowTypeFilter()
		{
			GenericMenu menu = new GenericMenu();

			menu.AddItem (new GUIContent ("Show All"), false, ()=>{
				_typeFilter = -1;
			});

			menu.AddItem (new GUIContent ("Hide All"), false, ()=>{
				_typeFilter = 0;
			});

			menu.AddSeparator ("");

			for(int i = 0; i < _kVariatorTypes.Count; ++i)
			{
				bool isOn = ((1 << i) & _typeFilter) != 0;
				int index = i;
				menu.AddItem (new GUIContent (string.Format ("{0} [{1}]", _kVariatorTypes[index].Name, _typeCounts[index])), isOn, ()=>{
					if(isOn)
						_typeFilter &= ~(1 << index);
					else
						_typeFilter |= (1 << index);
				});
			}

			menu.ShowAsContext ();
		}

		private void ShowMixerFilter()
		{
			GenericMenu menu = new GenericMenu();

			menu.AddItem (new GUIContent ("Show All"), false, ()=>{
				_mixerFilter = -1;
			});

			menu.AddItem (new GUIContent ("Hide All"), false, ()=>{
				_mixerFilter = 0;
			});

			menu.AddSeparator ("");

			for(int i = 0; i < _mixers.Count; ++i)
			{
				bool isOn = ((1 << i) & _mixerFilter) != 0;
				int index = i;
				menu.AddItem (new GUIContent (string.Format ("{0} [{1}]", _mixers[index], _mixerCounts[index])), isOn, ()=>{
					if(isOn)
						_mixerFilter &= ~(1 << index);
					else
						_mixerFilter |= (1 << index);
				});
			}

			menu.ShowAsContext ();
		}
	}
}
