﻿using UnityEngine;
using UnityEditor;

using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio.Drawers
{
	public class AudioClipWindow : BaseWindow<AudioClipWindow, AudioClip, AudioClipData>
	{
		/// <summary>
		/// Sort mode for the audio files
		/// </summary>
		private enum SortMode
		{
			Name,
			Time,
			Quality,
			OrigSize,
			CompRate,
			CompSize,
		}

		private float _totalClipTime;
		private float _totalClipOrigSize;
		private float _totalClipCompSize;

		private string _totalClipTimeFormatted;
		private string _totalClipOrigSizeFormatted;
		private string _totalClipCompSizeFormatted;

		private int[] _totalTagCounts;

		private SortMode _currentSortMode = SortMode.Name;
		private bool _sortDescending = false;

		private SortMode lastSort = SortMode.Name;
		private bool lastDescending = false;

		public int TotalSoundsPlayed { get; set; }

		/// <summary>
		/// The currently playing clip
		/// </summary>
		private AudioClipData _playingClip;

		private string _stringFilter = "";
		private int _tagFilter = -1;
		private bool _showUntagged = true;

		public AudioClipWindow(AudioManagerWindow parent) : base(parent){  }

		/// <summary>
		/// Changes the sort mode and re-orders the clips
		/// </summary>
		/// <param name="mode">Mode.</param>
		private void SetSortMode(SortMode mode)
		{
			if(mode == _currentSortMode)
				_sortDescending = !_sortDescending;
			else
			{
				_currentSortMode = mode;
				_sortDescending = false;
			}

			SortClips ();
		}

		/// <summary>
		/// Sorts the clip susing the current sort mode
		/// </summary>
		private void SortClips()
		{
			List<AudioClipData> allClips = GetAllAssets ();

			System.Comparison<AudioClipData> sortDelegate = null;
			int sortOder = _sortDescending ? -1 : 1;

			switch(_currentSortMode)
			{
			case SortMode.Name:
				sortDelegate = (l, r)=>{
					return sortOder * string.Compare (l.Asset.name, r.Asset.name);
				};
				break;

			case SortMode.OrigSize:
				sortDelegate = (l, r)=>{
					return sortOder * (l.OrigSize - r.OrigSize);
				};
				break;

			case SortMode.CompSize:
				sortDelegate = (l, r)=>{
					return sortOder * (l.CompSize - r.CompSize);
				};
				break;

			case SortMode.CompRate:
				sortDelegate = (l, r)=>{
					return sortOder * Mathf.RoundToInt (Mathf.Sign ((l.CompRate - r.CompRate)));
				};
				break;

			case SortMode.Quality:
				sortDelegate = (l, r)=>{
					return sortOder * Mathf.RoundToInt (Mathf.Sign (l.Quality - r.Quality));
				};
				break;

			case SortMode.Time:
				sortDelegate = (l, r)=>{
					return sortOder * Mathf.RoundToInt (Mathf.Sign (l.ClipTime - r.ClipTime));
				};
				break;
			}

			if(sortDelegate != null)
				allClips.Sort (sortDelegate);
		}

		/// <summary>
		/// Raised when the clips are collected. Gathers metadata on the clips
		/// </summary>
		public void OnClipsCollcted()
		{
			List<AudioClipData> allClips = GetAllAssets ();

			_totalClipTime = 0f;
			_totalClipOrigSize = 0f;
			_totalClipCompSize = 0f;

			// How many times a tag is used
			int[] tagTotals = new int[AudioManagerWindow.projectSettings.GetTagNames().Length+1];

			AudioClipData current;
			for(int i = 0; i  < allClips.Count; ++i)
			{
				current = allClips[i];

				if(current.TagFlag == 0)	// Untagged
					++tagTotals[tagTotals.Length-1];
				else
				{
					for(int t = 0; t < tagTotals.Length-1; ++t)
					{
						if((current.TagFlag & (1 << t)) != 0)
							++tagTotals[t];
					}
				}

				_totalClipTime += current.ClipTime;
				_totalClipOrigSize += current.OrigSize;
				_totalClipCompSize += current.CompSize;
			}

			_totalClipTimeFormatted = System.TimeSpan.FromSeconds (_totalClipTime).ToString ();
			_totalClipOrigSizeFormatted = AudioClipData.GetFormattedSize ((double)_totalClipOrigSize);
			_totalClipCompSizeFormatted = AudioClipData.GetFormattedSize ((double)_totalClipCompSize);

			_totalTagCounts = tagTotals;

			SortClips ();
		}

		public override void OnInspectorUpdate ()
		{
			// Only need to repaint when playing the clip to update the time bar
			if(_playingClip != null)
				parentWindow.Repaint ();
		}

		public override void WillRefreshInterface ()
		{
			// Store the ordering before the Enabled/Disable is run
			lastSort = _currentSortMode;
			lastDescending = _sortDescending;
		}

		public override void OnRefreshedInterface ()
		{
			// Restore the ordering after the Enabled/Disable is run
			_currentSortMode = lastSort;
			_sortDescending = lastDescending;
			SortClips ();
		}

		protected override List<AudioClipData> GetAllAssets ()
		{
			return parentWindow.GetAllClips ();
		}

		protected override List<AudioClipData> GetFilteredAssets()
		{
			string[] searchTerms = _stringFilter.Split (' ');

			List<AudioClipData> filtered = new List<AudioClipData>(GetAllAssets());
			for(int i = filtered.Count - 1; i >= 0; --i)
			{
				var current = filtered[i];

				bool removed = false;
				for(int s = 0; s < searchTerms.Length; ++s)
				{
					if(string.IsNullOrEmpty (searchTerms[s]))
						continue;

					// If the search term isn't in the asset name, remove the asset
					if(!current.Asset.name.Contains (searchTerms[s]))
					{
						filtered.RemoveAt (i);
						removed = true;
						break;
					}
				}

				if(removed)
					continue;

				if((current.TagFlag == 0))
				{
					// If the asset is untagged and we hide the untagged, remove the asset
					if(!_showUntagged)
						filtered.RemoveAt (i);
				}
				else if((current.TagFlag & _tagFilter) == 0)
				{
					// If the asset's tags don't contain any of the shown tags, remove the asset
					filtered.RemoveAt (i);
				}
			}

			return filtered;
		}

		public override void DrawWindow (Rect r)
		{
			// Pressing escape clears the search bar
			if(Event.current.isKey && Event.current.keyCode == KeyCode.Escape)
				_stringFilter = "";

			List<AudioClipData> allClips = GetAllAssets ();
			List<AudioClipData> filteredClips = GetFilteredAssets ();

			// Draw the search bar
			Rect searchRect = GUILayoutUtility.GetRect (parentWindow.position.width, 20);
			DrawSearchBar (ref _stringFilter, searchRect, 400);

			// Draw the header
			AudioClipData.Column startRow = EditorApplication.isPlaying ? AudioClipData.Column.RuntimePlayCount : AudioClipData.Column.VariatorCount;
			using(var scope = new EditorGUILayout.HorizontalScope())
			{
				DrawHeader (startRow, allClips, filteredClips);
				GUILayout.Space (_kScrollBarWidth);
			}

			// Seperator
			GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (1f));

			if(filteredClips != null && filteredClips.Count > 0)
			{
				using(var scroll = new EditorGUILayout.ScrollViewScope(scrollPos, false, true))
				{
					if(scroll.scrollPosition != scrollPos)
						nextScrollPos = scroll.scrollPosition;
					
					Vector2 scrollView = new Vector2(scrollPos.y - _kPadding, scrollPos.y + r.height + _kPadding);

					for(int i = 0; i < filteredClips.Count; ++i)
					{
						AudioClipData currentClip = filteredClips[i];
						if(currentClip.Selected && currentClip.IsClipPlaying)
							_playingClip = currentClip;

						// Draw a space if the entry is outside the culling view, otherwise draw the clip
						if(_kElementSize * i < scrollView.x || _kElementSize * (i+1) > scrollView.y)
							GUILayout.Space (_kElementSize);
						else
							currentClip.Draw (startRow, parentWindow.BoxStyle);
					}

					if(Event.current.type != EventType.layout)
					{
						bool needsRepaint = scrollPos != nextScrollPos;
						scrollPos = nextScrollPos;
						if(needsRepaint)
							parentWindow.Repaint ();
					}
				}
			}
			else
			{
				// No clip to show
				using(var hScope = new EditorGUILayout.HorizontalScope ())
				{
					GUILayout.FlexibleSpace ();
					using(var vScope = new EditorGUILayout.VerticalScope())
					{
						GUILayout.Label ("No Audio Clips Found", parentWindow.BoxStyle);
						if(GUILayout.Button ("Search Again"))
							parentWindow.CollectClips ();
					}
					GUILayout.FlexibleSpace ();
				}
			}

			DrawFooter ();
		}

		/// <summary>
		/// Draws the two header rows
		/// </summary>
		/// <param name="startColumn">Start column.</param>
		/// <param name="allClips">All clips.</param>
		/// <param name="filteredClips">Filtered clips.</param>
		private void DrawHeader(AudioClipData.Column startColumn, List<AudioClipData> allClips, List<AudioClipData> filteredClips)
		{
			const float height = 20f;

			using(new EditorGUILayout.VerticalScope ())
			{
				using(new EditorGUILayout.HorizontalScope())
				{
					if(EditorApplication.isPlaying)
					{
						GUILayout.Label ("P", parentWindow.BoxStyle, GUILayout.Height (height), GUILayout.Width (AudioClipData.kWidths[0]));
						GUILayout.Label ("B?", parentWindow.BoxStyle, GUILayout.Height (height), GUILayout.Width (AudioClipData.kWidths[1]));
					}

					GUILayout.Label ("V?", parentWindow.BoxStyle, GUILayout.Height (height), GUILayout.Width (AudioClipData.kWidths[2]));

					if(GUILayout.Button ("Clip Name", parentWindow.BoxStyle, GUILayout.Height (height)))
						SetSortMode (SortMode.Name);
					
					if(GUILayout.Button ("Time", parentWindow.BoxStyle, GUILayout.Width (AudioClipData.kWidths[4]), GUILayout.Height (height)))
						SetSortMode (SortMode.Time);
					
					if(GUILayout.Button ("Quality", parentWindow.BoxStyle, GUILayout.Width (AudioClipData.kWidths[5]), GUILayout.Height (height)))
						SetSortMode (SortMode.Quality);

					if(GUILayout.Button ("Original Size", parentWindow.BoxStyle, GUILayout.Width (AudioClipData.kWidths[6]), GUILayout.Height (height)))
						SetSortMode (SortMode.OrigSize);

					if(GUILayout.Button ("Comp. Rate", parentWindow.BoxStyle, GUILayout.Width (AudioClipData.kWidths[7]), GUILayout.Height (height)))
						SetSortMode (SortMode.CompRate);

					if(GUILayout.Button ("Comp. Size", parentWindow.BoxStyle, GUILayout.Width (AudioClipData.kWidths[8]), GUILayout.Height (height)))
						SetSortMode (SortMode.CompSize);

					if(GUILayout.Button ("Tags", parentWindow.BoxStyle, GUILayout.Width (AudioClipData.kWidths[9]), GUILayout.Height (height)))
						TagFilterContextMenu ();
				}

				using(new EditorGUILayout.HorizontalScope())
				{
					bool filtered = (filteredClips.Count != allClips.Count);

					if(EditorApplication.isPlaying)
					{
						GUILayoutUtility.GetRect (GUIContent.none, parentWindow.BoxStyle, GUILayout.Height (height), GUILayout.Width (AudioClipData.kWidths[0]));
						GUILayoutUtility.GetRect (GUIContent.none, parentWindow.BoxStyle, GUILayout.Height (height), GUILayout.Width (AudioClipData.kWidths[1]));
					}

					GUILayoutUtility.GetRect (GUIContent.none, parentWindow.BoxStyle, GUILayout.Height (height), GUILayout.Width (AudioClipData.kWidths[2]));

					if(EditorApplication.isPlaying)
					{
						if(filtered)
							GUILayout.Label (string.Format ("AudioClip Files: {0} | Total Files: {1} | AudioClips Played: {2}", filteredClips.Count, allClips.Count, TotalSoundsPlayed), parentWindow.BoxStyle, GUILayout.Height (height));
						else
							GUILayout.Label (string.Format ("Total AudioClip Files: {0} | AudioClips Played: {1}", allClips.Count, TotalSoundsPlayed), parentWindow.BoxStyle, GUILayout.Height (height));
					}
					else
					{
						if(filtered)
							GUILayout.Label (string.Format ("AudioClip Files: {0} | Total Files: {1}", filteredClips.Count, allClips.Count), parentWindow.BoxStyle, GUILayout.Height (height));
						else
							GUILayout.Label (string.Format ("Total AudioClip Files: {0}", allClips.Count), parentWindow.BoxStyle, GUILayout.Height (height));
					}

					GUILayout.Label (_totalClipTimeFormatted, parentWindow.BoxStyle, GUILayout.Width (AudioClipData.kWidths[4]), GUILayout.Height (height));
					GUILayoutUtility.GetRect (GUIContent.none, parentWindow.BoxStyle, GUILayout.Height (height), GUILayout.Width (AudioClipData.kWidths[5]));
					GUILayout.Label (_totalClipOrigSizeFormatted, parentWindow.BoxStyle, GUILayout.Width (AudioClipData.kWidths[6]), GUILayout.Height (height));
					GUILayoutUtility.GetRect (GUIContent.none, parentWindow.BoxStyle, GUILayout.Height (height), GUILayout.Width (AudioClipData.kWidths[7]));
					GUILayout.Label (_totalClipCompSizeFormatted, parentWindow.BoxStyle, GUILayout.Width (AudioClipData.kWidths[8]), GUILayout.Height (height));

					Rect tagRect = GUILayoutUtility.GetRect (GUIContent.none, parentWindow.BoxStyle, GUILayout.Height (height), GUILayout.Width (AudioClipData.kWidths[9]));
					var allTags = AudioManagerWindow.projectSettings.allTags;
					float xOffset = tagRect.width / allTags.Count;
					tagRect.width = xOffset;

					using(new BackgroundColorScope())
					{
						for(int i = 0; i < allTags.Count; ++i)
						{
							GUI.backgroundColor = allTags[i].color;	
							GUI.Box (tagRect, new GUIContent ((string)null, allTags[i].name), AudioClipData.tagStyle);
							tagRect.x += xOffset;
						}
					}
				}
			}
		}

		/// <summary>
		/// Draws the tag filter menu
		/// </summary>
		private void TagFilterContextMenu()
		{
			GenericMenu menu = new GenericMenu();

			menu.AddItem (new GUIContent ("Show All"), false, ()=>{
				_tagFilter = -1;
				_showUntagged = true;
			});

			menu.AddItem (new GUIContent ("Hide All"), false, ()=>{
				_tagFilter = 0;
				_showUntagged = false;
			});

			menu.AddSeparator ("");

			menu.AddItem (new GUIContent (string.Format ("Untagged [{0}]", _totalTagCounts[_totalTagCounts.Length-1])), _showUntagged, ()=>{
				_showUntagged = !_showUntagged;
			});

			string[] tags = AudioManagerWindow.projectSettings.GetTagNames ();
			for(int i = 0; i < tags.Length; ++i)
			{
				int index = i;
				bool enabled = (_tagFilter & (1 << index)) != 0;
				menu.AddItem (new GUIContent (string.Format ("{0} [{1}]", tags[index], _totalTagCounts[index])), enabled, ()=>{
					if(enabled)
						_tagFilter &= ~(1 << index);
					else
						_tagFilter |= (1 << index);
				});
			}

			menu.ShowAsContext ();
		}

		/// <summary>
		/// Draws the audio clip tab footer.
		/// </summary>
		private void DrawFooter()
		{
			if(_playingClip != null)
			{
				using(var scope = new EditorGUILayout.HorizontalScope ())
				{
					float clipPosition = _playingClip.ClipPosition;

					GUILayout.Label (string.Format ("Clip: {0} | Time: {1:F3}", _playingClip.Asset.name, clipPosition));
					GUILayout.Box ("", GUILayout.Width (300f), GUILayout.Height (10f));
					Rect r = GUILayoutUtility.GetLastRect ();
					EditorGUI.ProgressBar (r, _playingClip.ClipPosition / (_playingClip.ClipTime / 1000f), "");
				}
			}
			else
				GUILayout.Label ("No clip playing");
		}
	}
}
