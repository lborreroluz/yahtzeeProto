﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Hibernum.Unity.Audio
{
	[CustomPropertyDrawer (typeof (ClipVariator.Variation))]
	public class ClipVariationDrawer : PropertyDrawer
	{
		const float kPadding = 2f;

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty (position, label, property);

			SerializedProperty overrideVolume = property.FindPropertyRelative ("volumeOverride");
			SerializedProperty overridePitch = property.FindPropertyRelative ("pitchOverride");

			Rect overrideVolumeRect = new Rect(position.x, position.y+kPadding, position.width, position.height/3f-kPadding);
			if(overrideVolume.boolValue)
			{
				Rect newPos = EditorGUI.PrefixLabel (overrideVolumeRect, GUIUtility.GetControlID (FocusType.Passive), new GUIContent ("Volume Override"));

				Rect overrideRect = new Rect(newPos.x, newPos.y, 20f, newPos.height);
				Rect volumeRect = new Rect(newPos.x+20f, newPos.y, newPos.width-20f, newPos.height);

				EditorGUI.PropertyField (overrideRect, overrideVolume, GUIContent.none);
				EditorGUI.PropertyField (volumeRect, property.FindPropertyRelative ("volume"), GUIContent.none);
			}
			else
			{
				EditorGUI.PropertyField (overrideVolumeRect, overrideVolume);
			}

			Rect overridePitchRect = new Rect(position.x, position.y+position.height/3f+kPadding, position.width, position.height/3f-kPadding);
			if(overridePitch.boolValue)
			{
				Rect newPos = EditorGUI.PrefixLabel (overridePitchRect, GUIUtility.GetControlID (FocusType.Passive), new GUIContent ("Pitch Override"));

				Rect overrideRect = new Rect(newPos.x, newPos.y, 20f, newPos.height/3f);
				Rect pitchRect = new Rect(newPos.x+20f, newPos.y, newPos.width-20f, newPos.height);

				EditorGUI.PropertyField (overrideRect, overridePitch, GUIContent.none);
				EditorGUI.PropertyField (pitchRect, property.FindPropertyRelative ("pitch"), GUIContent.none);
			}
			else
			{
				EditorGUI.PropertyField (overridePitchRect, overridePitch);
			}

			Rect clipRect = new Rect(position.x, position.y+2f*(position.height/3f)+kPadding, position.width, position.height/3f-kPadding);
			EditorGUI.PropertyField (clipRect, property.FindPropertyRelative ("clip"));

			EditorGUI.EndProperty ();
		}

		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			return 3 * (base.GetPropertyHeight (property, label) + 2 * kPadding);
		}
	}
}
