﻿using UnityEngine;
using UnityEditor;

using System.Collections;
using System.Collections.Generic;

namespace Hibernum.Unity.Audio.Drawers
{
	/// <summary>
	/// Base class for AudioManager editor window panels
	/// </summary>
	public abstract class BaseWindow<WindowType, AssetType, EditorAssetType> 
		where WindowType : BaseWindow<WindowType, AssetType, EditorAssetType> 
		where AssetType : UnityEngine.Object 
		where EditorAssetType : BaseEditorData<AssetType, EditorAssetType, WindowType>
	{
		protected const float _kPadding = 15f;
		protected const float _kScrollBarWidth = 15f;
		protected const float _kElementSize = 19f;

		protected Vector2 scrollPos;
		protected Vector2 nextScrollPos;
		protected AudioManagerWindow parentWindow { get; private set; }

		public BaseWindow(AudioManagerWindow parent)
		{
			parentWindow = parent;
		}

		/// <summary>
		/// Gets all the inspectable assets
		/// </summary>
		/// <returns>The all assets.</returns>
		protected abstract List<EditorAssetType> GetAllAssets();
		/// <summary>
		/// Gets the inspectable assets filtered
		/// </summary>
		/// <returns>The filtered assets.</returns>
		protected abstract List<EditorAssetType> GetFilteredAssets();

		/// <summary>
		/// Gets the list of currently selected inspectable assets
		/// </summary>
		/// <returns>The selected.</returns>
		public List<EditorAssetType> GetSelected()
		{
			List<EditorAssetType> allAssets = GetAllAssets ();
			List<EditorAssetType> selected = new List<EditorAssetType>();
			for(int i = 0; i < allAssets.Count; ++i)
			{
				if(allAssets[i].Selected)
					selected.Add (allAssets[i]);
			}

			return selected;
		}

		/// <summary>
		/// Selects all the inspectable assets
		/// </summary>
		public void SelectAll()
		{
			List<EditorAssetType> allAssets = GetFilteredAssets ();
			UnityEngine.Object[] newSelection = new Object[allAssets.Count];

			for(int i = 0; i < newSelection.Length; ++i)
				newSelection[i] = allAssets[i].Asset;

			Selection.objects = newSelection;
		}

		/// <summary>
		/// Handles the shift selection between two selctable assets
		/// </summary>
		/// <param name="fromAsset">From asset.</param>
		/// <param name="toData">To data.</param>
		public void ShiftSelect(AssetType fromAsset, EditorAssetType toData)
		{
			List<EditorAssetType> allAssets = GetFilteredAssets ();
			List<UnityEngine.Object> selection = new List<Object>(Selection.objects);

			int fromIndex = allAssets.FindIndex ((c)=>{
				return c.Asset == fromAsset;
			});

			int toIndex = allAssets.IndexOf (toData);

			// Invert 'from' and 'to' if 'to' is smaller so we don't have to change the loop
			if(toIndex < fromIndex)
			{
				int temp = toIndex;
				toIndex = fromIndex;
				fromIndex = temp;
			}

			for(int i = fromIndex; i <= toIndex; ++i)
			{
				if(!selection.Contains (allAssets[i].Asset))
					selection.Add (allAssets[i].Asset);
			}

			Selection.objects = selection.ToArray ();
		}

		/// <summary>
		/// Draws a generic search bar using the provided rect
		/// </summary>
		/// <param name="searchText">Search text.</param>
		/// <param name="area">Area.</param>
		/// <param name="searchBarWidth">Search bar width.</param>
		public void DrawSearchBar(ref string searchText, Rect area, float searchBarWidth)
		{
			float midY = (area.height - 15) / 2;
			float currentx = 4;

			GUI.BeginGroup (area, GUI.skin.FindStyle("Toolbar"));
			GUI.Label (new Rect (currentx, midY, 36, 15), new GUIContent ("Filter"));
			currentx += 36;

			searchBarWidth = Mathf.Min (searchBarWidth, area.width - 59);
			searchText = GUI.TextField (new Rect(currentx, midY, searchBarWidth, 15), searchText, GUI.skin.FindStyle("ToolbarSeachTextField"));
			currentx += searchBarWidth;
			if(GUI.Button (new Rect(currentx, midY, 15, 15), "", GUI.skin.FindStyle("ToolbarSeachCancelButton"))){
				searchText = "";
			}
			GUI.EndGroup ();
		}

		public abstract void DrawWindow(Rect r);

		public virtual void OnEnable(){  }
		public virtual void OnDisable(){  }
		public virtual void WillRefreshInterface(){  }
		public virtual void OnRefreshedInterface(){  }
		public virtual void OnPlayModeChanged(){  }
		public virtual void OnProjectChanged(){  }
		public virtual void OnInspectorUpdate(){  }
	}
}
