﻿using UnityEngine;
using System.Collections;
using System;

namespace Hibernum.Unity.Audio.Internal
{
	/// <summary>
	/// Creates a scope that will reset the GUI.backgroundColor when disposed
	/// </summary>
	public class BackgroundColorScope : IDisposable
	{
		private Color _bgColor;

		public BackgroundColorScope ()
		{
			_bgColor = GUI.backgroundColor;
		}

		public BackgroundColorScope (Color newColor):this()
		{
			GUI.backgroundColor = newColor;
		}

		/// <summary>
		/// Resets the GUI.backgroundColor to the color when the scope was created
		/// </summary>
		public void Reset ()
		{
			GUI.backgroundColor = _bgColor;
		}

		#region IDisposable implementation
		public void Dispose ()
		{
			Reset ();
		}
		#endregion
	}

	/// <summary>
	/// Creates a scope that will reset the GUI.contentColor when disposed
	/// </summary>
	public class ContentColorScope : IDisposable
	{
		private Color _contentColor;

		public ContentColorScope ()
		{
			_contentColor = GUI.contentColor;
		}

		public ContentColorScope (Color newColor):this()
		{
			GUI.contentColor = newColor;
		}

		/// <summary>
		/// Resets the GUI.contentColor to the color when the scope was created
		/// </summary>
		public void Reset()
		{
			GUI.contentColor = _contentColor;
		}

		#region IDisposable implementation
		public void Dispose ()
		{
			Reset ();
		}
		#endregion
	}
}
