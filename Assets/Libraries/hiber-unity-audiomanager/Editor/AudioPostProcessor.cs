﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Hibernum.Unity.Audio
{
	public class AudioPostProcessor : AssetPostprocessor
	{
		/// <summary>
		/// Forces the AudioManagerWindow to refresh all assets if it is opened.
		/// </summary>
		private void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
		{
			AudioManagerWindow.OnAudioAssetImported ();
		}

		/// <summary>
		/// Forces the AudioManagerWindow to refresh all assets if it is opened.
		/// </summary>
		private void OnPostprocessAudio(AudioClip clip)
		{
			AudioManagerWindow.OnAudioAssetImported ();
		}
	}
}
