﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEditor;

using System;
using System.Collections;
using System.Collections.Generic;

using Hibernum.Unity.Audio.Drawers;
using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio
{
	public class BaseVariatorData : BaseEditorData<BaseClipVariator, BaseVariatorData, VariatorWindow>
	{
		public static readonly float[] _kWidths = new float[]{
			200f,
			-1f,
			150f,
		};

		private static GUILayoutOption[] _kWidthOptions;
		public static GUIContent kNoMixerGroup = new GUIContent("No Default Mixer Set");

		static BaseVariatorData()
		{
			_kWidthOptions = new GUILayoutOption[(int)Rows.LAST];

			_kWidthOptions[(int)Rows.VariatorType] = GUILayout.Width (_kWidths[0]);
			_kWidthOptions[(int)Rows.Name] = GUILayout.ExpandWidth (true);
			_kWidthOptions[(int)Rows.MixerGroup] = GUILayout.Width (_kWidths[2]);
		}

		public enum Rows
		{
			VariatorType = 0,
			Name = 1,
			MixerGroup = 2,
			LAST
		}

		private float _lastClickTime;

		public GUIContent NameFormatted { get; private set; }
		public GUIContent TypeFormatted { get; private set; }

		public BaseVariatorData(BaseClipVariator variator, VariatorWindow window) : base (window)
		{
			Asset = variator;

			NameFormatted = new GUIContent(variator.name);
			TypeFormatted = new GUIContent(variator.GetType ().Name);
		}

		public void Draw(Rows firstRow, GUIStyle style)
		{
			Color bg = GUI.backgroundColor;
			if(Selected)
				GUI.backgroundColor = _selectedColor;

			using(var scope = new EditorGUILayout.HorizontalScope (GUIStyle.none))
			{
				for(int i = (int)firstRow; i < (int)Rows.LAST; ++i)
					DrawRow ((Rows)i, style);
			}

			GUI.backgroundColor = bg;
		}

		public void DrawRow(Rows pass, GUIStyle style)
		{
			GUILayoutOption widthOption = _kWidthOptions[(int)pass];

			switch(pass)
			{
			case Rows.VariatorType:
				SelectionButton (TypeFormatted, style, widthOption);
				break;

			case Rows.Name:
				SelectionButton (NameFormatted, style, widthOption);
				break;

			case Rows.MixerGroup:
				SelectionButton (Asset.DefaultMixerGroup != null ? new GUIContent(Asset.DefaultMixerGroup.name) : kNoMixerGroup, style, widthOption);
				break;
			}
		}

		#region IDisposable implementation
		public override void Dispose ()
		{
			Asset = null;
		}
		#endregion
	}
}
