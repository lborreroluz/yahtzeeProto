﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Hibernum.Unity.Audio.Internal
{
	public class ProjectSettings : ScriptableObject
	{
		[System.Serializable]
		public struct Tag
		{
			public Color color;
			public string name;

			public Tag(Color color, string name) : this()
			{
				this.color = color;
				this.name = name;
			}
		}

		[System.Serializable]
		private class AssetMap
		{
			[SerializeField] private List<string> _keys = new List<string>();
			[SerializeField] private List<int> _values = new List<int>();

			public List<string> keys { get { return new List<string>(_keys); } }
			public List<int> values { get { return new List<int>(_values); } }

			public bool HasKey(string key)
			{
				return _keys.IndexOf (key) >= 0;
			}

			public void Remove(string key)
			{
				int index = _keys.IndexOf (key);
				if(index >= 0)
				{
					_keys.RemoveAt (index);
					_values.RemoveAt (index);
				}
			}

			public int this [string key]
			{
				get
				{
					int index = _keys.IndexOf (key);
					if(index >= 0)
						return _values[index];
					else
						return 0;
				}
				set
				{
					int index = _keys.IndexOf (key);
					if(index >= 0)
						_values[index] = value;
					else
					{
						_keys.Add (key);
						_values.Add (value);
					}
				}
			}
		}

		private static readonly Tag _kInvalidTag = new Tag(Color.red, "INVALID");

		private readonly List<Tag> _defaultTags = new List<Tag>()
		{
			new Tag(new Color32 (255, 0, 234, 255), "Placeholder"),
			new Tag(Color.green, "Final"),
		};
			
		[SerializeField, HideInInspector] private AssetMap _audioClipTags = new AssetMap();
		[SerializeField] private List<Tag> _customTags = new List<Tag>();
		private List<Tag> _allTags = new List<Tag>();


		public List<Tag> allTags
		{
			get
			{
				_allTags.Clear ();
				_allTags.AddRange (_defaultTags);
				_allTags.AddRange (_customTags);
				return _allTags;
			}
		}

		public Tag GetTag(int index)
		{
			if(allTags.Count <= index)
				return _kInvalidTag;
			
			return _allTags[index];
		}

		public string[] GetTagNames()
		{
			string[] names = new string[allTags.Count];
			for(int i = 0; i < _allTags.Count; ++i)
				names[i] = _allTags[i].name;

			return names;
		}

		public int GetAssetTagFlag(string assetGuid)
		{
			return _audioClipTags[assetGuid];
		}

		public int[] GetAssetTags(string assetGuid)
		{
			List<int> tags = new List<int>();
			int flag = _audioClipTags[assetGuid];
			for(int i = 0, iMax = allTags.Count; i < iMax; ++i)
			{
				if(((1 << i) & flag) != 0)
					tags.Add (i);
			}

			return tags.ToArray ();
		}

		public int SetAssetTags(string assetGuid, int[] tags)
		{
			int flag = 0;
			for(int i = 0; i < tags.Length; ++i)
				flag |= (1 << tags[i]);

			_audioClipTags[assetGuid] = flag;
			EditorUtility.SetDirty (this);
			return flag;
		}

		public void CleanupAssetTags()
		{
			List<string> assets = _audioClipTags.keys;
			for(int i = 0; i < assets.Count; ++i)
			{
				string path = AssetDatabase.GUIDToAssetPath (assets[i]);
				if(string.IsNullOrEmpty (path))
					_audioClipTags.Remove (assets[i]);
			}
			EditorUtility.SetDirty (this);
		}
	}
}
