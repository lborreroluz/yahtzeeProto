﻿using UnityEngine;
using UnityEditor;

using System;
using System.Collections;
using System.Collections.Generic;
using Hibernum.Unity.Audio.Drawers;

namespace Hibernum.Unity.Audio
{
	public class BaseEditorData<AssetType, EditorAssetType, WindowType> : IDisposable
		where AssetType : UnityEngine.Object
		where EditorAssetType : BaseEditorData<AssetType, EditorAssetType, WindowType>
		where WindowType : BaseWindow<WindowType, AssetType, EditorAssetType>
	{
		protected const long _kShiftSelectModifier = (long)EventModifiers.Shift;
		#if UNITY_EDITOR_OSX
		protected const long _kMultiSelectModifier = (long)EventModifiers.Command;
		#else
		protected const long _kMultiSelectModifier = (long)EventModifiers.Control;
		#endif

		#if UNITY_PRO_LICENSE
		protected static readonly Color _selectedColor = new Color(0.5f, 0.5f, 1f, 1f);
		#else
		protected static readonly Color _selectedColor = new Color(0.8f, 0.8f, 1f, 1f);
		#endif

		public bool Selected { get; set; }
		public AssetType Asset { get; set; }

		private float _lastClickTime = 0f;
		protected WindowType windowInstance;

		protected virtual void OnSingleClick(){  }
		protected virtual void OnRightClick(){  }
		protected virtual void OnDoubleClick(){  }

		public BaseEditorData(WindowType window)
		{
			windowInstance = window;
		}

		protected void SelectionButton(GUIContent content, GUIStyle style, params GUILayoutOption[] options)
		{
			if(GUILayout.Button (content, style, options))
				HandleClicks ();
		}

		protected void SelectionButton(Rect r, GUIContent content, GUIStyle style)
		{
			if(GUI.Button (r, content, style))
				HandleClicks ();
		}

		protected void HandleClicks()
		{
			if(Event.current.button == 1)
				HandleRightClick ();
			else if(((long)Event.current.modifiers & _kShiftSelectModifier) != 0)
				HandleShiftSelection ();
			else if(((long)Event.current.modifiers & _kMultiSelectModifier) != 0)
				HandleMultiSelection ();
			else
				HandleSelection ();
		}

		private void HandleRightClick()
		{
			if(!Selected)
				Selection.activeObject = Asset;
			
			OnRightClick ();
		}

		private void HandleShiftSelection()
		{
			List<UnityEngine.Object> selection = new List<UnityEngine.Object>(Selection.objects);

			for(int i = selection.Count -1; i >= 0; --i)
			{
				if(selection[i] is AssetType)
				{
					windowInstance.ShiftSelect (selection[i] as AssetType, this as EditorAssetType);
					return;
				}
			}
		}

		private void HandleMultiSelection()
		{
			// Add or remove clip from inspection
			List<UnityEngine.Object> selection = new List<UnityEngine.Object>(Selection.objects);
			if(Selected)
				selection.Remove (Asset);	// Unselect
			else
				selection.Add (Asset); 		// Select

			Selection.objects = selection.ToArray ();
		}

		private void HandleSelection()
		{
			// Select the clip for inspection
			Selection.objects = new UnityEngine.Object[]{ Asset };

			if(Time.realtimeSinceStartup - _lastClickTime < 0.25f)
				OnDoubleClick ();
			else
				OnSingleClick ();

			_lastClickTime = Time.realtimeSinceStartup;
		}

		#region IDisposable implementation
		public virtual void Dispose (){  }
		#endregion
	}
}
