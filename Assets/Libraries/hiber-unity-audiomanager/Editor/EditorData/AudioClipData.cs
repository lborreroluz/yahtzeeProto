﻿using UnityEngine;
using UnityEditor;

using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using Hibernum.Unity.Audio.Drawers;
using Hibernum.Unity.Audio.Internal;

namespace Hibernum.Unity.Audio
{
	/// <summary>
	/// Audio clip wrapper for the AudioManagerWindow.
	/// </summary>
	public class AudioClipData : BaseEditorData<AudioClip, AudioClipData, AudioClipWindow>
	{
		public static readonly float[] kWidths = new float[]{20f, 20f, 20f, -1f, 100f, 45f, 100f, 45f, 100f, 100f};

		private static GUILayoutOption[] _kWidthOptions;
		private static GUIContent _kLabelDash;
		private static GUIContent _kLabelEmpty;

		private static Texture2D _tagTexture;
		private static GUIStyle _tagStyle;
		public static GUIStyle tagStyle
		{ 
			get
			{
				if(_tagStyle == null)
				{
					_tagStyle = new GUIStyle();
					_tagStyle.normal.background = _tagTexture;
				}

				if(_tagTexture == null)
				{
					_tagTexture = new Texture2D(16,16);
					for(int x = 0; x < _tagTexture.width; ++x){
						for(int y = 0; y < _tagTexture.height; ++y){
							_tagTexture.SetPixel (x,y, Color.white);
						}
					}
					_tagTexture.Apply ();
					_tagStyle.normal.background = _tagTexture;
				}

				return _tagStyle;
			} 
		}

		static AudioClipData()
		{
			_kWidthOptions = new GUILayoutOption[(int)Column.LAST];

			_kWidthOptions[(int)Column.RuntimePlayCount] = GUILayout.Width (kWidths[0]);
			_kWidthOptions[(int)Column.BreakOnPlay] = GUILayout.Width (kWidths[1]);
			_kWidthOptions[(int)Column.VariatorCount] = GUILayout.Width (kWidths[2]);
			_kWidthOptions[(int)Column.ClipName] = GUILayout.ExpandWidth (true);
			_kWidthOptions[(int)Column.ClipDuration] = GUILayout.Width (kWidths[4]);
			_kWidthOptions[(int)Column.ClipImportQuality] = GUILayout.Width (kWidths[5]);
			_kWidthOptions[(int)Column.ClipOrigSize] = GUILayout.Width (kWidths[6]);
			_kWidthOptions[(int)Column.ClipCompRate] = GUILayout.Width (kWidths[7]);
			_kWidthOptions[(int)Column.ClipCompSize] = GUILayout.Width (kWidths[8]);
			_kWidthOptions[(int)Column.Tags] = GUILayout.Width (kWidths[9]);

			_kLabelDash = new GUIContent("-");
			_kLabelEmpty = new GUIContent("");
		}

		public enum Column
		{
			RuntimePlayCount = 0,
			BreakOnPlay = 1,
			VariatorCount = 2,
			ClipName = 3,
			ClipDuration = 4,
			ClipImportQuality = 5,
			ClipOrigSize = 6,
			ClipCompRate = 7,
			ClipCompSize = 8,
			Tags = 9,
			LAST
		}

		/// <summary>
		/// The name of the represented asset.
		/// </summary>
		public GUIContent NameFormatted { get; private set; }
		/// <summary>
		/// The original size of the asset file
		/// </summary>
		public int OrigSize { get; private set; }
		/// <summary>
		/// The original size of the asset file (Human readable)
		/// </summary>
		public GUIContent OrigSizeFormatted { get; private set; }
		/// <summary>
		/// The compressed size of the asset file
		/// </summary>
		/// <value>The size of the comp.</value>
		public int CompSize { get; private set; }
		/// <summary>
		/// The compressed size of the asset file (Human readable)
		/// </summary>
		public GUIContent CompSizeFormatted { get; private set; }
		/// <summary>
		/// The compression rate of the asset file
		/// </summary>
		public float CompRate { get; private set; }
		/// <summary>
		/// The compression rate of the asset file (Human readable)
		/// </summary>
		public GUIContent CompRateFormatted { get; private set; }
		/// <summary>
		/// The quality set in the inspector (Human readable)
		/// </summary>
		public float Quality { get; private set; }
		/// <summary>
		/// The quality set in the inspector (Human readable)
		/// </summary>
		public GUIContent QualityFormatted { get; private set; }
		/// <summary>
		/// The total time of the clip
		/// </summary>
		public float ClipTime { get; private set; }
		/// <summary>
		/// The total time of the clip (Human readable)
		/// </summary>
		public GUIContent ClipTimeFormatted { get; private set; }
		/// <summary>
		/// The clip variators that contains this vlip
		/// </summary>
		public List<BaseClipVariator> Variators { get; private set; }
		/// <summary>
		/// The variator count (GUIContent)
		/// </summary>
		public GUIContent VariatorCount { get; private set; }
		/// <summary>
		/// Is this audio clip present in a variator
		/// </summary>
		public bool HasClipVariator { get { return Variators != null && Variators.Count > 0; } }
		/// <summary>
		/// The number of times this clip has played
		/// </summary>
		public int RuntimePlayCount { get; set; }
		/// <summary>
		/// Should the editor pause when this sound is played?
		/// </summary>
		public bool BreakOnPlay { get; set; }
		/// <summary>
		/// The GUID of the associated audio clip
		/// </summary>
		public string AssetGUID { get; private set; }
		/// <summary>
		/// The tags associated with the asset
		/// </summary>
		public int[] Tags { get; private set; }
		/// <summary>
		/// A bitmask representing the tags of the asset
		/// </summary>
		public int TagFlag { get; private set; }

		/// <summary>
		/// Current time of the clip.
		/// </summary>
		public float ClipPosition { 
			get {
				return  EditorAudioUtilities.GetClipPosition (Asset);
			}
		}

		/// <summary>
		/// is the clip currently playing in the editor
		/// </summary>
		public bool IsClipPlaying { 
			get {
				return EditorAudioUtilities.IsClipPlaying (Asset);
			}
		}

		public AudioClipData(AudioClip clip, List<BaseClipVariator> variators, AudioClipWindow window) : base (window)
		{
			Asset = clip;
			Variators = variators;
			VariatorCount = new GUIContent(variators.Count.ToString ());

			Refresh ();
		}

		public void Refresh()
		{
			Asset = AssetDatabase.LoadAssetAtPath<AudioClip>(AssetDatabase.GetAssetPath (Asset));
			AssetGUID = AudioManagerWindow.GetAssetGUID (Asset);

			Tags = AudioManagerWindow.projectSettings.GetAssetTags (AssetGUID);
			TagFlag = AudioManagerWindow.projectSettings.GetAssetTagFlag (AssetGUID);

			NameFormatted = new GUIContent(Asset.name);

			// Get importer
			AudioImporter importer = AssetImporter.GetAtPath (AssetDatabase.GetAssetPath (Asset)) as AudioImporter;
			OrigSize = EditorAudioUtilities.GetOrigSize (importer);
			OrigSizeFormatted = new GUIContent (GetFormattedSize (OrigSize));

			CompSize = EditorAudioUtilities.GetCompSize (importer);
			CompSizeFormatted = new GUIContent (GetFormattedSize (CompSize));

			CompRate = CompSize / (float)OrigSize;
			CompRateFormatted = new GUIContent(string.Format ("{0}%", (CompRate * 100f).ToString ("N0")));

			ClipTime = (float)EditorAudioUtilities.GetClipDuration (Asset);
			ClipTimeFormatted = new GUIContent ((ClipTime/1000f).ToString ("F3"));

			// Get importer sample settings
			AudioImporterSampleSettings settings = importer.GetOverrideSampleSettings (EditorUserBuildSettings.activeBuildTarget.ToString ());
			Quality = settings.quality;

			if(settings.compressionFormat == AudioCompressionFormat.Vorbis)
				QualityFormatted = new GUIContent (string.Format ("{0}%", settings.quality * 100f));
			else
				QualityFormatted = new GUIContent ("--");
		}

		/// <summary>
		/// Draw the specified pass and style.
		/// </summary>
		/// <param name="pass">Pass.</param>
		/// <param name="style">Style.</param>
		private void DrawRow(Column pass, GUIStyle style, Color bg)
		{
			GUILayoutOption widthOption = _kWidthOptions[(int)pass];

			switch (pass)
			{
			case Column.RuntimePlayCount:
				if(RuntimePlayCount > 0)
					SelectionButton (new GUIContent (RuntimePlayCount.ToString ()), style, widthOption);
				else
				{
					GUI.backgroundColor = Color.red;
					SelectionButton (_kLabelDash, style, widthOption);
					GUI.backgroundColor = Selected ? _selectedColor : bg;
				}
				break;
			
			case Column.BreakOnPlay:
				if(BreakOnPlay)
					GUI.backgroundColor = Color.red;
				
				if(GUILayout.Button (_kLabelEmpty, style, widthOption))
					BreakOnPlay = !BreakOnPlay;
				
				GUI.backgroundColor = Selected ? _selectedColor : bg;
				break;

			case Column.VariatorCount:
				if(GUILayout.Button (HasClipVariator ? VariatorCount : _kLabelDash, style, widthOption))
					ShowVariatorMenu (true);
				break;
			case Column.ClipName:
				SelectionButton (NameFormatted, style, widthOption);
				break;

			case Column.ClipDuration:
				SelectionButton (ClipTimeFormatted, style, widthOption);
				break;

			case Column.ClipImportQuality:
				SelectionButton (QualityFormatted, style, widthOption);
				break;

			case Column.ClipOrigSize:
				SelectionButton (OrigSizeFormatted, style, widthOption);
				break;

			case Column.ClipCompRate:
				SelectionButton (CompRateFormatted, style, widthOption);
				break;

			case Column.ClipCompSize:
				SelectionButton (CompSizeFormatted, style, widthOption);
				break;

			case Column.Tags:
				if(Tags.Length > 0)
				{
					Rect tagRect = GUILayoutUtility.GetRect (GUIContent.none, style, widthOption);

					float xOffset = tagRect.width / Tags.Length;
					Rect currentRect = new Rect(tagRect);
					currentRect.width = xOffset;

					for(int i = 0; i < Tags.Length; ++i)
					{
						var tag = AudioManagerWindow.projectSettings.GetTag (Tags[i]);
						GUI.backgroundColor = tag.color;
						GUI.Box (currentRect, new GUIContent ((string)null, tag.name), tagStyle);
						currentRect.x += xOffset;
					}

					SelectionButton (tagRect, GUIContent.none, "label");
				}
				else
				{
					GUI.backgroundColor = Color.clear;
					SelectionButton (GUIContent.none, style, widthOption);
				}
				GUI.backgroundColor = bg;
				break;
			}
		}

		public void Draw(Column startRow, GUIStyle style)
		{
			Color bg = GUI.backgroundColor;
			if(Selected)
				GUI.backgroundColor = _selectedColor;

			EditorGUILayout.BeginHorizontal (GUIStyle.none);

			for(int i = (int)startRow; i < (int)Column.LAST; ++i)
				DrawRow ((Column)i, style, bg);

			EditorGUILayout.EndHorizontal ();

			GUI.backgroundColor = bg;
		}

		private void ShowVariatorMenu(bool hasSelectMenu)
		{
			GenericMenu menu = new GenericMenu();

			if(Selection.objects.Length > 1)
				menu.AddDisabledItem (new GUIContent(string.Format("{0} clips selected", Selection.objects.Length)));
			else
				menu.AddDisabledItem (new GUIContent(string.Format ("GUID: {0}", AssetGUID)));

			if(hasSelectMenu)
			{
				if(Variators.Count > 0)
				{
					for(int i = 0; i < Variators.Count; ++i)
					{
						int index = i;
						menu.AddItem (new GUIContent(string.Format ("Select/{0}", Variators[i].name)), false, ()=>{
							Selection.activeObject = Variators[index];
							EditorGUIUtility.PingObject (Selection.activeObject);
						});
					}
				}
				else{
					menu.AddDisabledItem (new GUIContent("Select/No Variators"));
				}
			}
			else{
				menu.AddDisabledItem (new GUIContent("Select/Cannot Multi-Select"));
			}

			menu.AddItem (new GUIContent("New/Clip Variator"), false, ()=>{
				CreateAudioClipUtility.CreateClipVariator ();
			});

			menu.AddItem (new GUIContent("New/BGM Variator"), false, ()=>{
				CreateAudioClipUtility.CreateBGMVariator ();
			});

			string[] tags = AudioManagerWindow.projectSettings.GetTagNames ();

			List<int> assetTags = new List<int>(Tags);
			List<AudioClipData> data = windowInstance.GetSelected();
			menu.AddItem (new GUIContent ("Tags/Clear Tags"), false, ()=>{
				for(int a = 0; a < data.Count; ++a)
				{
					AudioClipData currentData = data[a];
					assetTags.Clear ();
					currentData.Tags = assetTags.ToArray ();
					currentData.TagFlag = AudioManagerWindow.projectSettings.SetAssetTags (currentData.AssetGUID, currentData.Tags);
				}
			});

			menu.AddSeparator ("Tags/");

			for(int i = 0; i < tags.Length; ++i)
			{
				int index = i;
				bool isTagged = assetTags.IndexOf (index) >= 0;
				menu.AddItem (new GUIContent (string.Format("Tags/{0}", tags[index])), isTagged, ()=>{
					for(int a = 0; a < data.Count; ++a)
					{
						AudioClipData currentData = data[a];
						assetTags = new List<int>(currentData.Tags);

						if(isTagged)
							assetTags.Remove (index);
						else if(!assetTags.Contains (index))
							assetTags.Add (index);

						assetTags.Sort ();
						currentData.Tags = assetTags.ToArray ();
						currentData.TagFlag = AudioManagerWindow.projectSettings.SetAssetTags (currentData.AssetGUID, currentData.Tags);
					}
				});
			}

			menu.ShowAsContext ();
		}

		protected override void OnSingleClick ()
		{
			EditorAudioUtilities.StopAllClips ();
		}

		protected override void OnRightClick ()
		{
			if(!Selected)
				Selection.activeObject = Asset;
			
			ShowVariatorMenu (Selected && Selection.objects.Length == 1);
		}

		protected override void OnDoubleClick ()
		{
			EditorAudioUtilities.PlayClip (Asset);
		}

		/// <summary>
		/// Gets the size, formatted in human-readable format.
		/// </summary>
		/// <returns>The formatted size.</returns>
		/// <param name="len">Length.</param>
		public static string GetFormattedSize(double len)
		{
			string[] sizes = { "B", "KB", "MB", "GB" };
			int order = 0;
			while (len >= 1024 && order + 1 < sizes.Length)
			{
				order++;
				len = len/1024;
			}

			return string.Format("{0:0.##} {1}", len, sizes[order]);
		}

		#region IDisposable implementation
		public override void Dispose ()
		{
			Asset = null;
			Variators.Clear ();
			Variators = null;
		}
		#endregion
	}
}
