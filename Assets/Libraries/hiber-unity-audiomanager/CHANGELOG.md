# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/) and [Keep a CHANGELOG](http://keepachangelog.com).

## [Unreleased]
### Added
- [POOLING] Automatic AudioSource pooling and management
- [AUDIO] Clip Variators to create dynamic or static audio clip variatons using a single asset
- [AUDIO] BGM Module to handle multiple bgm tracks playing at once or in sequence
- [EDITOR] Editor window to inspect all audio assets in the project
- [EDITOR] Editor window to inspect all variator assets in the project
- [EDITOR] Sound tracking to identify which assets were played and how many times
- [EDITOR] Breakpoints can be added on sounds so the editor pauses when the sound is played
- [ERROR CHECKING] Extension methods on Variators for in-line null-checks
- [COMPONENTS] Monobehaviours for integration of sounds based on events
- [BGM] Optionally synchronized bgm layer tracks

[Unreleased]: https://gitlab.hibernum.com/libs/hiber-unity-audiomanager/compare/a529f40d...HEAD

