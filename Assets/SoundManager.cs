﻿using UnityEngine;
using System.Collections;
using Hibernum.Unity.Audio;

public class SoundManager : MonoBehaviour {

	public GameController gc; 

	public ClipVariator ellen_game_starts;
	public ClipVariator ellen_game_ends;
	public ClipVariator ellen_bonus_EllenRoll;
	public ClipVariator ellen_bonus_ReReReRoll;
	public ClipVariator ellen_bonus_completedTheUpperSection;
	public ClipVariator ellen_round_completedTheLowerSection;
	public ClipVariator ellen_results_dominantwin;
	public ClipVariator ellen_results_consolation;
	public ClipVariator ellen_results_almostWin;
	public ClipVariator ellen_results_almostLose;
	public ClipVariator ellen_summary_win;
	public ClipVariator ellen_summary_consolation;
	public ClipVariator ellen_waiting;
	public ClipVariator ellen_matchmaking;
	public ClipVariator ellen_slot_zeroInASlot;
	public ClipVariator ellen_slot_3orMoreSame;
	public ClipVariator ellen_slot_pairOrLower;
	public ClipVariator ellen_slot_special_highscore_threeOfAKind;
	public ClipVariator ellen_slot_special_lowScore_threeOfAKind;
	public ClipVariator ellen_slot_special_highscore_rourOfAKind;
	public ClipVariator ellen_slot_special_lowScore_rourOfAKind;
	public ClipVariator ellen_slot_special_fullhouse;
	public ClipVariator ellen_slot_special_smallStraight;
	public ClipVariator ellen_slot_special_largeStraight;
	public ClipVariator ellen_slot_special_FiveOfAKind;
	public ClipVariator ellen_slot_special_Chance;
	public ClipVariator ellen_slot_special_UpperSectionBonus;
	public ClipVariator ellen_slot_special_FiveOfAKindBonus;
	public ClipVariator ellen_slot_special_almostScoredNothing;
	public ClipVariator ellen_slot_special_almostScoredBig;
	public ClipVariator ellen_rolling_timeToroll;
	public ClipVariator ellen_rolling_rollingMore;
	public ClipVariator ellen_rolling_soClose;
	public ClipVariator ellen_lastRoll_whichShouldYouSelect;

	public bool mute = false;


	public void playAfterRoll ()
	{
		System.Random rnd = new System.Random();
		int dice = rnd.Next(0, 5);
		switch(dice){
		case 0:
	{
		PlayingClipInfo playing = ellen_results_almostLose.PlayClip ();		
	}
			break;
		case 1:
	{
		PlayingClipInfo playing = ellen_results_almostLose.PlayClip ();		
	}
			break;
		case 2:
	{
		PlayingClipInfo playing = ellen_slot_3orMoreSame.PlayClip ();		
	}
			break;
		case 3:
	{
		PlayingClipInfo playing = ellen_rolling_soClose.PlayClip ();		
	}
			break;
		case 4:
	{
		PlayingClipInfo playing = ellen_rolling_soClose.PlayClip ();		
	}
			break;
		}
	}

	public void playStartSound ()
	{
		PlayingClipInfo playing = ellen_game_starts.PlayClip ();		
	}

	public void playEndSound ()
	{
		PlayingClipInfo playing = ellen_game_ends.PlayClip ();		
	}

	public void playSound (ClipVariator cv)
	{
		PlayingClipInfo playing = cv.PlayClip ();
	}

	public void playSound (int id)
	{
//		return;
		int returnvalue = 0;
		switch ((PlayerVO.PlayerScoreBoard)id) {
		case PlayerVO.PlayerScoreBoard.ones:
			returnvalue = gc.sc.addPoints (GameStateVO.Instance.Dices, 1);
			if (returnvalue >= 3 * (id + 1)) {
//				PlayingClipInfo playing = ellen_slot_3orMoreSame.PlayClip ();	
			} else {
//				PlayingClipInfo playing = ellen_slot_pairOrLower.PlayClip ();	
			}
			break;
		case PlayerVO.PlayerScoreBoard.twos:
			returnvalue = gc.sc.addPoints (GameStateVO.Instance.Dices, 2);
			if (returnvalue >= 3 * (id + 1)) {
//				PlayingClipInfo playing = ellen_slot_3orMoreSame.PlayClip ();	
			} else {
//				PlayingClipInfo playing = ellen_slot_pairOrLower.PlayClip ();	
			}
			break;
		case PlayerVO.PlayerScoreBoard.threes:
			returnvalue = gc.sc.addPoints (GameStateVO.Instance.Dices, 3);
			if (returnvalue >= 3 * (id + 1)) {
//				PlayingClipInfo playing = ellen_slot_3orMoreSame.PlayClip ();	
			} else {
//				PlayingClipInfo playing = ellen_slot_pairOrLower.PlayClip ();	
			}
			break;
		case PlayerVO.PlayerScoreBoard.frous:
			returnvalue = gc.sc.addPoints (GameStateVO.Instance.Dices, 4);
			if (returnvalue >= 3 * (id + 1)) {
//				PlayingClipInfo playing = ellen_slot_3orMoreSame.PlayClip ();	
			} else {
//				PlayingClipInfo playing = ellen_slot_pairOrLower.PlayClip ();	
			}
			break;
		case PlayerVO.PlayerScoreBoard.fives:
			returnvalue = gc.sc.addPoints (GameStateVO.Instance.Dices, 5);
			if (returnvalue >= 3 * (id + 1)) {
//				PlayingClipInfo playing = ellen_slot_3orMoreSame.PlayClip ();	
			} else {
//				PlayingClipInfo playing = ellen_slot_pairOrLower.PlayClip ();	
			}
			break;
		case PlayerVO.PlayerScoreBoard.sixes:
			returnvalue = gc.sc.addPoints (GameStateVO.Instance.Dices, 6);
			if (returnvalue >= 3 * (id + 1)) {
//				PlayingClipInfo playing = ellen_slot_3orMoreSame.PlayClip ();	
			} else {
//				PlayingClipInfo playing = ellen_slot_pairOrLower.PlayClip ();	
			}
			break;
		case PlayerVO.PlayerScoreBoard.threeOfAKind:
			if (gc.sc.CheckForXOfAKind (GameStateVO.Instance.Dices, 3)) {
				returnvalue = gc.sc.addPoints (GameStateVO.Instance.Dices);
					
				if (returnvalue >= 3 * (4)) {
//					PlayingClipInfo playing = ellen_slot_3orMoreSame.PlayClip ();	
				} else {
//					PlayingClipInfo playing = ellen_slot_pairOrLower.PlayClip ();	
				}
			}
			break;
		case PlayerVO.PlayerScoreBoard.fourOfAKind:
			if (gc.sc.CheckForXOfAKind (GameStateVO.Instance.Dices, 4)) {
				returnvalue = gc.sc.addPoints (GameStateVO.Instance.Dices);					
				if (returnvalue >= 4 * (4)) {
//					PlayingClipInfo playing = ellen_slot_3orMoreSame.PlayClip ();	
				} else {
//					PlayingClipInfo playing = ellen_slot_pairOrLower.PlayClip ();	
				}
			}
			break;
		case PlayerVO.PlayerScoreBoard.fullHouse:
			if (gc.sc.CheckForFullHouse (GameStateVO.Instance.Dices)) {
				returnvalue = 25;
//				PlayingClipInfo playing = ellen_slot_special_fullhouse.PlayClip ();	
			}
			break;
		case PlayerVO.PlayerScoreBoard.smallStraight:
			if (gc.sc.CheckForShortSequence (GameStateVO.Instance.Dices)) {
				returnvalue = 30;
//				PlayingClipInfo playing = ellen_slot_special_smallStraight.PlayClip ();	
			}
			break;
		case PlayerVO.PlayerScoreBoard.largeStraight:
			if (gc.sc.CheckForLongSequence (GameStateVO.Instance.Dices)) {
				returnvalue = 40;
//				PlayingClipInfo playing = ellen_slot_special_largeStraight.PlayClip ();	
			}
			break;
		case PlayerVO.PlayerScoreBoard.yahtzee:
			if (gc.sc.CheckForXOfAKind (GameStateVO.Instance.Dices, 5)) {
				returnvalue = 50;
				PlayingClipInfo playing = ellen_slot_special_FiveOfAKind.PlayClip ();	
			}
			break;
		case PlayerVO.PlayerScoreBoard.chance:
			{
				returnvalue = gc.sc.addPoints (GameStateVO.Instance.Dices);
//				PlayingClipInfo playing = ellen_slot_special_Chance.PlayClip ();	
			break;
			}
		}
		if (returnvalue == 0) 
		{
				PlayingClipInfo playing = ellen_results_almostWin.PlayClip ();	
		}	
	}
}
