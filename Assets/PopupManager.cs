﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PopupManager : MonoBehaviour {

	public GameObject backdrop;
	public GameObject closeButton;
	public List<GameObject> popupList = new List<GameObject>();
	
	public enum PopUpTypes
	{
		noPopups = -1,
		WinScreen
	}

	public PopUpTypes currentltyActivePopup = PopUpTypes.noPopups;

	public void closePopups()
	{
		SetPop(PopUpTypes.noPopups);
	}

	public void SetPop (PopUpTypes ppt)
	{
		currentltyActivePopup = ppt;
		for (int i = 0; i < popupList.Count; i++) 
		{
			popupList[i].SetActive((i == (int)ppt));
		}
		backdrop.SetActive(ppt != PopUpTypes.noPopups);
		closeButton.SetActive(ppt != PopUpTypes.noPopups);
	}
}
