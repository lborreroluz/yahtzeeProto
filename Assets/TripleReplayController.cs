﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class TripleReplayController : MonoBehaviour {
	
	public GameObject ReplayButton;
	public Button VisualButton;
	public Text buttonText;
	public Color activeTextColor;
	public Color disabledTextColor;

	public Color activeIconColor;
	public Color disabledIconColor;

	public GameObject costContainer;

	public List<Image> icons;
	
	public void UpdateVisuals ()
	{
		switch (GameStateVO.Instance.currentRoll) {
		case GameStateVO.Rolls.noRoll:	
		case GameStateVO.Rolls.roll0:
		case GameStateVO.Rolls.roll1:
		case GameStateVO.Rolls.roll2:
		case GameStateVO.Rolls.bonusRoll:	
			ReplayButton.SetActive (false);
			break;
//		case GameStateVO.Rolls.tripleReplay:	
			ReplayButton.SetActive (true);
			VisualButton.interactable = !GameStateVO.Instance.HasPlayerPlayedTripleReplay ();
			costContainer.SetActive (!GameStateVO.Instance.HasPlayerPlayedTripleReplay ());
			buttonText.color = (!GameStateVO.Instance.HasPlayerPlayedTripleReplay ()) ? activeTextColor : disabledTextColor;
			for (int i = 0; i < icons.Count; i++) {
				icons[i].color = (!GameStateVO.Instance.HasPlayerPlayedTripleReplay ()) ? activeIconColor : disabledIconColor;
			}
			break;
			case GameStateVO.Rolls.RollEnd:	
			ReplayButton.SetActive(false);
			break;
		}
	}
}
