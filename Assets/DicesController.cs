﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DicesController : MonoBehaviour {
	public List<DiceController> DicesControllerList;
	public GameController gc;
	public GameObject messageContainerAllLocked;

	void Start ()
	{
		for (int i = 0; i < DicesControllerList.Count; i++) {
			DicesControllerList [i].id = i;
			DicesControllerList [i].gc = gc;
		}
	}

	public void UpdateVisuals ()
	{
		for (int i = 0; i < DicesControllerList.Count; i++) {
			DicesControllerList [i].value = GameStateVO.Instance.Dices [i].value;
			DicesControllerList [i].UpdateVisuel ();
		}		
		messageContainerAllLocked.SetActive (false);
//		messageContainerAllLocked.SetActive (GameStateVO.Instance.AreAllDiceLocked ());
//		if (GameStateVO.Instance.AreAllDiceLocked ()) {
//			StartCoroutine ("HideErrorMessage");
//		}
	}

	IEnumerator HideErrorMessage()
	{
		yield return new WaitForSeconds (1.5f);
		messageContainerAllLocked.SetActive(false);
		yield return null;
	}

	public void MakeDicesEmpty ()
	{
		for (int i = 0; i < DicesControllerList.Count; i++) 
		{
			DicesControllerList[i].MakeEmpty();
			DicesControllerList[i].UpdateVisuel ();
		}		
	}

	public void RollDices ()
	{
		for (int i = 0; i < DicesControllerList.Count; i++)
		{
			DicesControllerList[i].RollDice();
			GameStateVO.Instance.Dices[i].value = DicesControllerList[i].value;
		}
	}
}
