﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CurrencyCounterController : MonoBehaviour {
	
	public Text currencyCounter;
	public GameObject StorePopup;	

	public void UpdateVisuals()
	{
		currencyCounter.text = "" + UserDataVO.Instance.userSoftCurrencyCount;
	}

	public bool PayStuff(int paymentValue)
	{
		bool returnValue = false;
		if (UserDataVO.Instance.RemoveCash (paymentValue)) 
		{
			UpdateVisuals();
			returnValue = true;
		} else {
			StorePopup.SetActive(true);
		}
		return returnValue;
	}
}