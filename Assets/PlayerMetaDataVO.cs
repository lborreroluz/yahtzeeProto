﻿using UnityEngine;
using System.Collections;

public sealed class PlayerMetaDataVO
{
	int currencyCount;
	
	static readonly PlayerMetaDataVO _instance = new PlayerMetaDataVO();
    public static PlayerMetaDataVO Instance
    {
		get
		{
		    return _instance;
		}
    }

    PlayerMetaDataVO()
    {
		currencyCount = 0;
    }

	public bool currencyRemoved (int cost)
	{
		bool returnValue = (currencyCount<cost);
		currencyCount -= cost;
		return returnValue;
	}
}