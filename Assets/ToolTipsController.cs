﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ToolTipsController : MonoBehaviour {

	public List<GameObject> ToolTips;

	public void TurnOnAToolTip (int id)
	{
//		Debug.Log(id + " " + ToolTips.Count);
		if (id < ToolTips.Count) 
		{
			ToolTips [id].SetActive (true);
		}
	}

	public void TurnOffAToolTip (int id)
	{
		ToolTips [id].SetActive (false);
	}

	public void TurnOffAllToolTips ()
	{
		for (int i = 0; i < ToolTips.Count; i++) 
		{
			ToolTips [i].SetActive (false);
		}
	}
}