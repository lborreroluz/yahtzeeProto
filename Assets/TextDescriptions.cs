﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextDescriptions {

	static public List<string> typeTitleStrings = new List<string>();
	static public List<string> descriptionStrings = new List<string>();
	

	static TextDescriptions ()
	{
		typeTitleStrings = new List<string> 
	{
	"Ones",
	"Twos",
	"Threes",
	"Fours",
	"Fives",
	"Sixes",
	"Three Of A Kind",
	"Four Of A Kind",
	"Full House",
	"Small Straight",
	"Large Straight",
	"Yahtzee",
	"Chance"
	};

	descriptionStrings = new List<string> 
	{
	"Sum of ones only",
	"Sum of twos only",
	"Sum of threes only",
	"Sum of four only",
	"Sum of fives only",
	"Sum of sixes only",
	"At least three of the same dice \nScore: Sum of all dice",
	"At least four of the same dice \nScore: Sum of all dice",
	"A three of a kind and a pair \nScore: 25 points",
	"Four dice in a sequence \nScore: 30 points",
	"Five dice in a sequence \nScore: 40 points",
	"All five of the same dice \nScore: 50 points",
	"Any combination of dice \nScore: Sum of all dice"	
	};
	}

	static public string getTitleForSlotID (PlayerVO.PlayerScoreBoard slotId)
	{
		string returnValue = "";
		int inputValue = (int)slotId;
		if (inputValue >= 0) {
			returnValue = typeTitleStrings[(int)slotId];
		}
		
		return returnValue;
	}

	static public string getTitleForSlotID(int slotId)
	{
		string returnValue = "";
		int inputValue = (int)slotId;
		if (inputValue >= 0) {
			returnValue = typeTitleStrings[inputValue];
		}
		Debug.Log("returnValue: " + returnValue + " " + slotId);
		return returnValue;
	}
	
	static public string getDescriptionForSlotID(PlayerVO.PlayerScoreBoard slotId)
	{
		string returnValue = "";
		int inputValue = (int)slotId;
		if (inputValue >= 0) {
			returnValue = descriptionStrings[(int)slotId];
		}
		return returnValue;
	}

	static public string getDescriptionForSlotID(int slotId)
	{
		string returnValue = "";
		int inputValue = (int)slotId;
		if (inputValue >= 0) {
			returnValue = descriptionStrings[(int)slotId];
		}
		return returnValue;
	}
}
