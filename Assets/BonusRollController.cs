﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BonusRollController : MonoBehaviour {

	public Button bonusRollButton;
	public Button BonusRollGetMore;
	public Text buttonText;
	public Color activeTextColor;
	public Color disabledTextColor;
	public Image activeIcon;
	public Image disabledIcon;
	public GameObject costContainer;

	public void UpdateVisuals ()
	{
		switch (GameStateVO.Instance.currentRoll) {
		case GameStateVO.Rolls.noRoll:	
			bonusRollButton.gameObject.SetActive (false);
			bonusRollButton.interactable = false;
			buttonText.color = activeTextColor;
			activeIcon.gameObject.SetActive (false);
			costContainer.gameObject.SetActive (true);
			BonusRollGetMore.gameObject.SetActive (false);
			break;
		case GameStateVO.Rolls.roll0:
		case GameStateVO.Rolls.roll1:
		case GameStateVO.Rolls.roll2:	
			bonusRollButton.gameObject.SetActive (false);
			bonusRollButton.interactable = true;
			buttonText.color = activeTextColor;
			activeIcon.gameObject.SetActive (false);
			costContainer.gameObject.SetActive (true);
			BonusRollGetMore.gameObject.SetActive (false);
//			buttonText.text = "Roll (1)";
			break;
		case GameStateVO.Rolls.bonusRoll:	
			if (UserDataVO.Instance.userSoftCurrencyCount >= 1) {
				bonusRollButton.gameObject.SetActive (true);
				bonusRollButton.interactable = true;
				buttonText.color = activeTextColor;
				activeIcon.gameObject.SetActive (true);
				costContainer.gameObject.SetActive (true);
			} else {
				BonusRollGetMore.gameObject.SetActive (true);
			}
//			buttonText.text = "Bonus Roll 3[BR]";
			break;
//			case GameStateVO.Rolls.tripleReplay:	
			bonusRollButton.gameObject.SetActive(false);
			bonusRollButton.interactable = false;
			buttonText.color = disabledTextColor;
			activeIcon.gameObject.SetActive(false);
			costContainer.gameObject.SetActive(false);
			BonusRollGetMore.gameObject.SetActive(false);
//			buttonText.text = "Bonus Roll 3[BR]";
			break;
			case GameStateVO.Rolls.RollEnd:	
			bonusRollButton.gameObject.SetActive(true);
			buttonText.color = disabledTextColor;
			bonusRollButton.interactable = false;
			costContainer.gameObject.SetActive(false);
			activeIcon.gameObject.SetActive(false);
			BonusRollGetMore.gameObject.SetActive(false);
//			buttonText.text = "";
			break;
		}
	}
}