﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class lockIconController : MonoBehaviour {

	public Image LockIcon;

	public enum LockIconState
	{
		nothing = 0,
		deactivated,
		disable,
		enabled
	}
	public LockIconState currentLockIconState;

	public Color nothing;
	public Color deactivated;
	public Color disable;
	public Color enabled;

	public void SetLockIconState(LockIconState lis)
	{
		currentLockIconState = lis;
		switch (currentLockIconState) 
		{
		case LockIconState.nothing:
			LockIcon.color = nothing;
			break;
		case LockIconState.deactivated:
			LockIcon.color = deactivated;
			break;
		case LockIconState.disable:
			LockIcon.color = disable;
			if (GameStateVO.Instance.AreAnyDicesLocked ()) 
			{
				currentLockIconState = LockIconState.enabled;
				SetLockIconState (LockIconState.enabled);
			}
			break;
		case LockIconState.enabled:
			LockIcon.color = enabled;
			break;
		}
	}
}
