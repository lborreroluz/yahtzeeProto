﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TickerController : MonoBehaviour {

	public Text TickerLabel;
	public GameObject ChatLayout;
	public GameObject YouVsThemLayout;
	public GameObject BonusRollLayout;
	public GameObject OpponentsTurnLayout;	

	public enum TickerState:int
	{
		youThem = 0,
		chat,
		bonusRollCount,
		opponentTurn,
	}
	public TickerState currentTickerState = TickerState.youThem;

	public void ChangeTickerState ()
	{
		switch(GameStateVO.Instance.currentRoll) {
		case GameStateVO.Rolls.noRoll:
			currentTickerState = TickerState.youThem;
			break;
		case GameStateVO.Rolls.roll0:
			currentTickerState = TickerState.youThem;
//			currentTickerState = TickerState.chat;
			break;
		case GameStateVO.Rolls.roll1:
		case GameStateVO.Rolls.roll2:
			currentTickerState = TickerState.youThem;
			break;
		case GameStateVO.Rolls.bonusRoll:
//		case GameStateVO.Rolls.tripleReplay:
			currentTickerState = TickerState.bonusRollCount;
			break;
		case GameStateVO.Rolls.RollEnd:
			currentTickerState = TickerState.youThem;
//			currentTickerState = TickerState.opponentTurn;
			break;
		}
	}

	public void UpdateVisualState ()
	{
		ChangeTickerState ();
		switch (currentTickerState) 
		{
			case TickerState.youThem:
//			TickerLabel.text = "";
			ChatLayout.SetActive(false);
			YouVsThemLayout.SetActive(true);
			BonusRollLayout.SetActive(false);
			OpponentsTurnLayout.SetActive(false);
			break;
			case TickerState.opponentTurn:
//			TickerLabel.text = "Roll Dice. You have " + (4 - (int)(GameStateVO.Instance.currentRoll)) + " rolls left.";
			ChatLayout.SetActive(false);
			YouVsThemLayout.SetActive(false);
			BonusRollLayout.SetActive(false);
			OpponentsTurnLayout.SetActive(true);
			break;
			case TickerState.bonusRollCount:
//			TickerLabel.text = "You have 4[BR].";
			ChatLayout.SetActive(false);
			YouVsThemLayout.SetActive(false);
			BonusRollLayout.SetActive(true);
			OpponentsTurnLayout.SetActive(false);
			break;
			case TickerState.chat:
//			TickerLabel.text = "Hey! GG.";
			ChatLayout.SetActive(true);
			YouVsThemLayout.SetActive(false);
			BonusRollLayout.SetActive(false);
			OpponentsTurnLayout.SetActive(false);
			break;
		}
	}
}
