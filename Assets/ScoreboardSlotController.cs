﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ScoreboardSlotController : MonoBehaviour {

	public GameObject shadow;
	public GameObject highlight;
	public GameObject cell;
	public GameObject yahtzeeBonusBanner;
	public Text TitleText;
	public Text DescriptiveText;
	public Text ScoreText;
	public Text ScoreThemText;
	public Button ConfirmationButton;
	public Color normalColor;
	public Color scoredColor;
	public Color highlightedColor;
	public Color otherColor;
	public int id;
	public GameController gc;
	public GameObject tooltip;
	public GameObject playTip;
	public GameObject ThemProfilePicture;
	public Button submitButton;
	public Text ErrorMessage;

	public IconSelector iconSel;

	public bool newScoreSet = false;
	public bool isToolTip = true;

	public enum VisualState
	{
		disabled = 0,
		normalNoVisualizedScore,
		scored,
		visualizingPoint,
		visualizingPointWithHighlight,
		highlight
	}
	public VisualState currentVisualState = VisualState.normalNoVisualizedScore;

	public void UpdateVisual ()
	{
		if (GameStateVO.Instance.player1.ScoreBoard [id].currentState == ScoreboardSlotVO.SlotState.scored) {
			chooseVisualState (VisualState.scored);
		} else {
			switch (GameStateVO.Instance.currentRoll) {
			case GameStateVO.Rolls.noRoll:
			case GameStateVO.Rolls.roll0:
			case GameStateVO.Rolls.roll1:
			case GameStateVO.Rolls.roll2:
				chooseVisualState (VisualState.normalNoVisualizedScore);
				break;
			case GameStateVO.Rolls.bonusRoll:
//			case GameStateVO.Rolls.tripleReplay:
			case GameStateVO.Rolls.RollEnd:
				chooseVisualState (VisualState.highlight);
				break;
			}
		}
	}

	public void chooseVisualState (VisualState selectedisualState)
	{
		currentVisualState = selectedisualState;
		Debug.Log (selectedisualState);
		switch (currentVisualState) {
		case VisualState.disabled:
			break;
		case VisualState.normalNoVisualizedScore:
			ScoreText.text = "";
			ScoreText.color = highlightedColor;
			EnableBonusBanner (false);
			submitButton.gameObject.SetActive (false);
			ErrorMessage.gameObject.SetActive (false);
			shadow.SetActive (false);
			cell.SetActive (true);
			break;
		case VisualState.visualizingPoint:
			shadow.SetActive (false);
			if (GameStateVO.Instance.player1.ScoreBoard [id].currentState == ScoreboardSlotVO.SlotState.scored) {
				ErrorMessage.gameObject.SetActive(true);
				ErrorMessage.text = "*You have already submitted this score combination";
				submitButton.gameObject.SetActive(false);
			}
			else
			{
				ScoreText.text = "" + gc.sc.CalculatePoints (id);
				ScoreText.color = highlightedColor;
				submitButton.gameObject.SetActive(true);
				ErrorMessage.gameObject.SetActive(GameStateVO.Instance.currentRoll == GameStateVO.Rolls.roll0);
				ErrorMessage.text = "*Roll Your Dice First";

				if (GameStateVO.Instance.currentRoll == GameStateVO.Rolls.roll0) {
					submitButton.interactable = false;
				} else {
					submitButton.interactable = true;
				}
			}
			gc.ttc.TurnOnAToolTip (45 + id);
			if (isToolTip) {
				gc.ttc.TurnOnAToolTip (16 + id);
			} else {
				gc.ttc.TurnOnAToolTip (16 + id + 13);
			}
			gc.ttc.TurnOnAToolTip (42);
			break;
		case VisualState.visualizingPointWithHighlight:
			shadow.SetActive (true);
			if (GameStateVO.Instance.player1.ScoreBoard [id].currentState == ScoreboardSlotVO.SlotState.scored) {
				ErrorMessage.gameObject.SetActive(true);
				ErrorMessage.text = "*You have already submitted this score combination";
				submitButton.gameObject.SetActive(false);
			}
			else
			{
				ScoreText.text = "" + gc.sc.CalculatePoints (id);
				ScoreText.color = highlightedColor;
				submitButton.gameObject.SetActive(true);
				ErrorMessage.gameObject.SetActive(GameStateVO.Instance.currentRoll == GameStateVO.Rolls.roll0);
				ErrorMessage.text = "*Roll Your Dice First";

				if (GameStateVO.Instance.currentRoll == GameStateVO.Rolls.roll0) {
					submitButton.interactable = false;
				} else {
					submitButton.interactable = true;
				}
			}
			gc.ttc.TurnOnAToolTip (45 + id);
			if (isToolTip) {
				gc.ttc.TurnOnAToolTip (16 + id);
			} else {
				gc.ttc.TurnOnAToolTip (16 + id + 13);
			}
			gc.ttc.TurnOnAToolTip (42);
			break;
		case VisualState.scored:
			ScoreText.text = "" + GameStateVO.Instance.player1.ScoreBoard [id].value;
			ScoreText.color = scoredColor;
			cell.SetActive (false);
			shadow.SetActive (false);
			break;
		case VisualState.highlight:
			ScoreText.text = "";
			ScoreText.color = highlightedColor;
			cell.SetActive (true);
			shadow.SetActive (true);
			break;
		}
//		if (!isUpdateOthersScoreRunnning) {
			StartCoroutine (UpdateOthersScore());
//		}
		ThemProfilePicture.SetActive(GameStateVO.Instance.player2.lastScoredSlotId == id && ThemProfilePicture.activeSelf);
		iconSel.UpdateVisuals(id);
	}

	bool isUpdateOthersScoreRunnning;
	public IEnumerator UpdateOthersScore()
	{
		isUpdateOthersScoreRunnning = true;
//		yield return new WaitForSeconds (1.1f);
		ScoreThemText.gameObject.SetActive(GameStateVO.Instance.player2.ScoreBoard [id].currentState == ScoreboardSlotVO.SlotState.scored);
		ThemProfilePicture.SetActive(GameStateVO.Instance.player2.lastScoredSlotId == id);
		ScoreThemText.color = otherColor;
		ScoreThemText.text = "" + GameStateVO.Instance.player2.ScoreBoard [id].value;
		isUpdateOthersScoreRunnning = false;
		yield return null;
	}

	public void EnableBonusBanner(bool value)
	{
		yahtzeeBonusBanner.SetActive(value);
	}

	public void VisualizePoints ()
	{
		if (currentVisualState == VisualState.normalNoVisualizedScore) {
			gc.ttc.TurnOffAllToolTips ();
			gc.sc.UpdateVisual ();
			chooseVisualState (VisualState.visualizingPoint);
			gc.sc.SelectedSlotId = id;
			gc.sbc.SetActiveSubmitButton (false);

			TitleText.text = TextDescriptions.getTitleForSlotID (gc.sc.SelectedSlotId);
			DescriptiveText.text = TextDescriptions.getDescriptionForSlotID (gc.sc.SelectedSlotId);
		} else if (currentVisualState == VisualState.visualizingPoint) {
			chooseVisualState (VisualState.normalNoVisualizedScore);
			gc.ttc.TurnOffAllToolTips ();
			gc.sc.UpdateVisual ();
			gc.sbc.SetActiveSubmitButton (false);
		} else if (currentVisualState == VisualState.highlight) {
			gc.ttc.TurnOffAllToolTips ();
			gc.sc.UpdateVisual ();
			chooseVisualState (VisualState.visualizingPointWithHighlight);
			gc.sc.SelectedSlotId = id;
			gc.sbc.SetActiveSubmitButton (false);

			TitleText.text = TextDescriptions.getTitleForSlotID (gc.sc.SelectedSlotId);
			DescriptiveText.text = TextDescriptions.getDescriptionForSlotID (gc.sc.SelectedSlotId);
		} else if (currentVisualState == VisualState.visualizingPointWithHighlight) {
			chooseVisualState (VisualState.normalNoVisualizedScore);
			gc.ttc.TurnOffAllToolTips ();
			gc.sc.UpdateVisual ();
			gc.sbc.SetActiveSubmitButton (false);
		} else if (currentVisualState == VisualState.scored) {
			gc.ttc.TurnOffAllToolTips ();
			gc.sc.UpdateVisual ();
			chooseVisualState (VisualState.visualizingPoint);
			gc.sc.SelectedSlotId = id;
			gc.sbc.SetActiveSubmitButton (false);

			TitleText.text = TextDescriptions.getTitleForSlotID (gc.sc.SelectedSlotId);
			DescriptiveText.text = TextDescriptions.getDescriptionForSlotID (gc.sc.SelectedSlotId);
		}
	}

	public void ScorePoints ()
	{
		gc.ttc.TurnOffAllToolTips();
		GameStateVO.Instance.CurrentPlayer ().ScoreBoard [id].currentState = ScoreboardSlotVO.SlotState.scored;
		GameStateVO.Instance.CurrentPlayer ().ScoreBoard [id].value = GameStateVO.Instance.CurrentPlayer ().ScoreBoard [id].value;
		chooseVisualState(VisualState.scored);
		gc.EndTurnPlayer();
	}

	public void ShowTip()
	{
		tooltip.SetActive(false);
		playTip.SetActive(false);
//		gc.ttc.TurnOnAToolTip(id);
	}
}